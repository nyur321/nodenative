const { compile } = require('nexe');
var old_conf = {"nexe": {
    "input": "./main.js",
        "output": "./bin/nodenative.exe",
        "temp": "src",
        "vcBuild": "x64",
        "nodeVCBuildArgs": [
        "release",
        "nosign",
        "x64"
    ],
        "download": {
        "proxy": "http://192.168.1.3:8080"
    },
    "resourceRoot": [
        "./assets",
        "./files",
        "./resources"
    ],
        "runtime": {
        "framework": "node",
            "version": "8.6.0",
            "js-flags": "--use_strict",
            "ignoreFlags": false
    }
}};
compile({
    "input": "./main.js",
    "output": "./bin/main.exe",
    "temp": "src",
    "vcBuild": "x64",
    "flags":true,
    "build":true,
    configure:['--dest-cpu=x64'],
    "target":"windows-x64-8.6.0",
    vcBuild:[
        "release",
        "nosign",
        "x64"
    ],
    "resources": [
        "./assets",
        "./files",
        "./resources"
    ]
}).then(() => {
    console.log('ShinApp build successful')
});