"use strict";
//var mailin = require('mailin');
//var nodemailer = require('nodemailer');

var async = require('async');
var common = require('./common');
var _ = require('lodash');

class Middleware {
    constructor(system) {
        this.log = system.log;
        this.app = system.app;
        this.middleware_stack = [];
        this.valid_types = ['router','socket'];
        this.unloaded = [];
        this.middleware_types = ['defaults', 'system', 'extensions', 'application', 'sockets', 'route', 'error', 'static'];
        this.initialized = false;
        this.primary_key = 1;
        this.hooks = {
            addMiddleware: function (system, parent, input) {
                return this.addMiddleware(input, load).bind(this)
            }.bind(this),
            deleteMiddleware: function (system, id, name) {
                return this.deleteMiddleware(id, name).bind(this)
            }.bind(this),
            loadMiddleware: function (system, id, input) {
                return this.load(id, input).bind(this)
            }.bind(this),
            unloadMiddleware: function (system, id, input) {
                return this.unload(id, input).bind(this)
            }.bind(this),
        };
    }

    push_middleware(obj,type) {

        obj['id'] = this.primary_key;
        if(!(common.typeOf(obj.type) == 'string')){
            obj['type'] = 'router';
        }

        if(type){
            if(this.valid_types.indexOf(type) != -1){
                obj['type'] = type;
            }
        }
        this.middleware_stack.push(obj);
        this.primary_key++;
    }

    define(parent, stacks, locals) {
        var temp_locals = locals || {};
        if (common.typeOf(stacks) == 'array') {
            stacks.forEach(function (stack) {
                var obj = {name: null, parent: parent || 'defaults', locals: temp_locals};
                if (common.typeOf(stack) == 'function') {
                    obj['handler'] = stack;
                } else if (common.typeOf(stack) == 'object') {
                    common.extend(obj.locals, stack.locals);
                    obj['name'] = stack.name || null;
                    obj['type'] = stack.type || 'router';
                    obj['parent'] = stack['parent'] || obj['parent'];
                    if (common.typeOf(stack.handler) == 'function') {
                        obj['handler'] = stack.handler;
                    }

                }
                this.push_middleware(obj);
            }.bind(this));
        } else if (common.typeOf(stacks) == 'function') {
            var obj = {name: null, parent: 'defaults', locals: temp_locals};
            obj['handler'] = stacks;
            this.push_middleware(obj);
        }

    }


    getStack() {
        return this.middleware_stack;
    }

    load(id, input) {
        var key = this.search(input, common.typeOf(id) == 'number' ? 'id' : 'name');
        if (key.index > -1) {
            var index_unload = this.unloaded.indexOf(key.value.id);
            this.unloaded.splice(index_unload, 1);
        }

    }

    useSocket(parent,name){
        return function (socket, next) {
            var middleware_array = this.middleware_stack.filter(function (s) {
                var cond_parent = true, cond_filter = true;
                if (parent) {
                    cond_parent = (s.parent == parent);
                }
                if (name) {
                    cond_filter = (s.name == name);
                }

                if (cond_parent && cond_filter && (this.unloaded.indexOf(s.id) == -1)) {
                    return (this.unloaded.indexOf(s.id) == -1) && true;
                } else {
                    return false;
                }


            }.bind(this)).map(function (stack) {
                return async.apply(stack.handler, socket);
            });
            async.series(middleware_array, function (err, results) {
                next();
            });
        }.bind(this);
    }

    use(parent, name,type) {
        var default_type = 'router';
        if (type) {
            default_type = type;
        }
        return function (req, res, next) {
            var middleware_array = this.middleware_stack.filter(function (s) {
                var cond_parent = true, cond_filter = true,cond_type = true;
                if (parent) {
                    cond_parent = (s.parent == parent);
                }
                if (name) {
                    cond_filter = (s.name == name);
                }
                cond_type = (s.type == default_type);
                if (cond_parent && cond_filter && cond_type && (this.unloaded.indexOf(s.id) == -1)) {
                    return true;
                } else {
                    return false;
                }


            }.bind(this)).map(function (stack) {
                return async.apply(stack.handler, req, res);
            });
            async.series(middleware_array, function (err, results) {
                next();
            });
        }.bind(this);
    }

    attach(parent, middlewares,type) {
        var middlewares_stack = middlewares.filter(function (stack) {
            var cond_stack = false;
            switch (common.typeOf(stack)) {
                case 'string':
                    var obj = _.find(this.middleware_stack, function (md) {
                        var cond_name = (md.name == stack),cond_parent = true, cond_type = true;
                        if(parent){
                            cond_parent = (md.parent == parent);
                        }
                        if(type){
                            cond_type = (md.type == type);
                        }
                        return (cond_name && cond_parent && cond_type);
                    });
                    cond_stack = (obj ? true : false);
                    break;
                case 'function':
                    cond_stack = true;
                    break;
                default:
                    cond_stack = false;
                    break;
            }


            return cond_stack;

        }.bind(this));
        return function (req, res, next) {
            var middlewares = middlewares_stack.map(function (stack) {
                switch (common.typeOf(stack)) {
                    case 'string':
                        var obj = _.find(this.middleware_stack, function (md) {
                            return md.name == stack;
                        });
                        if (obj)
                            return async.apply(obj.handler, req, res);
                        break;
                    case 'function':
                        return async.apply(stack, req, res);
                        break;
                }
            }.bind(this));
            async.series(middlewares, function (err, results) {
                next();
            });
        }.bind(this);
    }
    
    attachSocketMiddleware(parent,middlewares){
        var middlewares_stack = middlewares.filter(function (stack) {
            if (common.typeOf(stack) == 'string') {
                var obj = _.find(this.middleware_stack, function (md) {
                    return md.name == stack && md.parent == parent;
                });
                return (obj ? true : false);
            } else {
                return true;
            }
        }.bind(this));

        return function (socket, next) {
            var middlewares = middlewares_stack.map(function (stack) {
                switch (common.typeOf(stack)) {
                    case 'string':
                        var obj = _.find(this.middleware_stack, function (md) {
                            return md.name == stack;
                        });
                        if (obj)
                            return async.apply(obj.handler, socket);
                        break;
                    case 'function':
                        return async.apply(stack, socket);
                        break;
                }
            }.bind(this));
            async.series(middlewares, function (err, results) {
                next();
            });
        }.bind(this);
        
    }

    unload(id, input) {
        var key = this.search(input, common.typeOf(id) == 'number' ? 'id' : 'name').index;
        if (key > -1)
            this.unloaded.push(this.middleware_stack[key].id);
    }

    search(input, key) {
        var index = _.findIndex(this.middleware_stack, function (o) {
            return o[key] == input;
        });

        return {
            index: index,
            value: this.middleware_stack[index]
        }
    }

    addMiddleware(parent, input) {
        var obj = {};
        if (common.typeOf(input) == 'object') {
            obj['name'] = input.name || null;
            obj['parent'] = parent || input.parent || 'default';
            obj['type'] = input.type || 'router';
            if (common.typeOf(input.handler) == 'function') {
                obj['handler'] = input.handler;
                this.push_middleware(obj);
                return true;
            } else {
                this.log('warn', 'invalid middleware ' + obj.name);

            }
            return false;
        }
    }


    addFromFile(file_path, parent) {
        var midware_file = null;
        try {
            midware_file = require(file_path);
        } catch (e) {
            this.log('warn', 'Cannot load middleware from path ' + file_path + ' error: ' + e.message);
        }
        if (common.typeOf(midware_file) == 'object') {
            for (var key in midware_file) {
                var obj = {};
                obj['name'] = key;
                obj['parent'] = parent || 'route';
                if (common.typeOf(midware_file[key]) == 'function') {
                    obj['handler'] = midware_file[key];
                }
                if(key.indexOf(':') != -1){
                    var key_split = key.split(':');
                    obj['name'] = key_split[1];
                    obj['parent'] = key_split[0];
                    obj['type'] = 'socket';

                }
                this.push_middleware(obj);
            }

        }
    }

    deleteMiddleware(id, name) {
        var key = _.findIndex(this.middleware_stack, function (o) {
            return o.name == name;
        });
        if (key > -1)
            this.middleware_stack.splice(key, 1);
    }


}


module.exports = Middleware;