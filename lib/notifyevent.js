var EventEmitter = require('events');
var async = require('async');
var common = require('./common');

class NotifyEvent extends EventEmitter {
    constructor(system) {
        super();
        this.system = system;
        this.log = system.log;
        this.io = system.io;
        this.events = ['loaded', 'started'];
        this.event_file = require(system.c('path.events'));

    }


    init() {
        this.once('newListener', function (event, listener) {
            if (this.events.indexOf(event) == -1)
                this.events.push(event);
        }.bind(this));
    }


    addEvent(name, fn) {
        if (common.typeOf(name) == 'string') {
            this.on(name, fn);
        }
    }

    loadEvents(event, options, callback) {
        if (common.typeOf(event) == 'string') {
            async.series(this.system.events[event], function (err, results) {
                this.emit('loaded', {
                    name: event,
                    type: options.type || 'procedure'
                });
                switch (event){
                    case 'model':
                        this.event_file['onModelsLoad'](function (err,res) {
                            if (common.typeOf(callback) == 'function') {
                                callback(err, res);
                            }

                        });
                        break;
                    case 'controller':
                        this.event_file['onControllersLoad'](function (err,res) {
                            if (common.typeOf(callback) == 'function') {
                                callback(err, res);
                            }
                        });
                        break;
                    case 'route':
                        this.event_file['onRoutesLoad'](function (err,res) {
                            if (common.typeOf(callback) == 'function') {
                                callback(err, res);
                            }
                        });
                        break;
                    case 'plugin':
                        this.event_file['onPluginsLoad'](function (err,res) {
                            if (common.typeOf(callback) == 'function') {
                                callback(err, res);
                            }
                        });
                        break;
                    default:
                        if (common.typeOf(callback) == 'function') {
                            callback(null, results);
                        }
                        break
                }




            }.bind(this));
        }

    }


}


module.exports = NotifyEvent;