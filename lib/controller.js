"use strict";


var fs = require('fs');
var url = require('url');
var express = require("express");
var _ = require('lodash');
var path = require('path');
var ObjectTree = require('object-tree');
var util = require('util');
var common = require('./common');
var io = require("socket.io");
class Controller {
    constructor(own) {
        this.own = own;
        this.app = own.app;
        this.log = own.log;
        this.c = own.c;
        this.io = own.io;
        this.socket = own.socket;
        this.controllers = [];
        this.routes = [];
        this.controller_files = [];
        this.middleware = own.middleware;
        this.sockets = [];
        this.path = {
            controller: this.c('path.controller'),
            route: this.c('path.routes')
        };
        this.allowedMethods = [
            'all',
            'get',
            'post',
            'put',
            'head',
            'delete',
            'options',
            'trace',
            'copy',
            'lock',
            'mkcol',
            'move',
            'purge',
            'propfind',
            'proppatch',
            'unlock',
            'report',
            'mkactivity',
            'checkout',
            'merge',
            'm-search',
            'notify',
            'subscribe',
            'unsubscribe',
            'patch',
            'search',
            'connect'
        ];
        this.allowedSocketMethods = [
            "socket",
            "sockets"
        ];
    }


    initControllers() {
        var controller_files = this.controller_files = this.getControllers(this.path.controller);
        var sockets = [];
        if (controller_files.length > 0) {
            for (var y = 0; y < controller_files.length; y++) {
                var v_router = express.Router();
                v_router.use(this.middleware.use('route', 'default'));
                var v_parents = controller_files[y];
                var v_obj = Object.keys(v_parents.obj).map(function(v_index) {
                    var obj = {
                        path: v_index,
                        method: 'GET',
                        handler: function(req, res) {
                            res.send('No handler associated to ' + req.url);
                        },
                        middleware: [],
                        params: {}
                    };
                    switch (common.typeOf(v_parents.obj[v_index])) {
                        case 'array':
                            for (var x = 0; x < 4; x++) {
                                var index = v_parents.obj[v_index][x];
                                switch (common.typeOf(index)) {
                                    case 'string':
                                        obj['method'] = index;
                                        break;
                                    case 'function':
                                        obj['handler'] = index;
                                        break;
                                    case 'array':
                                        obj['middleware'] = index;
                                        break;
                                    case 'object':
                                        obj['params'] = index;
                                        break;
                                }
                            }
                            break;

                        case 'function':
                            obj['handler'] = v_parents.obj[v_index];
                            break;
                    }

                    return obj;
                }.bind(this));

                v_obj.forEach(function(vo) {
                    vo.parent = v_parents.name;
                    if (this.allowedMethods.indexOf(vo.method.toLowerCase()) != -1 && (!common.isEmpty(vo.method.toLowerCase()))) {
                        v_router[vo.method.toLowerCase()]('/' + (vo.path == 'index' ? '' : vo.path), function(req, res, next) {
                            res.locals['params'] = this;
                            res.locals['title'] = this.title || '';
                            res.locals['name'] = this.name || '';
                            next();
                        }.bind(vo.params), this.middleware.attach('route', vo.middleware), vo.handler);
                    }

                    if (this.allowedSocketMethods.indexOf(vo.method.toLowerCase()) != -1 && (!common.isEmpty(vo.method.toLowerCase()))) {
                        this.sockets.push({
                            parent: v_parents.name,
                            nsp: (vo.path == 'index' ? '' : vo.path),
                            handler: vo.handler,
                            params: vo.params,
                            middleware: vo.middleware

                        });
                    }

                }.bind(this));
                this.controllers.push({
                    path: v_parents.name,
                    router: v_router,
                    sub_routes: v_obj
                });
            }


            this.controllers.forEach(function(cont) {
                this.app.use('/' + cont.path, cont.router);
            }.bind(this));



        } else {
            this.log('warn', 'No controllers found');
        }


        return {
            unregistered_midware: this.unregistered_midware,
            controllers: this.controllers,
            sockets: this.sockets,
        };

    }


    initRoutes() {
        var routes = require(this.path.route);
        var route_objs = [];
        routes.forEach(function(route) {
            var route_obj = {
                "name": route['name'] || null,
                "obj": route,
                "type": 'route'
            };

            if (common.typeOf(route) == 'object') {
                if (common.typeOf(route.method) == 'array') {
                    route.method.forEach(function(meth) {
                        route_obj.name = this.registerRoute(route, meth);
                    }.bind(this));
                } else {
                    route_obj.name = this.registerRoute(route, 'GET');
                }
            } else {
                this.log('Invalid route: type: ' + typeof route + ' at index ' + routes.indexOf(route) + ' of /api/routes.js', 'error', {
                    source: 'route',
                    file: __filename
                });
            }


            route_objs.push(route_obj);


        }.bind(this));

        this.routes = route_objs;
        return route_objs;
    }

    getNamespace(nsp) {
        return _.find(this.sockets,function(socket){
            return socket.path == nsp;
        }).nsp;
    }

    attachSockets(server) {
        this.socket = this.io(server,{
            // path: '/test',
            // serveClient: false,
            // below are engine.IO options
            // pingInterval: 10000,
            // pingTimeout: 5000,
            // cookie: false
        });


        this.sockets = this.sockets.map(function(socket) {
            var socket_url = common.urlJoin('/' + socket.parent, socket.nsp);
            socket_url = '/' + common.urlJoin.apply(null, socket_url.split('/').filter(function(str) {
                return !common.isEmpty(str);
            }));
            return {
                path: socket_url,
                nsp: this.socket.of(socket_url, socket.handler).use(this.middleware.attachSocketMiddleware('socket', socket.middleware))
            };
        }.bind(this));

        

        // this.own.io.on('connection', function(sockets) {
            // console.log(sockets.id);
            // setInterval(function () {
            //     console.log(data);
            //     sockets.emit('message',data);
            // },1000);
            // sockets.on('message',function (data) {
            //
            // })
        // });


    }

    registerRoute(route, meth) {
        var obj = {
            path: route.path,
            method: meth || 'GET',
            handler: function(req, res) {
                res.send('No handler associated to ' + req.url);
            },
            middleware: (common.typeOf(route.middleware) == 'array') ? route.middleware : [],
            params: Object.assign({}, route.params),
            type: route.type || 'router'
        };

        if (typeof route.handler == 'function') {
            obj['handler'] = route.handler;
        }

        if (typeof route.handler == 'string') {
            var obj = {
                path: route.path,
                method: 'GET',
                handler: function(req, res) {
                    res.send('No handler associated to ' + req.url);
                },
                middleware: [],
                attributes: {}
            };
            var handler_str = route.handler.toLowerCase().split('.');
            var path = handler_str[0];
            var sub_route_path = handler_str[1];
            var handler = _.find(this.controllers, function(o) {
                return o.path == path;
            });
            if (handler) {
                handler = _.find(handler.sub_routes, function(sr) {
                    return sr.path == sub_route_path;
                });
                if (handler) {
                    obj['handler'] = handler.handler;
                } else {
                    this.log('warn', 'No handler found for "' + path + '.' + sub_route_path + '"');
                }
            } else {
                this.log('warn', 'No "' + path + '" controller found for route "' + sub_route_path + '"');
            }
        }
        if (obj.type == 'router') {
            if (common.typeOf(route.use) == 'function') {
                this.app.use(obj.path, function(req, res, next) {
                    res.locals.params = obj['params'];
                    res.locals.title = obj['params']['title'] || '';
                    res.locals.name = obj['params']['name'] || '';
                    next();
                }, this.middleware.attach('route', obj['middleware']), route.use);
            } else {
                this.app[obj.method.toLowerCase()](obj.path, function(req, res, next) {
                    res.locals.params = obj['params'];
                    res.locals.title = obj['params']['title'] || '';
                    res.locals.name = obj['params']['name'] || '';
                    next();
                }, this.middleware.attach('route', obj['middleware']), obj.handler);
            }
        }
        if (obj.type == 'socket') {
            this.sockets.push({
                parent: obj.path,
                nsp: '',
                handler: obj.handler,
                params: obj.params,
                middleware: obj['middleware']
            });
        }


        return obj.path;

    }

    getControllers(controller_dir) {
        var controller_files = fs.readdirSync(controller_dir).filter(function(file) {
            if (typeof require(path.join(controller_dir, file)) == 'object') {
                return (file.indexOf(".") !== 0) && (file !== "index.js");
            } else {
                return false;
            }
        }).map(function(file) {
            var name = file.replace('.js', '').toLowerCase();
            var obj = require(path.join(controller_dir, file));
            var policy = [];
            var new_obj = {};
            for (var obj_key in obj) {
                if (Array.isArray(obj[obj_key])) {
                    new_obj[obj_key] = obj[obj_key];
                } else if (typeof obj[obj_key] == 'function') {
                    if (common.validateExpressHandler(obj[obj_key])) {
                        new_obj[obj_key] = obj[obj_key];
                    } else {
                        this.log('warn', 'Not enough supplied arguments for handler "' + obj_key + '" at "' + name + '" controller');
                    }

                } else {
                    this.log('warn', obj_key + ' is not a valid handler at "' + name + '" controller');
                }
            }
            return {
                name: name,
                obj: new_obj,
                type: 'controller',
                policy: policy
            };
        }.bind(this));
        return controller_files;
    }


}



module.exports = Controller;