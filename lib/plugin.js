"use strict";
const EventEmitter = require("events");
var path = require("path");
var fs = require("fs");
var common = require("./common");
var objectTree = require("object-tree");
var _ = require("lodash");
var ot = new objectTree();
var async = require('async');

class Plugin extends EventEmitter {
    constructor(own) {
        super();
        this.system = own;
        var c = this.config = own.c;
        this.plugin_path = c('path.plugins');
        this.app = own.app;
        this.io = own.io;
        this.root = own.root;
        this.hooks = own.hook.getAll();
        this.setPath = own.hook.get('setPath');
        this.getPath = own.hook.get('getPath');
        this.helpers = own.helpers;
        this.path = c('path');
        this.log = own.log;
        this.addHook = own.hook.addHook;
        this.common = own.common;
        this.controllers = own.controllers;
        this.middleware = own.middleware;
        this.model = own.model;
        this.fn = function (plugin) {
            return function (name, fn) {
                plugin._fn.push({
                    name: name,
                    fn: fn
                });
            }
        };
        this.run = function () {
            var args = arguments;
            var pass = [];
            for (var ctr = 1; args.length > ctr; ctr++) {
                pass.push(args[ctr]);
            }
            if (common.typeOf(args[0]) == 'string') {
                var splitted = args[0].split('.');
                var plugin = _.find(this.plugins,function (p) {
                    return p.name == splitted[0]
                });

                if(plugin){
                    var fn = _.find(plugin._fn,function (p) {
                        return p.name == splitted[1]
                    });
                    if(fn){
                        return fn.fn.apply(null,pass);
                    }else{
                        own.log('warn','cannot run '+splitted[1]+' function in '+plugin.name+' not found');
                    }
                }else{
                    own.log('warn','cannot run '+args[0]+' plugin not found');
                }

            }

        }.bind(this);
        this.db = own.db;
        this.common = own.common;
        this.plugins = [];
        this.validate_objs = ['package', 'init', 'config', 'provides', 'lifecycle'];
    }

    init(callback) {
        var plugin_inits = {};
        for (var key in this.plugins) {
            if (common.typeOf(this.plugins[key].module.init) == 'function') {
                this.plugins[key].module.init.bind(this.plugins[key]);
                plugin_inits[this.plugins[key].name] = async.apply(this.plugins[key].module.init, {
                    fn: this.fn(this.plugins[key]),
                    name: this.plugins[key].name,
                    module: this.plugins[key].module,
                    app: this.app,
                    system: this.system,
                    common: this.common,
                    helpers: this.helpers,
                    hooks: this.hooks,
                    addHook: this.addHook,
                    path: this.path,
                    setPath: this.setPath,
                    getPath: this.getPath,
                    io: this.app,
                    c: this.config,
                    models: this.model.models,
                    controllers: this.controllers,
                    middleware: this.middleware,
                    config: this.plugins[key]._config,
                    providers: {},
                });
            }
        }

        async.series(plugin_inits, function (err, plugin_init) {
            callback(err, plugin_init);
        });
    }


    loadPlugins(plugin_path) {
        plugin_path = (typeof plugin_path != 'string' ? this.plugin_path : plugin_path);
        var system = this;
        var plugin_files = fs.readdirSync(plugin_path).filter(function (file) {
            var plug_file = null;
            try {
                plug_file = require(path.join(plugin_path, file));
            } catch (e) {
                this.system.log('error', 'Could not load ' + file + ' Error:' + e.message);
            }
            if (plug_file && file.indexOf(".") !== 0 && (file !== "index.js"))
                return this.validatePlugin(plug_file);
            else
                return false;


        }.bind(this)).map(function (file) {
            var plugin_module = require(path.join(plugin_path, file));
            return {
                name: path.parse(file).name,
                module: plugin_module
            }
        }.bind(this));

        var plugin_order_tmp = [];
        var sorting = "toposort";
        switch (sorting) {
            case "toposort":
                for (var plugin_files_key in plugin_files) {

                    var plugin = plugin_files[plugin_files_key], push = true;
                    if (ot.lookup('package.dependencies', plugin.module) && Object.keys(ot.lookup('package.dependencies', plugin.module)).length > 0) {
                        var dependencies = ot.lookup('package.dependencies', plugin.module)
                        for (var dependencyKey in dependencies) {
                            var dependency = dependencies[dependencyKey];
                            plugin_order_tmp.push([plugin.name, dependencyKey]);
                        }
                    } else {
                        plugin_order_tmp.push([plugin.name
                            , null]);
                    }
                }
                plugin_order_tmp = common.toposort(common.uniqueNodes(plugin_order_tmp), plugin_order_tmp).reverse().filter(function (x) {
                    return x != null;
                });

                break;
        }

        plugin_order_tmp = plugin_order_tmp.map(function (p) {
            var mod = {};

            var temp_mod = _.find(plugin_files, function (o) {
                return o.name == p;
            });
            if (temp_mod) {
                if (temp_mod.hasOwnProperty('module')) {
                    mod = temp_mod.module;
                }
                for (var key_mod in mod) {
                    if (typeof mod[key_mod] == "function") {
                        mod[key_mod].bind(this);
                    }
                }

            }
            return {
                name: p,
                module: mod
            }

        }.bind(this));
        this.plugins = plugin_order_tmp;
        return this.plugins;


    }


    loadBefore() {
        for (var key in this.plugins) {
            if (common.typeOf(this.plugins[key].module.lifecycle) == 'object') {
                if (common.typeOf(this.plugins[key].module.lifecycle.before) == 'function') {
                    this.plugins[key].module.lifecycle.before.call(null, {
                        fn: this.fn(this.plugins[key]),
                        name: this.plugins[key].name,
                        module: this.plugins[key].module,
                        app: this.app,
                        system: this.system,
                        common: this.common,
                        helpers: this.helpers,
                        hooks: this.hooks,
                        addHook: this.addHook,
                        path: this.path,
                        setPath: this.setPath,
                        getPath: this.getPath,
                        io: this.app,
                        c: this.config,
                        models: this.model.models,
                        controllers: this.controllers,
                        middleware: this.middleware,
                        config: this.plugins[key]._config,
                        providers: {},
                    });
                }
            }
        }
    }

    loadServerStarted(server) {
        for (var key in this.plugins) {
            if (common.typeOf(this.plugins[key].module.lifecycle) == 'object') {
                if (common.typeOf(this.plugins[key].module.lifecycle.started) == 'function') {
                    this.plugins[key].module.lifecycle.started.call(null, {
                        fn: this.fn(this.plugins[key]),
                        name: this.plugins[key].name,
                        module: this.plugins[key].module,
                        app: this.app,
                        system: this.system,
                        common: this.common,
                        helpers: this.helpers,
                        hooks: this.hooks,
                        addHook: this.addHook,
                        path: this.path,
                        setPath: this.setPath,
                        getPath: this.getPath,
                        io: this.app,
                        c: this.config,
                        models: this.model.models,
                        controllers: this.controllers,
                        middleware: this.middleware,
                        config: this.plugins[key]._config,
                        providers: {},
                    }, server);
                }
            }
        }
    }

    loadHelpers() {

    }

    validatePlugin(plugin) {
        if (typeof plugin == 'object' && !(common.isEmpty(plugin))) {
            if (plugin.hasOwnProperty('init')) {
                if (common.typeOf(plugin.init) == 'function') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {

            return false;
        }


    }


    loadConfig(callback) {
        for (var key in this.plugins) {
            this.plugins[key]['_fn'] = [];
            if (common.isEmpty(this.plugins[key].module)) {
                this.plugins[key]['_config'] = {};

            } else {
                var config_setup = null;
                if (common.typeOf(this.plugins[key].module.config) == 'function') {
                    try {
                        config_setup = this.plugins[key].module.config();
                    } catch (e) {
                        console.log('warn', 'Could not load config');
                    }
                } else if (common.typeOf(this.plugins[key].module.config) == 'object') {
                    config_setup = this.plugins[key].module.config;
                }

                switch (common.typeOf(config_setup)) {
                    case 'array':
                        var config = [];
                        config_setup.forEach(function (config_setup) {
                            var temp_config = {};
                            if (typeof config_setup.config == 'object') {
                                temp_config = config_setup.config
                            }
                            if (typeof config_setup.inherit == 'object') {
                                for (var inherit_key in config_setup.inherit) {
                                    if (!common.isEmpty(this.system.c(config_setup.inherit[inherit_key]))) {
                                        temp_config[inherit_key] = this.system.c(config_setup.inherit[inherit_key]);
                                    }
                                }
                            }

                            var config_key = _.findIndex(config, function (o) {
                                return o.name === config_setup.name;
                            });
                            if (config_key === -1) {
                                this.system.c(config_setup.name, temp_config);
                                config.push({name: config_setup.name, config: temp_config});
                            } else {
                                this.system.log('warn', 'Duplicate config name: "' + config_setup.name + '" in [' + this.plugins[key].name + '] plugin');
                            }
                        }.bind(this));
                        if (config.length > 1) {
                            var temp_o = {};
                            for (var c_key in config) {
                                this.plugins[key].module['_config'] = {};
                                var o_con_keys = Object.keys(config[c_key].config);
                                o_con_keys.forEach(function (o_con_key) {
                                    temp_o[o_con_key] = config[c_key].config[o_con_key];
                                }.bind(this));

                            }
                            this.plugins[key]['_config'] = temp_o;
                        } else {
                            this.plugins[key]['_config'] = config[0].config;
                        }


                        break;
                    case 'object':
                        this.plugins[key]['_config'] = {};
                        var temp_config = {};
                        if (common.typeOf(config_setup.config) == 'object') {
                            temp_config = config_setup.config;
                        }

                        if (common.typeOf(config_setup.config) == 'object') {
                            for (var inherit_key in config_setup.inherit) {
                                if (!common.isEmpty(this.system.c(config_setup.inherit[inherit_key]))) {
                                    temp_config[inherit_key] = this.system.c(config_setup.inherit[inherit_key]);
                                }
                            }
                        }
                        this.plugins[key]['_config'] = temp_config;
                        break;
                }
            }

        }
    }

    validate() {


    }


}

module.exports = Plugin;


