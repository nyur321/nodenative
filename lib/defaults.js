"use strict";

module.exports = {
    "ssl": {
        enable: false,
        certificates: null
    },
    "database": {
        "connectors":{
            "default":"sequelize",
            "sequelize":{},
            "mongoose":{
                "uri":""
            }
        },
        "host": "mysql",
        "port": "mysql",
        "name": "Shindb",
        "username": "root",
        "password": ""
    },
    "web": {
        "port": 3000,
        "host": null,
        "views": {},
        "cluster":0
    },
    "globals":{
        "set_global_variable": "$" //default is $
    },
    "views":{

    },
    "assets": {
        "bower_url": "",
        "assets_url": "/components",
        "overrides": { //Overrides packages bower.json. This is used when a package is missing a main config
            "bootstrap": {
                "main": [
                    "dist/css/bootstrap.min.css",
                    "dist/js/bootstrap.min.js"
                ]
            }
        },
        "themes": [{
            name: 'default', // Set name of the theme
            "includes": { // Assets to include to this theme. Set your own custom css and js files
                "css": ["/public/css/main.css"],
                "js": ["/public/js/main.js"]
            },
            excludes: ['/AdminLTE/'] // Array of bower packages to exclude from the theme.
        }]
    }
};