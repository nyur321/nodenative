'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var common = require('./common');



class Model {
    constructor(system){
        this.path = system.c('path.model');
        this.db = system.db;
        this.log = system.log;
        this.dataTypes = {};
        this.defaultConnector = system.c('database.connectors.default');
        this.models = {};
        this.models_list = [];
    }


    getModels(){
        return this.models;
    }
    
    
    init(connector){
        var models_path = this.path;
        this.defaultConnector = connector || this.defaultConnector;
        if (fs.existsSync(models_path)) {
            fs.readdirSync(models_path).filter(function (file) {
                return (file.indexOf(".") !== 0) && (file !== "index.js");
            }).forEach(function (file) {
                var model = {
                    connector:this.defaultConnector,
                    name:path.parse(file).base.replace('.js','').toLowerCase(),
                    attributes:null,
                    options:{}
                };

                var model_file = require(path.join(models_path,file));

                if(model_file.hasOwnProperty('name')){
                    model.name = model_file.name;
                }
                if(model_file.hasOwnProperty('options')){
                    model.options = model_file.options;
                }
                if(!common.isEmpty(model_file['connector'])){
                    model.connector = model_file['connector'];
                }
                if(!common.isEmpty(model_file['events'])){
                    model.events = model_file['events'];
                }

                if(common.isEmpty(model.connector)){
                    console.log(model.name + ' needs to specifiy a connector')
                }else{
                    if(this.db.dataTypes[model.connector]){
                        model.attributes = model_file.attributes(this.db.dataTypes[model.connector]);
                    }
                    this.models[model.name] = this.db.models[model.connector][model.name] = this.db.model_map[model.connector](model,this.db.connectors[model.connector]);
                    model._self = this.models[model.name];
                }
                this.models_list.push(model);

            }.bind(this));
        }


        return this.models;


    }




    onLoad(callback){
        var promises = [];
        var async_chain = [];
        Object.keys(this.db.events).forEach(function(connector){
            var obj = this.db.events[connector].onLoad(this.db.connectors[connector], this.db.models[connector],this.db.config[connector]);
            if(!common.isEmpty(obj)){
                switch (common.typeOf(obj)){
                    case 'function':
                        async_chain.push(obj);
                        break;
                }
            }
        }.bind(this));

        async.series(async_chain,function(err,result){
            this.events('onModelLoad');
           callback(err,result);
        }.bind(this));
    }


    events(e){
        switch (e){
            case 'onModelLoad':
                this.models_list.forEach(function (m) {
                    if(m.events){
                        if(common.typeOf(m.events['onModelLoad']) == 'function'){
                            m.events['onModelLoad'](this.models);
                        }
                    }
                }.bind(this));

                break;
            case '':


                break;
        }
    }






}

module.exports = Model;

 function e (own) {
     var c = own.c;
     var controllers = own.controllers;
     var models_path = own.config.path.model;



     var sql_mode = (process.env.SQL_MODE || 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION').split(',');
     var set_global_sql_mode = true;
     var remove_sql_mode = (process.env.REMOVE_SQL_MODE ? process.env.REMOVE_SQL_MODE.split(',') : ['ONLY_FULL_GROUP_BY']);




 };