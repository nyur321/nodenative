"use strict";

var path = require('path');
var fs = require('fs');
var async = require('async');
var _ = require('lodash');
var objectTree = require('object-tree');
var ot = new objectTree();
var common = require('./common');

class Hook {
    constructor(system, ClassInstance) {
        this.hooks = {};
        this.system = system;
        this.registry = system.registry;
        this.ClassInstance = ClassInstance;
        this.initialized = false;
        this.defaults = {
            'expose': ['c', 'addGlobal', 'setGlobal']
        }
    }


    init() {
        var ClassInstance = this.ClassInstance;
        Object.keys(predefined_hooks).forEach(function (hook) {
            this.hooks[hook] = function () {
                var args = [this];
                for (var ctr = 0; ctr < arguments.length; ctr++) {
                    if (!(arguments[ctr] instanceof ClassInstance))
                        args.push(arguments[ctr]);
                }
                return predefined_hooks[hook].apply(this, args);
            }.bind(this.system);
        }.bind(this));
        this.initialized = true;

    }

    addHook(name, fn) {
        if (this.initialized) {
            var own = this.system;
            var ClassInstance = this.ClassInstance;
            if (name in this.hooks) {
                own.log('warn', 'Hook "' + name + '" has already been defined');
            } else {
                own.registry.hooks.push(name);
                this.hooks[name] = function () {
                    var args = [own];
                    for (var ctr = 0; ctr < arguments.length; ctr++) {
                        if (!(arguments[ctr] instanceof ClassInstance))
                            args.push(arguments[ctr]);
                    }
                    return fn.apply(this.system, args);
                }.bind(this.system);
                return true;
            }
            return true;
        } else {
            return false;
        }


    }


    getAll() {
        return this.hooks;
    }

    get(hook) {
        if (hook in this.hooks)
            return this.hooks[hook];
        else return false;
    }

    removeHook() {


    }

    attach(obj,binder) {
        if (this.initialized) {
            for(var key in obj) {
                var own = this.system;
                var ClassInstance = this.ClassInstance;
                if (key in this.hooks) {
                    own.log('warn', 'Hook "' + name + '" has already been defined');
                } else {
                    own.registry.hooks.push(name);
                    this.hooks[key] = function () {

                    }.bind(this.system);
                    return true;
                }
                return true;
            }
        } else {
            return false;
        }
    }

    expose(name) {
        if (!this.initialized) {
            this.system.log('warn' + 'Hooks not yet initialized');
        } else {
            if (common.typeOf(name) == 'string' && common.typeOf(this.system) != 'undefined') {
                if (name in this.hooks) {
                    this.system[name] = this.hooks[name];
                    return true;
                } else {
                    this.system.log('warn' + 'No ' + name + ' hook found.');
                }
            } else {
                this.system.log('warn', 'Invalid parameters in hook.expose [object,hook_name]');
            }
        }
        return false;
    }



}


module.exports = Hook;

var predefined_hooks = {
    loadEvents:function(system,event,options,callback){
        if(common.typeOf(event) == 'string'){
            async.series(system.events[event],function (err,results) {
                system.emit('loaded',{
                    name:event,
                    type:options.type || 'procedure'
                });
                if(common.typeOf(callback) == 'function'){
                    callback(err,results);
                }

            });
        }
        
    },
    pushEvents:function(system,type,fn){
        if(type in system.events){
            system.events[type].push(fn);
        }else{
            system.log('warn','Invalid event type "'+type+'" in push events');
        }

    },
    handler: function (system, name, fn) {


    },
    c: function (system) {
        var system = this;
        var args = arguments;
        if (typeof args[2] != 'undefined') {
            return ot.set(args[1], args[2], system.config);
        } else {
            return ot.lookup(args[1], system.config);
        }
    },
    setGlobal: function () {

    },
    addGlobal: function (system, name, global_variable) {
        if (name in system.$) {
            system.log('warn', 'Global variable "' + name + '" is already defined');
        } else {
            system.registry.globals.push(name);
            system.$[name] = global_variable;
        }
    },
    addHelper: function (system, name, fn, is_not_async) {

    },
    getPath: function (system, name) {
        return system.c('path.' + name);
    },
    setPath: function (system, name, path_variable, override) {
        var push = true;
        if(!override){
            if (name in system.c('path')) {
                system.log('warn', 'Path "' + name + '" has already been defined');
                push = false;
            }
        }
        if (push) {
            this.registry.path.push(name);
            this.c('path.' + name, path.join(system.root, path_variable));
            return true;
        }


    },
    registerDatabase: function (system, options) {
        system.db['connectors'][options.name] = options.connector(options.config);
        system.db['dataTypes'][options.name] = options.dataTypes;
        system.db['model_map'][options.name] = options.model;
        system.db['config'][options.name] = options.config;
        system.db['events'][options.name] = {
            'onLoad': options.onLoad
        };
        system.db['models'][options.name] = {};
    },
    registerOrm:function (system,name,options,config) {
        system.db['connectors'][name] = options.connector(options.config);
        system.db['dataTypes'][name] = options.dataTypes;
        system.db['model_map'][name] = options.model;
        system.db['config'][name] = options.config;
        system.db['models'][name] = {};
        system.db['events'][name] = {
            'onLoad': options.load
        };
    },
    registerExtension: function (system, defaultExtension) {
        var extensions_list = [], app = system.app;
        if (typeof defaultExtension == "object") {
            var ext = defaultExtension.ext, registered = false;
            if (defaultExtension.hasOwnProperty("extended")) {
                defaultExtension.extended.forEach(function (extended) {
                    if (typeof extended.params == "object" ||
                        typeof extended.params == "function" ||
                        typeof extended.params == "string") {
                        if (Array.isArray(extended.params)) {
                            if (typeof ext[extended.name] == "function") {
                                app.use(ext[extended.name].apply(null, extended.params));
                                extensions_list.push({name: extended.name, config: extended.params});
                                registered = true;
                            }
                        } else {
                            app.use(ext[extended.name](extended.params));
                            extensions_list.push({name: extended.name, config: extended.params});
                            registered = true;
                        }

                    } else {
                        app.use(ext[extended.name]());
                        registered = true;
                    }

                });
            } else {
                if (typeof defaultExtension.params == "object" ||
                    typeof defaultExtension.params == "function" ||
                    typeof defaultExtension.params == "string") {
                    if (Array.isArray(defaultExtension.params)) {
                        app.use(ext.apply(null, defaultExtension.params));
                        extensions_list.push({name: defaultExtension.name, config: defaultExtension.params});
                        registered = true;
                    } else {
                        app.use(ext(defaultExtension.params));
                        extensions_list.push({name: defaultExtension.name, config: defaultExtension.params});
                        registered = true;
                    }


                } else {
                    app.use(ext());
                    registered = true;
                }
            }


        }

        if (registered) {
            if (typeof defaultExtension == 'object') {
                system.log('debug', 'Extension registered: ' + defaultExtension.name);
                system.registry.extensions.push(defaultExtension);
            }

        }


    }


};