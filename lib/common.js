"use strict";
var _ = require('lodash');

module.exports = {
    groupBy: function (dataToGroupOn, fieldNameToGroupOn, fieldNameForGroupName, fieldNameForChildren) {
        var result = _.chain(dataToGroupOn)
            .groupBy(fieldNameToGroupOn)
            .toPairs()
            .map(function (currentItem) {
                return _.zipObject([fieldNameForGroupName, fieldNameForChildren], currentItem);
            })
            .value();
        return result;
    },
    getBrackets: function (input) {
        var regex = /\[([^[]+)\].*|R/;
        var matches = input.match(regex);
        var trimmed = '';
        if (matches != null) {
            trimmed = matches.input.replace(matches[0], '');
        } else {
            trimmed = input;
        }
        var data = [];

        while (matches != null) {
            var temp_data = matches;
            data.push(temp_data[1]);
            temp_data = temp_data[0].replace(temp_data[1], '');
            matches = temp_data.match(regex);
        }
        return [data, trimmed];
    },
    getPeriods: function (input) {
        var m = input.match(/^[^.]*/)[0];
        var remainder = input.replace(m, '');
        var ar = _.filter(remainder.split('.'), function (o) {
            return o != null && o != '';
        });

        return [m, ar, input];


    },
    uniqPushMerge: function (input, current) {
        var temp_array = current;

        //console.log(_.differenceBy(temp_array, [input]));

        temp_array.push(input);


        return _.uniq(temp_array);

    },
    uniqBy: function (a, key) {
        var seen = {};
        return a.filter(function (item) {
            var k = key(item);
            return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        })
    },
    filterKey: function (obj, keep) {
        var result = {};
        for (var key in obj) {
            if (keep.indexOf(key) != -1) {
                result[key] = obj[key];
            }
        }
        return result;
    },

    validateExpressHandler(fn) {
        var args = /function[^(]*\(([^)]*)\)/.exec((typeof fn == "string" ? fn : fn.toString()));
        var args_arr = [];
        if (args) {
            if (args[1] != "") {
                args_arr = args[1].split(/\s*,\s*/);
            }

            if (args_arr.length >= 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },

    "uniqueNodes": function (arr) {
        var res = [];
        for (var i = 0, len = arr.length; i < len; i++) {
            var edge = arr[i]
            if (res.indexOf(edge[0]) < 0) res.push(edge[0])
            if (res.indexOf(edge[1]) < 0) res.push(edge[1])
        }
        return res
    },

    "toposort": function (nodes, edges) {
        var cursor = nodes.length
            , sorted = new Array(cursor)
            , visited = {}
            , i = cursor

        while (i--) {
            if (!visited[i]) visit(nodes[i], i, [])
        }

        return sorted;

        function visit(node, i, predecessors) {
            if (predecessors.indexOf(node) >= 0) {
                throw new Error('Cyclic dependency: ' + JSON.stringify(node))
            }

            if (!~nodes.indexOf(node)) {
                throw new Error('Found unknown node. Make sure to provided all involved nodes. Unknown node: ' + JSON.stringify(node))
            }

            if (visited[i]) return;
            visited[i] = true

            // outgoing edges
            var outgoing = edges.filter(function (edge) {
                return edge[0] === node
            })
            if (i = outgoing.length) {
                var preds = predecessors.concat(node)
                do {
                    var child = outgoing[--i][1]
                    visit(child, nodes.indexOf(child), preds)
                } while (i)
            }

            sorted[--cursor] = node
        }
    },

    "isEmpty": function (obj) {
        switch (this.typeOf(obj)) {
            case 'object':
                return Object.keys(obj).length === 0;
                break;
            case 'array':
                return Object.keys(obj).length === 0;
                break;
            case 'string':
                return obj === "";
            default:
                return this.typeOf(obj) == 'null' || this.typeOf(obj) == 'undefined';
        }
    },

    "typeOf": function (obj) {
        return {}.toString.call(obj).split(' ')[1].slice(0, -1).toLowerCase();
    },

    "urlJoin": function () {
        var startsWith = function (str, searchString) {
            return str.substr(0, searchString.length) === searchString;
        };

        var normalize = function (str, options) {

            if (startsWith(str, 'file://')) {

                // make sure file protocol has max three slashes
                str = str.replace(/(\/{0,3})\/*/g, '$1');
            } else {

                // make sure protocol is followed by two slashes
                str = str.replace(/:\//g, '://');

                // remove consecutive slashes
                str = str.replace(/([^:\s\%\3\A])\/+/g, '$1/');
            }

            // remove trailing slash before parameters or hash
            str = str.replace(/\/(\?|&|#[^!])/g, '$1');

            // replace ? in parameters with &
            str = str.replace(/(\?.+)\?/g, '$1&');

            return str;
        };


        var input = arguments;
        var options = {};

        if (typeof arguments[0] === 'object') {
            input = arguments[0];
            options = arguments[1] || {};
        }

        var joined = [].slice.call(input, 0).join('/');
        return normalize(joined, options);


    },

    "extend": function () {
        var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {},
            i = 1,
            length = arguments.length,
            deep = false,
            toString = Object.prototype.toString,
            hasOwn = Object.prototype.hasOwnProperty,
            push = Array.prototype.push,
            slice = Array.prototype.slice,
            trim = String.prototype.trim,
            indexOf = Array.prototype.indexOf,
            class2type = {
                "[object Boolean]": "boolean",
                "[object Number]": "number",
                "[object String]": "string",
                "[object Function]": "function",
                "[object Array]": "array",
                "[object Date]": "date",
                "[object RegExp]": "regexp",
                "[object Object]": "object"
            },
            jQuery = {
                isFunction: function (obj) {
                    return jQuery.type(obj) === "function"
                },
                isArray: Array.isArray ||
                function (obj) {
                    return jQuery.type(obj) === "array"
                },
                isWindow: function (obj) {
                    return obj != null && obj == obj.window
                },
                isNumeric: function (obj) {
                    return !isNaN(parseFloat(obj)) && isFinite(obj)
                },
                type: function (obj) {
                    return obj == null ? String(obj) : class2type[toString.call(obj)] || "object"
                },
                isPlainObject: function (obj) {
                    if (!obj || jQuery.type(obj) !== "object" || obj.nodeType) {
                        return false
                    }
                    try {
                        if (obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                            return false
                        }
                    } catch (e) {
                        return false
                    }
                    var key;
                    for (key in obj) {
                    }
                    return key === undefined || hasOwn.call(obj, key)
                }
            };
        if (typeof target === "boolean") {
            deep = target;
            target = arguments[1] || {};
            i = 2;
        }
        if (typeof target !== "object" && !jQuery.isFunction(target)) {
            target = {}
        }
        if (length === i) {
            target = this;
            --i;
        }
        for (i; i < length; i++) {
            if ((options = arguments[i]) != null) {
                for (name in options) {
                    src = target[name];
                    copy = options[name];
                    if (target === copy) {
                        continue
                    }
                    if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && jQuery.isArray(src) ? src : []
                        } else {
                            clone = src && jQuery.isPlainObject(src) ? src : {};
                        }
                        // WARNING: RECURSION
                        target[name] = extend(deep, clone, copy);
                    } else if (copy !== undefined) {
                        target[name] = copy;
                    }
                }
            }
        }
        return target;

    }


}