"use strict";
var path = require("path");
var fs = require("fs");


class Loader{
    constructor(options) {
        this.root = options.root;


    }

    path(input) {
        var own = this;
        return path.join(own.root,input);
    }

    require(input){
        return require(this.path(input));
    }


    requireAll(root,module_name){
        var reqs = [];
        module_name.forEach(function(mod){
            reqs.push({
                name:module_name,
                file:require(path.join(root,module_name))
            });
        });
        return reqs;
    }

    config_file(config,files_path){
        var configs = {};
        if (fs.existsSync(files_path)) {

            fs.readdirSync(files_path).filter(function (file) {
                return (file.indexOf(".") !== 0) && (file !== "index.js");
            }).forEach(function (file) {

                if(path.parse(file).ext == '.js'){
                    var config_file = require(path.join(files_path,file));
                    //console.log();
                    if(typeof config_file == 'object'){
                        configs[path.parse(file).name] = config_file[config_file.env || 'dev'];
                    }
                }
            });
        }
        return configs;
    }

    config(parent,inputs,callback){
        var configs = [];
        inputs.forEach(function(input){
            var config = {};
            
        });

        callback(null,true);
    }


};


module.exports = function (options) {
    var defaults = {
        root: path.join(__dirname,"../")
    };
    return new Loader(defaults);
};