var socketio = require("socket.io");
var _ = require("lodash");
var common = require("./common");
class Socket {
    constructor(controller) {
        this.io = socketio;
        this.sockets = controller.sockets;
    }

    init(server) {
        this.io(server);
    }

    room(ns) {
        return this.io.connect(ns, {
            query: 'ns=' + ns,
            resource: "socket.io"
        });
    }

    load() {
        var self = this;
        var temp_socket = {};
        for (var key in this.sockets) {
            var socket_nsp = this.sockets[key];
            var key_nsp = socket_nsp.parent + '/' + socket_nsp.nsp;
            temp_socket[key_nsp] = self.io.of('/' + key_nsp);
            var obj = socket_nsp.obj;
            for (var obj_key in obj) {
                switch (common.typeOf(obj[obj_key])) {
                    case "array":
                        obj[obj_key].forEach(function (midware) {
                            if (typeof self.middleware[midware] == 'function') {
                                temp_socket[key_nsp].use(self.middleware[midware]);
                            }
                            if (typeof self.middleware[midware] == 'string') {
                                var middleware = _.find(self.middleware, function (o) {

                                });
                            }
                        });
                        break;
                    case "function":
                        temp_socket[key_nsp].on('connection', obj[obj_key]);

                        break;
                }
            }
        }
        return temp_socket;
    }
}

module.exports = Socket;