var nexe = require('nexe'),
    program = require("commander"),
    dotenv = require("dotenv").config();

program
    .version('0.1.0')
    .option('-c, --build [type]', 'Build the executable version')
    .parse(process.argv);



nexe.compile({
    input: 'main.js', // where the input file is
    output: './bin/nodenative.exe', // where to output the compiled binary
    nodeVersion: process.env.BUILD_NODE_VERSION || '5.6.0', // node version
    //nodeTempDir: 'src', // where to store node source.
    //nodeConfigureArgs: ['opt', 'val'], // for all your configure arg needs.
    //nodeMakeArgs: ["-j", "4"], // when you want to control the make process.
    //nodeVCBuildArgs: ["nosign", "x64"], // when you want to control the make process for windows.
                                        // By default "nosign" option will be specified
                                        // You can check all available options and its default values here:
                                        // https://github.com/nodejs/node/blob/master/vcbuild.bat
    //python: 'path/to/python', // for non-standard python setups. Or python 3.x forced ones.
    //resourceFiles: ['path/to/a/file'], // array of files to embed.
    resourceRoot: ['assets','plugins'], // where to embed the resourceFiles.
    flags: true, // use this for applications that need command line flags.
    jsFlags: "--use_strict", // v8 flags
    startupSnapshot: 'path/to/snapshot.js', // when you want to specify a script to be
                                            // added to V8's startup snapshot. This V8
                                            // feature deserializes a heap to save startup time.
                                            // More information in this blog post:
                                            //
                                            // http://v8project.blogspot.de/2015/09/custom-startup-snapshots.html
    framework: "node" // node, nodejs, or iojs
}, function (err) {
    if (err) {
        return console.log(err);
    }

    // do whatever
});