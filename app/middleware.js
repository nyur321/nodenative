var passport = $.run('express-basics.passport');
var _ = require('lodash');
var LocalStrategy = require('passport-local').Strategy;
passport.use('local-login',new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false
    },
    function (email, password,done) {
        $.models.users.authenticate(email,password).then(function (auth) {
            return done(null,auth);
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (id, done) {
    done(null, id);
});

function onAuthorizeSuccess(data, accept){
    // console.log('successful connection to socket.io');

    // The accept-callback still allows us to decide whether to
    // accept the connection or not.
    accept(null, true);

    // OR

    // If you use socket.io@1.X the callback looks different
    // accept();
}

function onAuthorizeFail(data, message, error, accept){
    if(error)
        throw new Error(message);
    // console.log('failed connection to socket.io:', message);

    // We use this callback to log all of our failed connections.
    accept(null, false);

    // OR

    // If you use socket.io@1.X the callback looks different
    // If you don't want to accept the connection
    // if(error)
    //     accept(new Error(message));
    // this error will be sent to the user as a special error-package
    // see: http://socket.io/docs/client-api/#socket > error-object
}


var expressBasics = _.find($.plugin.plugins,function (o) {
    return o.name == 'express-basics';
});

module.exports = {

    /**
     * This is the Default middleware all controllers and routes uses the default middleware
     * */

    'default': function (req, res, next) {
        // console.log($.run('express-session-auth.passport'));
        res.locals.def = {
            thiss:' is defaults'
        }

        next();
    },

    'api': function (req, res, next) {
        var anonymous_urls = [
            'contact/submit',
            'feedback/submit',
        ];
        res.locals['authenticated'] = false;
        if(req.isAuthenticated()){
            res.locals['authenticated'] = req.isAuthenticated();
            next();
        }else{
            
            res.send({
                error:'unauthorized'
            });
        }

    },
    'admin':function (req,res,next) {
        if(req.isAuthenticated()){
            if(req.user.role_id == 1 || req.user.role_id == 2){
                res.locals['authenticated'] = req.isAuthenticated();
                next();
            }else{

                res.send({
                    error:'unauthorized'
                });
            }
        }else{

            res.send({
                error:'unauthorized'
            });
        }
    },
    'sample':function (req,res,next) {
        res.locals.from = {
            sample:'yesss'
        };

        next();
    },
    'login': function (req, res, next) {


        next();
    },

    'socket:passport-socket':function (socket,next) {
        return $.run('express-basics.passport-socket',{
            success:      onAuthorizeSuccess,  // *optional* callback on success - read more below
            fail:         onAuthorizeFail
        })(socket,next);
    },
    'role': function (req,res,next) {
        $.models.roles.findOne({
            where:{
                id:req.user.role_id
            }
        }).then(function (role) {
            res.locals.role = role;
            next();
        });

    },
    'passport': function (req,res,next) {
        var data = {};
        if(data['login_retries'] != null || data['login_retries'] != 'undefined'){
            
        }else{
            data['login_retries'] = 0;
        }
        
        if(req.isAuthenticated()){
            data['authenticated'] = true;
            data['validations'] = [];
            data['user'] = req.user;
            data['error'] = 'already_logged_in';
            res.locals['data_login'] = data;
            next();
        }else{
            req.checkBody({
                email: {
                    notEmpty: {
                        errorMessage: 'Email is required'
                    },
                    isEmail: {
                        errorMessage: 'Please input a valid email'
                    }
                },
                password: {
                    notEmpty: true,
                    errorMessage: 'Password is required'
                }
            });
            req.getValidationResult().then(function (result) {
                return new Promise(function (resolve,reject) {
                    resolve(result.array())
                });
            }).then(function (validations) {
                data['validations'] = validations;
                data['user'] = false;
                data['error'] = null;
                if(validations.length > 0){
                    res.locals['data_login'] = data;
                    next();
                }else{
                    passport.authenticate('local-login', function(err, user, info) {
                        console.log(err);
                        data['user'] = user;
                        data['error'] = err;
                        res.locals['data_login'] = data;
                        if(user.validated == 1){
                            req.logIn(user, function(err) {
                                next();
                            });
                        }else{
                            next()
                        }
                    })(req, res, next);
                }

            });
        }

    }


    /** You can add more middleware here*/


};