"use strict";
var async = require('async');


module.exports = [{
        method: ['GET'],
        path: '/',
        use: $.run('express-react.router', {
            base_url: '/app',

        }),
        middleware: [function(req, res, next) {
            res.locals.props_user = req.user || {
                "role_id": 4,
                "role_name": "Anonymous",
                "department_id": null,
                "department_name": false,
                "validated": 0,
                "anonymous": 1
            };
            async.series({
                'feedbacktypes': function(callback) {
                    $.models.feedbacktypes.findAll().then(function(results) {
                        var data = [];
                        results.forEach(function(result) {
                            data.push({
                                value: result.id,
                                label: result.type,
                            });
                        });
                        callback(null, data);
                    });
                },
                roles: function(callback) {
                    $.models.roles.findAll().then(function(results) {
                        var data = [];
                        results.forEach(function(result) {
                            data.push({
                                value: result.id,
                                label: result.role,
                            });
                        });
                        callback(null, data);
                    });
                },
                'departments': function(callback) {
                    $.models.departments.findAll().then(function(results) {
                        var data = [];
                        results.forEach(function(result) {
                            data.push({
                                value: result.id,
                                label: result.name,
                            });
                        });
                        callback(null, data);
                    });
                }
            }, function(err, results) {
                res.locals.props_forms = {
                    departments: results.departments,
                    roles: results.roles,
                    feedbacktypes: results.feedbacktypes,
                };
                next();
            });
        }],
        params: {
            title: 'Online Public Assistance and Complaints Desk'
        }
    }, {
        method: ['GET'],
        path: '/socks',
        handler: "Main.index",
        middleware: ['sample'],
        params: {
            title: 'Home'
        }
    }, {
        method: ['GET'],
        path: '/routes',
        type: 'socket',
        handler: function(socket) {
            console.log(socket);
        }
    }


];