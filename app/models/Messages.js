"use strict";

module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            message:dataTypes.TEXT,
            type:dataTypes.INTEGER,
            from:dataTypes.INTEGER,
            ticket_id:dataTypes.INTEGER,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.messages.belongsTo(models.users,{foreignKey:'from'});
                models.messages.belongsTo(models.messagetype,{foreignKey:'type'});
                models.messages.belongsTo(models.tickets,{foreignKey:'ticket_id'});
                // models.messages.hasMany(models.attachments,{foreignKey:'message_id'});
            },
            getPerson:function (id) {
                // console.log(this)
            },
            submit:function (options) {
                return this.models.messages.create({
                    message:options.message,
                    from:options.from,
                    type:options.type,
                    ticket_id:options.ticket_id || null
                });
            }
        },
        instanceMethods: {}
    }
};