"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            user_id:{
                type:dataTypes.INTEGER,
                unique:true,
            },
            first_name:dataTypes.STRING,
            last_name:dataTypes.STRING,
            middle_name:dataTypes.STRING,
            address_reg:dataTypes.INTEGER,
            address_prov:dataTypes.INTEGER,
            address_citymun:dataTypes.INTEGER,
            address_brgy:dataTypes.INTEGER,
            address_street_num:dataTypes.STRING,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.persons.belongsTo(models.users,{foreignKey:'user_id'});
            },
            getPerson:function (id) {
             
            }
        },
        instanceMethods: {}
    }
};