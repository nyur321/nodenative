"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            ticket_id:dataTypes.INTEGER,
            department_id:dataTypes.INTEGER,
            forwarded_by:dataTypes.INTEGER,
            comments:dataTypes.TEXT
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.forwardhistory.belongsTo(models.departments,{foreignKey:'department_id'});
                models.forwardhistory.belongsTo(models.tickets,{foreignKey:'ticket_id'});
                models.forwardhistory.belongsTo(models.users,{foreignKey:'forwarded_by'});
            },
            submit:function (options) {
                return this.models.forwardhistory.findOrCreate({
                    where: {
                        ticket_id: options.ticket_id,
                        forwarded_by:options.forwarded_by,
                        comments:options.comments,
                        department_id:options.department_id
                    },
                }).spread(function(user,created) {
                    // if(!created){
                    //         this.models.forwardhistory.create({
                    //         comments:options.comments,
                    //     })
                    // }
                });
            }
        },
        instanceMethods: {}
    }
};