"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            room_id:dataTypes.INTEGER,
            user_id:dataTypes.INTEGER,
            message:dataTypes.TEXT,

        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.chatmessages.belongsTo(models.chatrooms,{foreignKey:'room_id'});
                models.chatmessages.belongsTo(models.users,{foreignKey:'user_id'});
            }
        },
        instanceMethods: {}
    }
};