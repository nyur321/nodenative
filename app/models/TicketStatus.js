"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            status:dataTypes.STRING,
            label:dataTypes.STRING,
            description:dataTypes.TEXT
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.ticketstatus.hasMany(models.tickets,{foreignKey:'status_id'});
                models.ticketstatus.hasMany(models.feedbacks,{foreignKey:'status_id'});
            }
        },
        instanceMethods: {}
    }
};