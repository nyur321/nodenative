"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            ticket_id:dataTypes.INTEGER,
            department_id:dataTypes.INTEGER,
            approved_by:dataTypes.INTEGER,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.approvedtickets.belongsTo(models.departments,{foreignKey:'department_id'});
                models.approvedtickets.belongsTo(models.tickets,{foreignKey:'ticket_id'});
                models.approvedtickets.belongsTo(models.users,{foreignKey:'approved_by'});
            },
            submit:function (options) {
                return this.models.approvedtickets.findOrCreate({
                    where: {
                        ticket_id: options.ticket_id,
                        approved_by:options.approved_by,
                        department_id:options.department_id
                    },
                }).spread(function(user,created) {
                    // if(!created){
                    //         this.models.forwardhistory.create({
                    //         comments:options.comments,
                    //     })
                    // }
                });
            }
        },
        instanceMethods: {}
    }
};