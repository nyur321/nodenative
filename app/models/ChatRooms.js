"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            type:dataTypes.STRING
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.chatrooms.hasMany(models.chattypes,{foreignKey:'type'});
                models.chatrooms.hasMany(models.chatsupports,{foreignKey:'room_id'});
                models.chatrooms.hasMany(models.chatmessages,{foreignKey:'room_id'});
            }
        },
        instanceMethods: {}
    }
};