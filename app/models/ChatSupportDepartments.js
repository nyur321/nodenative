"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            chat_support_id:dataTypes.INTEGER,
            department_id:dataTypes.INTEGER
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.chatsupportdepartments.belongsTo(models.chatsupports,{foreignKey:'chat_support_id'});
                models.chatsupportdepartments.belongsTo(models.departments,{foreignKey:'department_id'});
            }
        },
        instanceMethods: {}
    }
};