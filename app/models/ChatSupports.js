"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            reference_id:dataTypes.STRING,
            room_id:dataTypes.INTEGER,
            user_id:dataTypes.INTEGER,
            subject:dataTypes.TEXT,
            status:dataTypes.INTEGER,
            rating:dataTypes.INTEGER,
            comment:dataTypes.STRING,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.chatsupports.belongsTo(models.users,{foreignKey:'user_id'});
                models.chatsupports.belongsTo(models.chatrooms,{foreignKey:'room_id'});
                models.chatsupports.belongsTo(models.chatstatus,{foreignKey:'status'});
                models.chatsupports.hasMany(models.chatsupportdepartments,{foreignKey:'chat_support_id'});
            }
        },
        instanceMethods: {}
    }
};