"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            status:dataTypes.STRING,
            label:dataTypes.STRING,
            description:dataTypes.STRING
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.chatstatus.hasMany(models.chatsessions,{foreignKey:'status'});
            }
        },
        instanceMethods: {}
    }
};