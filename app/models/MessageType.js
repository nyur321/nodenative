"use strict";

module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            type:dataTypes.STRING,
            label:dataTypes.STRING,
            description:dataTypes.TEXT,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.messagetype.hasMany(models.messages,{foreignKey:'type'});
            },
            getPerson:function (id) {
                // console.log(this)
            },
            contact:function (options) {
                return this.models.messages.create({
                    message:options.message,
                    from:options.from.id,
                    subject:options.subject,
                    department_id:options.department_id || null
                });
            }
        },
        instanceMethods: {}
    }
};