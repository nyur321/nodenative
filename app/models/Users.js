"use strict";

var bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    attributes: function(dataTypes) {
        return {
            email: {
                type: dataTypes.STRING
            },
            password: dataTypes.STRING,
            role_id: dataTypes.INTEGER,
            department_id: dataTypes.INTEGER,
            remember_token: dataTypes.STRING,
            validated: dataTypes.INTEGER,
            anonymous: dataTypes.INTEGER,
        }
    },
    options: {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        classMethods: {
            associate: function(models) {
                models.users.hasMany(models.messages, {
                    foreignKey: 'from'
                });
                models.users.hasMany(models.tickets, {
                    foreignKey: 'from'
                });
                models.users.hasMany(models.forwardhistory, {
                    foreignKey: 'forwarded_by'
                });
                models.users.belongsTo(models.roles, {
                    foreignKey: 'role_id'
                });
                models.users.belongsTo(models.departments, {
                    foreignKey: 'department_id'
                });
                models.users.hasOne(models.persons, {
                    foreignKey: 'user_id'
                });
                models.users.hasMany(models.chatsessions, {
                    foreignKey: 'user_id'
                });
                models.users.hasMany(models.chatsupports, {
                    foreignKey: 'user_id'
                });
                models.users.hasMany(models.chatmessages, {
                    foreignKey: 'user_id'
                });
            },
            authenticate: function(email, password) {
                return this.models.users.find({
                    where: {
                        email: email
                    },
                    include: [{
                        all: true
                    }]
                }).then(function(user) {
                    if (user) {
                        return bcrypt.compare(password, user.password).then(function(auth) {
                            return new Promise(function(resolve, reject) {
                                resolve(auth ? {
                                    "id": user.id,
                                    "email": user.email,
                                    "name": {
                                        first_name: user.person.first_name,
                                        last_name: user.person.last_name,
                                    },
                                    "role_id": user.role_id,
                                    "role_name": user.role.role,
                                    "department_id": user.department_id,
                                    "department_name": user.department ? user.department.name : false,
                                    "validated": user.validated,
                                    "anonymous": user.anonymous
                                } : auth);
                            });
                        });
                    } else {
                        return new Promise(function(resolve, reject) {
                            resolve(false);
                        });
                    }

                })

            },
            updateUser: function(options) {
                return this.models.users.findOne({
                    where: {
                        id: options.id
                    }
                }).then(function(user) {
                    var update_options = {};
                    update_options['email'] = options.email;
                    update_options['department_id'] = parseInt(options.department_id, 10) || null;
                    update_options['role_id'] = parseInt(options.role_id, 10) || 3;
                    if (options.password != null) {
                        update_options['password'] = bcrypt.hashSync(options.password, saltRounds);
                    }
                    return user.update(update_options).then(function(new_user) {
                        return this.models.users.findOne({
                            where: {
                                id: new_user.id
                            },
                            include: [{
                                all: true
                            }]
                        });
                    }.bind(this)).then(function(new_user) {
                        return new_user.person.update({
                            first_name: options['first_name'] || null,
                            middle_name: options['middle_name'] || null,
                            last_name: options['last_name'] || null,
                        }, {
                            fields: ['first_name', 'middle_name', 'last_name']
                        });
                    }.bind(this)).then(function(updates) {
                        return this.models.users.findOne({
                            where: {
                                id: options.id
                            },
                            include: [{
                                all: true
                            }]
                        });
                    }.bind(this))
                }.bind(this))
            },
            newUser: function(options) {
                return this.models.users.findOne({
                    where: {
                        email: options.email
                    }
                }).then(function(user) {
                    var update_options = {};
                    update_options['email'] = options.email;
                    update_options['department_id'] = parseInt(options.department_id, 10) || null;
                    update_options['role_id'] = parseInt(options.role_id, 10) || 3;
                    if (options.password != null) {
                        update_options['password'] = bcrypt.hashSync(options.password, saltRounds);
                    }
                    return user.update(update_options).then(function(new_user) {
                        return this.models.users.findOne({
                            where: {
                                email: new_user.email
                            },
                            include: [{
                                all: true
                            }]
                        });
                    }.bind(this)).then(function(new_user) {
                        return new_user.person.update({
                            first_name: options['first_name'] || null,
                            middle_name: options['middle_name'] || null,
                            last_name: options['last_name'] || null,
                        }, {
                            fields: ['first_name', 'middle_name', 'last_name']
                        });
                    }.bind(this)).then(function(updates) {
                        return this.models.users.findOne({
                            where: {
                                email: options.email
                            },
                            include: [{
                                all: true
                            }]
                        });
                    }.bind(this))
                }.bind(this))
            },
            register: function(options) {
                var user_id = null;
                var anonymous = $.common.typeOf(options.anonymous) == 'number' ? parseInt(options.anonymous, 10) : 0;
                var register = false,
                    update = false;
                var create_user_options = {
                    email: options.email,
                    validated: 0
                };
                var update_user_options = {};
                create_user_options['validated'] = options.validated || 0;
                create_user_options['department_id'] = $.common.typeOf(options.department_id) == 'number' ? parseInt(options.department_id, 10) : null;
                create_user_options['role_id'] = $.common.typeOf(options.role_id) == 'number' ? parseInt(options.role_id, 10) : parseInt(options.role_id, 10);
                create_user_options['anonymous'] = anonymous;
                create_user_options['password'] = create_user_options['anonymous'] == 0 ? bcrypt.hashSync(options.password, saltRounds) : '';
                return this.models.users.findOrCreate({
                    where: {
                        email: options.email,
                    },
                    defaults: {
                        email: options.email,
                        department_id: create_user_options['department_id'],
                        role_id: create_user_options['role_id'],
                        anonymous: create_user_options['anonymous'],
                        password: create_user_options['password'],
                        validated: create_user_options['validated']
                    }
                }).then(function(user_obj) {
                    var user = user_obj[0];
                    var is_created = user_obj[1];
                    if (is_created) {
                        register = true;

                    } else {
                        if (user.anonymous == 1 && anonymous == 0) {
                            console.log('anony');
                            update_user_options['password'] = create_user_options['password'];
                            update_user_options['role_id'] = create_user_options['role_id'];
                            update_user_options['anonymous'] = 0;
                            register = true;
                        }
                        if (user.anonymous == 0 && anonymous == 0) {
                            register = false;
                            throw Error('Email is already used');
                        }
                    }


                    if (register) {
                        user_id = user.id || create_user_options['id'];
                        return user.update(update_user_options).then(function() {
                            return this.models.persons.upsert({
                                user_id: user_id,
                                first_name: options.first_name,
                                middle_name: options.middle_name,
                                last_name: options.last_name
                            }).then(function(upsert_person) {
                                return this.models.users.findOne({
                                    where: {
                                        id: user_id
                                    },
                                    include: [{
                                        all: true
                                    }]
                                });
                            }.bind(this))
                        }.bind(this));


                    }


                }.bind(this));

            },
            registerAnonymous: function(options) {
                return this.models.users.findOne({
                    where: {
                        email: options.email
                    }
                }).then(function(user) {
                    if (user) {
                        return user;
                    } else {
                        return this.models.users.create({
                            email: options.email,
                            anonymous: 1,
                            validated: 0
                        });
                    }
                }.bind(this));
            }
        },
        instanceMethods: {}
    },
    events: {
        onModelLoad: function(models) {

        }
    }
};