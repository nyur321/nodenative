"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            type:dataTypes.INTEGER,
            description:dataTypes.STRING
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                // models.chatstatus.hasMany(models.chatrooms,{foreignKey:'type'});
            }
        },
        instanceMethods: {}
    }
};