"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes) {
        return {
            chat_id: dataTypes.STRING,
            user_id: dataTypes.INTEGER,
            status: dataTypes.INTEGER,
            subject:dataTypes.STRING,
            departments:dataTypes.STRING
        }
    },
    options: {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        classMethods: {
            associate: function(models) {
                models.chatsessions.belongsTo(models.users, {
                    foreignKey: 'user_id'
                });
                models.chatsessions.belongsTo(models.chatstatus, {
                    foreignKey: 'status'
                });
            },
            startChat: function(options) {
                var departments = [];
                if(Array.isArray(options.departments)){
                    departments = options.departments.map(function (department) {
                        return department.value;
                    }).join(',');
                }
                // console.log(departments);

                return this.models.chatsessions.findOrCreate({
                    where: {
                        user_id:options.user_id
                    },
                    defaults:{
                        user_id:options.user_id,
                        status:1,
                        chat_id:null,
                        subject:options.subject,
                        departments:departments,
                    }
                });

            },
            checkChat:function (options) {
                console.log(options);
                return this.models.chatsessions.find({
                    where: {
                        user_id:options.user_id
                    }
                });
            },
            getList:function (options) {
                return this.models.chatsessions.findAll({
                    include:[{all:true},{
                        model:$.models.users,
                        include:[{model:$.models.persons}]
                    }],
                    where:{
                        status:[1,2,3]
                    }
                });
            },
            getActiveQueues:function(){
                return this.models.chatsessions.findAll({
                    where:{
                        '$and':{
                            status:1,
                            chat_id:{
                                '$not':null
                            }
                        }
                    }
                })
            }
        },
        instanceMethods: {}
    }
    
};