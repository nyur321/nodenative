"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            message:dataTypes.TEXT,
            subject:dataTypes.STRING,
            from:dataTypes.INTEGER,
            status_id:dataTypes.INTEGER,
           /* department_id:dataTypes.INTEGER,*/
            unread:dataTypes.INTEGER,
            anonymous:dataTypes.INTEGER,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.tickets.belongsTo(models.users,{foreignKey:'from'});
                models.tickets.belongsTo(models.ticketstatus,{foreignKey:'status_id'});
                /*models.tickets.belongsTo(models.departments,{foreignKey:'department_id'});*/
                models.tickets.hasMany(models.forwardhistory,{foreignKey:'ticket_id'});
                models.tickets.hasMany(models.approvedtickets,{foreignKey:'ticket_id'});
                models.tickets.hasMany(models.messages,{foreignKey:'ticket_id'});
                /*models.tickets.hasMany(models.attachments,{foreignKey:'ticket_id'});*/
            },
            getPerson:function (id) {
                // console.log(this)
            },
            submit:function (options) {
                console.log(options)
                return this.models.tickets.create({
                    email:options.email,
                    name:options.name,
                    message:options.message,
                    from:options.from.id,
                    subject:options.subject,
                    /*department_id:options.department_id || null,*/
                    status_id:options.status_id,
                    unread:options.unread,
                    anonymous: options.anonymous,
                   /* attachment: options.attachment*/
                });
            }
        },
        instanceMethods: {}
    }
};