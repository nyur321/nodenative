"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            reg_num:dataTypes.INTEGER,
            area_num:dataTypes.INTEGER,
            area_code:dataTypes.INTEGER,
            name:dataTypes.STRING,
            type:dataTypes.STRING,
            classification:dataTypes.STRING,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {

            },
            searchBarangay:function (keyword) {
                var Op = this.Sequelize.Op;
                return $.models.addresses.findAll({
                    where:{
                        'name':{
                            [Op.like]:keyword+'%'
                        }
                    }
                }).then(function (results) {


                });
            }
        },
        instanceMethods: {}
    }
};