"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            message_id:dataTypes.INTEGER,
            ticket_id:dataTypes.INTEGER,
            description:dataTypes.STRING,
            data:dataTypes.TEXT
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.attachments.belongsTo(models.messages,{foreignKey:'message_id'});
                models.attachments.belongsTo(models.tickets,{foreignKey:'ticket_id'});
                /*models.attachments.hasMany(models.files,{foreignKey:'attachment_id'});*/
            },
            getPerson:function (id) {
                
            },
            submit:function (options) {
                return this.models.tickets.create({
                    data: options.upload
                });
            }
        },
        instanceMethods: {}
    }
};