"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            type:dataTypes.STRING,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.feedbacktypes.hasMany(models.feedbacks,{foreignKey:'type'});
            },
            getPerson:function (id) {
                
            }
        },
        instanceMethods: {}
    }
};