"use strict";

const multer = require('multer');
const path = require('path');


module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            filename:dataTypes.STRING,
            mime_type:dataTypes.STRING,
            base_64:dataTypes.TEXT,
            path:dataTypes.TEXT,
            details:dataTypes.TEXT,
            attachment_id:dataTypes.INTEGER,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.files.belongsTo(models.tickets,{foreignKey:'ticket_id'});

            },
            getPerson:function (id) {
                
            }
        },
        instanceMethods: {}
    }
};

