"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            type:dataTypes.INTEGER,
            entity_concerned:dataTypes.TEXT,
            facts_details:dataTypes.TEXT,
            recommendations:dataTypes.TEXT,
            from:dataTypes.INTEGER,
            name:dataTypes.STRING,
            department_id:dataTypes.INTEGER,
            status_id:dataTypes.INTEGER,
            office_agency:dataTypes.STRING,
            address:dataTypes.STRING,
            email:dataTypes.STRING,
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.feedbacks.belongsTo(models.users,{foreignKey:'from'});
                models.feedbacks.belongsTo(models.ticketstatus,{foreignKey:'status_id'});
                models.feedbacks.belongsTo(models.departments,{foreignKey:'department_id'});
                models.feedbacks.belongsTo(models.feedbacktypes,{foreignKey:'type'});
            },
            submit:function (options) {
                var new_data = {}
                if(options.user){
                    new_data['email'] = options.user.email;
                }else{
                    new_data['email'] = options.email;
                }
                new_data['type'] = options.type;
                new_data['entity_concerned'] = options.entity_concerned;
                new_data['facts_details'] = options.facts_details;
                new_data['recommendations'] = options.recommendations;
                if(options.user){
                    new_data['from'] = options.user.id;
                }
                new_data['department_id'] = options.department_id;
                new_data['name'] = options.name;
                new_data['email'] = options.email;

                new_data['office_agency'] = options.office_agency;
                new_data['address'] = options.address;

                new_data['status_id'] = options.status_id;
                
                return this.models.feedbacks.create(new_data);
            }
        },
        instanceMethods: {}
    }
};