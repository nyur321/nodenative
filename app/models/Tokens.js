"use strict";



module.exports = {
    connector: 'sequelize',
    attributes: function(dataTypes){
        return {
            token:dataTypes.TEXT,
            user_id:dataTypes.INTEGER,

        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                /*models.chatmessages.belongsTo(models.chatrooms,{foreignKey:'room_id'});
                models.chatmessages.belongsTo(models.users,{foreignKey:'user_id'});*/
                models.tokens.belongsTo(models.users,{foreignKey:'user_id'});
            },
            
        instanceMethods: {}
    }
}
};