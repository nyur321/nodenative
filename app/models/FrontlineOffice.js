"use strict";



module.exports = {
    connector: 'sequelize', //default connector
    attributes: function(dataTypes){
        return {
            name:dataTypes.STRING,
            abbreviation:dataTypes.STRING,
            description:dataTypes.TEXT
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.departments.hasMany(models.users,{foreignKey:'department_id'});
                models.departments.hasMany(models.tickets,{foreignKey:'department_id'});
                models.departments.hasMany(models.forwardhistory,{foreignKey:'department_id'});
                models.departments.hasMany(models.feedbacks,{foreignKey:'department_id'});
                models.departments.hasMany(models.chatsupportdepartments,{foreignKey:'department_id'});
            },
            getPerson:function (id) {
                
            }
        },
        instanceMethods: {}
    }
};