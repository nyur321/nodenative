"use strict"

module.exports = {
    attributes: function(dataTypes){
        return {
            role: dataTypes.STRING,
            machine_name: dataTypes.TEXT,
            description: dataTypes.STRING
        }
    },
    options: {
        updatedAt:'updated_at',
        createdAt:'created_at',
        classMethods: {
            associate: function (models) {
                models.roles.hasMany(models.users,{foreignKey:'role_id'});
            }
        },
        instanceMethods: {}
    },
    events:{
        onModelLoad:function (models) {
            
        }
    }
};