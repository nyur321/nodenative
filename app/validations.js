/**
 * Custom Validations from the module Express validator.
 * Check the documentation further on https://github.com/ctavan/express-validator
 * */

module.exports = {

    isArray: function(value) {

        return Array.isArray(value);

    },

    gte: function(param, num) {

        return param >= num;

    }

};