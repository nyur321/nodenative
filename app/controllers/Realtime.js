const io = require('socket.io');
var moment = require('moment');
module.exports = {
    "chat": ['socket', ['passport-socket'], function (socket) {
        var user = socket.request.user;

        var starting_message = {
            id:0,
            name: 'System',
            subject: '',
            departments: [],
            messages:[{
                id:0,
                message: '',
                type:'message',
                date: '',
                from: {
                    name: 'System',
                    avatar: ''
                },
                time: moment().format('hh:mm:ss a')
            }]
        };

        socket.on('start_queue', function (data, callback) {
            if (user.role_name == 'Citizens') {

                socket.join('queue_room');
                // console.log(socket.client.id);
                socket.emit('message',{
                    data:{
                        messages:starting_message.messages
                    }
                });

                callback(null, {
                    error:0,
                    data:{
                        messages:starting_message.messages
                    },
                    message: 'Chat connected waiting for Officer'
                });

            }else{
                callback(true, {
                    error:1,
                    message: 'Unauthorized'
                });
            }
        });

        socket.on('start_chat',function (data,callback) {
            if (user.role_name == 'Admin' || user.role_name == 'Officers') {
                callback(true, {
                    error:1,
                    message: 'Cant start chat using an admin or officer account'
                });
            }else if(user.role_name == 'Citizens'){
                callback(null, {
                    error:0,
                    message: 'Chat started'
                });
            }else{

            }
        });

        socket.on('wait_for_request',function (data,callback) {
            if (user.role_name == 'Admin' || user.role_name == 'Officers') {
                socket.join('waiting_room');

                $.models.chatsessions.getList().then(function (results) {
                    var chatsessions = results.map(function (result) {
                        return {
                            id: result.id,
                            chat_id: result.chat_id,
                            user: result.user,
                            status: result.status,
                            subject: result.subject,
                            departments: result.departments,
                            created_at: result.created_at,
                            updated_at: result.updated_at
                        };
                    });
                    callback(null, {
                        error: 0,
                        data: {
                            chat_list: chatsessions
                        },
                        message: 'Chat connected waiting for Client'
                    });

                });

            }else{
                callback(true, {
                    error:1,
                    message: 'Unauthorized'
                });
            }
        });

        socket.on('send_message',function (err,data) {

            if (user.role_name == 'Admin' || user.role_name == 'Officers') {

            }else{


            }

        });

        socket.on('typing', function () {

        })


    }],
    "support": ['socket', ['passport-socket'], function (socket) {

    }],
    "contact_notifications":['socket',['passport-socket'],function (socket) {

    }],

    "feedback_notifications":['socket',['passport-socket'],function (socket) {

    }],

    "reply_notifications":['socket',['passport-socket'],function (socket) {

    }],

    "forward_notifications":['socket',['passport-socket'],function (socket) {

    }],

    "reply_notifications_client":['socket',['passport-socket'],function (socket) {

    }],

    "count_notifications":['socket',['passport-socket'],function (socket) {

    }],

};

function sendMessage(options,callback) {

}


var helpers = {
    'getClients': function (sockets, options) {

    }
};