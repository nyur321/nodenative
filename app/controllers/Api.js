var fs = require("fs");
var _ = require("lodash");
var async = require("async");
var bcrypt = require('bcrypt');
var datatable = require('sequelize-datatables');
var msgpack = require("msgpack-lite");
var path = require('path');
var Hashids = require('hashids');
var hashids = new Hashids();
var randtoken = require('rand-token');
var svngCaptcha = require('svg-captcha');
var multer  = require('multer')
var rand,host,link,token;

const storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});


const upload = multer({
  storage: storage,
  limits:{fileSize: 500000},
  fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  }
}).single('file');

function checkFileType(file, cb){
  const filetypes = /jpeg|jpg|png|gif|docx|doc|txt|pdf|odt/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Invalid file!');
  }
}

module.exports = {

    "index": ["GET", ["api"], function (req, res) {
        var script = react.DOM.script;
        var body = react.DOM.body;
        var div = react.DOM.div;
        res.send('dsgsfg');
        //res.reactRender('main.jsx');
    }, {
        title: 'Index of api',
        name: 'api_index'
    }],

    "uploads/:id": ["POST", function (req, res, next) {
        var message_id = req.params['id']; 
        upload(req, res, function(err){
        if(err){
            res.send(err);
        } else {
            $.models.files.create({
                filename:req.file.filename,
                mime_type:req.file.mimetype,
                base_64: req.file.encoding,
                path:req.file.path,
                details:req.file.originalname,
                ticket_id:message_id,
            }).then(function(file){
                res.send(file);
            })
        }
      });
    }],

    "uploads/get/:id": ["GET", function (req, res, next) {
        $.models.files.findAll({
            where: {
                ticket_id: req.params.id
            },
            include: [{
                all: true
            }]
        }).then(function (result) {
           res.send(result)
        }).catch(err =>
            res.send(err))
    }],

    "send/mail": ["POST", function (req, res) {
        $.run('email.compose', {
            from: 'pacd.baguiogovph@gmail.com',
            subject: req.body.subject,
            message: req.body.reply,
            to: req.body.to +'<br/><strong>Human Resource Management Office</strong><p>City Government of Baguio<br/>Baguio City Hall, City Hall Drive<br/>Baguio City<br/>(074) 300-6542</p>'
        }, function (err, message) {
            $.run('email.send', message.id, function (err_send, message_send) {
                if (err_send) {
                    res.send({error: err_send});
                } else {
                    res.send({message: message_send});
                }
            });
        });
    }],
    /*
    "captcha": ["GET", function(req, res){
        var captcha = svgCaptcha.create();
        req.session.captcha = captcha.text;
        res.type('svg');
        res.status(200).send(captcha.data);
    }],*/

    "user/register": ["POST", function (req, res) {
        rand= randtoken.generate(72);
        req.checkBody({
            first_name: {
                notEmpty: true,
                errorMessage: 'First name is required'
            },
            last_name: {
                notEmpty: true,
                errorMessage: 'Last name is required'
            },
            email: {
                notEmpty: true,
                isEmail: {
                    errorMessage: 'Email is invalid'
                },
                errorMessage: 'Email is required'
            },
            password: {
                notEmpty: true,
                errorMessage: 'Password is required'
            }
        });
        req.checkBody('password_confirmation', 'Password does not match').equals(req.body.password);
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            }
        }, function (err, results) {
            if (results.validations.length > 0) {
                res.send(results);
            } else {
                var email = req.body.email;
                $.models.users.findOne({
                    where: {
                        email: email,
                        validated: 0
                    }
                }).then(function (user) {
                    if(user){
                        $.models.users.newUser({
                            email: req.body.email,
                            password: req.body.password,
                            first_name: req.body.first_name,
                            middle_name: req.body.middle_name,
                            last_name: req.body.last_name,
                            role_id: req.body['role_id'] || 3,
                            anonymous: req.body['anonymous'],
                            validated: 0
                        }).then(function (user) {
                            $.models.tokens.findOrCreate({
                                where: {
                                    token:rand
                                },
                                defaults:{
                                    token: rand,
                                    user_id: user.id
                                }
                            })
                            results.success = 1;
                            res.send(results);
                        }).catch(function (err) {
                            results.success = 0;
                            results.error = {
                                code: err.message
                            };
                            res.send(results);
                        });
                    } else {
                        $.models.users.register({
                            email: req.body.email,
                            password: req.body.password,
                            first_name: req.body.first_name,
                            middle_name: req.body.middle_name,
                            last_name: req.body.last_name,
                            role_id: req.body['role_id'] || 3,
                            anonymous: req.body['anonymous'],
                            validated: 0
                        }).then(function (user) {
                            $.models.tokens.findOrCreate({
                                where: {user_id:user.id},
                                defaults:{
                                    token: rand,
                                    user_id: user.id
                                }
                            })
                            results.success = 1;
                            res.send(results);
                        }).catch(function (err) {
                            results.success = 0;
                            results.error = {
                                code: err.message
                            };
                            res.send(results);
                        });
                    }
                })
                
            }

        });


    }, {
        title: 'User registration',
        name: 'user_register'
    }],

    "send/verification": ["POST", function (req, res) {
        host=(req.get('host')+ '/api/');
        link=host+"verify/"+rand;
        $.run('email.compose', {
            from: 'pacd.baguiogovph@gmail.com',
            subject: 'Please confirm your email address',
            message: '<h3>Dear Ms./Mr. '+ req.body.last_name +'</h3><p>Please click on the link below to activate your account</p><br/><a href="http://'+link+'">Activate account</a><br/><p>Thank you for using the Online Public Assistance and Complaints Desk of the City Government of Baguio.</p><p>This is a system generated e-mail.<h4>Please do not reply</h4></p>',
            to: req.body.email
        }, function (err, message) {
            $.run('email.send', message.id, function (err_send, message_send) {
                if (err_send) {
                    res.send({error: err_send});
                } else {
                    res.send({message: message_send});
                }
            });
        });
    }],

    "verify/:id": ["GET", function (req, res) {
        $.models.tokens.findOne({
            where: {
                token: req.params.id
            },
            include: [{
                all: true
            }]
        }).then(function (result) {
            var user = result.user
            user.update({
                validated: 1
            })
            res.redirect('/login');
        }).catch(err =>
            res.send(err)
        )
    }],

     "user/forgot": ["POST", function (req, res) {
        token= randtoken.generate(8);
        req.checkBody({
            email: {
                notEmpty: true,
                isEmail: {
                    errorMessage: 'Email is invalid'
                },
                errorMessage: 'Email is required'
            }
        });
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            }
        }, function (err, results) {
            if (results.validations.length > 0) {
                res.send(results);
            } else {
                var email = req.body.email;
                $.models.users.findOne({
                    where: {
                        email: email,
                        validated: 1
                    },
                    include: [{all: true},
                        {model: $.models.persons}]
                }).then(function (user) {
                    if(user){
                        $.models.tokens.findOrCreate({
                            where: {
                                token:token
                            },
                            defaults:{
                                token: token,
                                user_id: user.id
                            }
                        })
                        res.end('success')
                    } else {
                        res.end('err')
                    }
                })
            }

        });

    }, {
        title: 'Forgot Passsword',
        name: 'forgot_password'
    }],

    "send/token": ["POST", function (req, res) {
        /*host=(req.get('host')+ '/api/');
        link=host+"token/"+token;*/
        host=(req.get('host'));
        link=host+"/new_password";
        $.run('email.compose', {
            subject: 'Reset your password request',
            message: '<p>We got a request to reset your password, if you do this then just copy the verification code and click the following link to reset your password, if not just ignore this.</p><br/><h3>Verification code:&nbsp;'+token+'</h3><br/><h4><a href="http://'+link+'">Click here to reset your password</a></h4><br/><p>Thank you for using the Online Public Assistance and Complaints Desk of the City Government of Baguio.</p><p>This is a system generated e-mail.<b>&nbsp;Please do not reply</b></p>',
            to: req.body.email
        }, function (err, message) {
            $.run('email.send', message.id, function (err_send, message_send) {
                if (err_send) {
                    res.send({error: err_send});
                } else {
                    res.send({message: message_send});
                }
            });
        });
    }],

    "new_password": ["POST", function (req, res) {
        req.checkBody({
            verification: {
                notEmpty: true,
                errorMessage: 'Verification code is required'
            },
            password: {
                notEmpty: true,
                errorMessage: 'Password is required'
            }
        });
        req.checkBody('password_confirmation', 'Password does not match').equals(req.body.password);
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            }
        }, function (err, results) {
            if (results.validations.length > 0) {
                res.send(results);
            } else {
                $.models.tokens.findOne({
                    where: {
                        token: req.body.verification
                    },
                    include: [{
                        all: true
                    }]
                }).then(function (result) {
                    if(result) {
                        var user = result.user.dataValues;
                        return $.models.users.findOne({
                            where: user.id
                        }).then(function(user){
                            user.update({
                                password: bcrypt.hashSync(req.body.password, 10)
                            })
                             res.end('success')
                        })
                    } else {
                       res.end('err');
                    }
                })
            }

        });
    }],

    "user/get/:id": ["GET", ["api"], function (req, res) {
        var user_id = req.params['id'] || req.user.id;
        $.models.users.findOne({
            where: {
                id: user_id
            },
            include: [{
                all: true
            }]
        }).then(function (user) {
            var data = {};
            if (user) {
                data['id'] = user.id;
                data['email'] = user.email;
                data['department_id'] = user.department_id || "";
                data['role_id'] = user.role_id || "";
                if (user.person) {
                    data['first_name'] = user.person.first_name;
                    data['middle_name'] = user.person.middle_name;
                    data['last_name'] = user.person.last_name;
                }
            } else {
                data['error'] = "user_not_found";
            }
            res.send(data);
        })
    }],

    "feedback/get/:id": ["GET", ["api"], function (req, res) {
        var feedback_id = req.params['id'] || req.feedback.id;
        $.models.feedbacks.findOne({
            where: {
                id: feedback_id
            },
            include: [{
                all: true
            }]
        }).then(function (result) {
            res.send(result);
        })
    }],
    "feedback/update/:id": ["POST", ['api'], function (req, res) {
        var feedback_id = req.params['id'] || req.ticket.id;
        $.models.feedbacks.findOne({
            where: {
                id: feedback_id
            },
        }).then(function (result) {
            result.update({
                status_id: req.body.status_id
            })
            res.send(result)
        }).catch(err =>
            res.send(err)
        )
    }],
    "ticket/get/:id": ["GET", ["api"], function (req, res) {
        var ticket_id = req.params['id'] || req.ticket.id;
        $.models.tickets.findOne({
            where: {
                id: ticket_id
            },
            include: [{all: true}, 
                {model: $.models.users, include: [{ model: $.models.persons}]},
                {model: $.models.approvedtickets, include: [{model: $.models.departments}]},
                {model: $.models.forwardhistory, include: [{
                    model: $.models.users, include: [{model: $.models.persons}]
                },{
                    model: $.models.departments
                }]
            }]
        }).then(function (ticket) {
            res.send(ticket)
            // console.log(ticket)
        })
    }],
    
    "reply/get/:id": ["GET", ["api"], function (req, res) {
        var ticket_id = req.params['id'] || req.ticket.id;
        $.models.messages.findAll({
            order: [
                ['id', 'ASC']
            ],
            where: {
                ticket_id: ticket_id,
                type: 1
            },
            include: [{all: true}, {
                model: $.models.users, include: [{
                    model: $.models.departments
                },{model: $.models.persons}]
            }]
        }).then(function (reply) {
            res.send(reply)
        })
    }],
    "user/update": ["POST", ['api'], function (req, res) {
        req.checkBody({
            first_name: {
                notEmpty: true,
                errorMessage: 'First name is required'
            },
            last_name: {
                notEmpty: true,
                errorMessage: 'Last name is required'
            },
            email: {
                notEmpty: true,
                isEmail: {
                    errorMessage: 'Email is invalid'
                },
                errorMessage: 'Email is required'
            },
            password: {
                optional: {
                    options: {
                        checkFalsy: true
                    }
                }
            }

        });
        req.checkBody('confirm_password', 'Password does not match').equals(req.body.password);
        // console.log(req.body);
        async.series({ 
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            }
        }, function (err, results) {
            if (results.validations.length > 0) {
                res.send(results);
            } else {
                //console.log(req.body.id);
                var user_id = req.body.id;
                if (user_id) {
                    var update_options = {};
                    update_options['id'] = user_id;
                    update_options['email'] = req.body.email;
                    update_options['department_id'] = parseInt(req.body.departments, 10);
                    update_options['role_id'] = parseInt(req.body.roles, 10) || 3;
                    update_options['first_name'] = req.body.first_name;
                    update_options['middle_name'] = req.body.middle_name;
                    update_options['last_name'] = req.body.last_name;
                    update_options['password'] = req.body.password || null;

                    $.models.users.updateUser(update_options).then(function (user) {
                        results.success = 1;
                        results.user = {
                            id: user.id,
                            email: user.email,
                            // first_name:user.person.first_name,
                            // middle_name:user.person.middle_name,
                            // last_name:user.person.last_name,
                        }
                        res.send(results);
                    })
                } else {
                    results.success = 0;
                    results.error = {
                        code: 'user_id_not_found'
                    };
                    res.send(results);
                }
            }

        });
    }],

    "user/all": ["POST", ["api"], function (req, res) {
        datatable($.models.users, req.body, {
            include: [{
                all: true
            }]
        }).then(function (result) {
            res.send(result);
        });
    }, {
        title: 'User lists',
        name: 'get_users'
    }],

    "ticket/all": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            include: [{
                all: true
            }]
        }).then(function (result) {
            // console.log(result);
            res.send(result)
            // console.log(result)
        });
    }, {
        title: 'Message lists',
        name: 'get_messages'
    }],

    "ticket/forward/all": ["POST", ["api"], function (req, res) {
        datatable($.models.forwardhistory, req.body, {
            where: {
                department_id: req.user.department_id
            },
            include: [{
                all: true
            }, {
                model: $.models.tickets,
                include: [{
                    model: $.models.ticketstatus
                }, {
                    model: $.models.messages
                }, {
                    model: $.models.users
                }, {
                    model: $.models.forwardhistory
                }]
            }],
            order: [ [$.models.tickets, 'created_at', 'DESC' ] ]
        }).then(function (result) {
            res.send(result)            
        });
    }, {
        title: 'Message lists',
        name: 'get_messages'
    }],

    "ticket/client/all": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                from: req.user.id
            },
            include: [{all: true},{
                model: $.models.messages
            }],
            order: [ [$.models.messages, 'updated_at', 'DESC' ] ]
        }).then(function (result) {
            res.send(result)
            // console.log(result);
        });
    }, {
        title: 'Message lists',
        name: 'get_messages'
    }],

    "ticket/ongoing": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                status_id: 1
            },
            include: [{all: true}],
            order: [ [$.models.messages, 'updated_at', 'DESC' ] ]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Ongoing tickets', name: 'get_tickets'}],

    "ticket/resolved": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                status_id: 2
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Resolved tickets', name: 'get_tickets'}],


    "ticket/anonymous": ["POST", ["api"], function (req, res) {
       datatable($.models.tickets, req.body, {
            where: {
                anonymous: 1,
                status_id: 3
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }],
    "ticket/pending": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                anonymous: 0,
                status_id: 3
            },
            order: [['updated_at', 'DESC']],
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Pending tickets', name: 'get_tickets'}],

    "ticket/setclosed/all": ["GET", ["api"], function (req, res) {
        var Op = $.model.db.connectors['sequelize'].Op;
        var date = new Date();
        date.setDate(date.getDate() - 3);

        datatable($.models.tickets, req.body, {
            where: {
                updated_at: {
                  [Op.lte]: date
                }
            },
            order: [['updated_at', 'DESC']]
        }).then(function (result) {
            res.send(result);
        });
    }],

    "ticket/closed": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                status_id: 4
            },
            order: [['updated_at', 'DESC']],
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Closed tickets', name: 'get_tickets'}],

    "ticket/revoked": ["POST", ["api"], function (req, res) {
        datatable($.models.tickets, req.body, {
            where: {
                status_id: 5
            },
            order: [['updated_at', 'DESC']],
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Closed tickets', name: 'get_tickets'}],

    "feedback/client/all": ["POST", ["api"], function (req, res) {
        datatable($.models.feedbacks, req.body, {
            where: {
                from: req.user.id
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Feedback lists', name: 'get_feedbacks'}],

    "feedback/all": ["POST", ["api"], function (req, res) {
        datatable($.models.feedbacks, req.body, {
            include: [{
                all: true
            }]
        }).then(function (result) {
            res.send(result);
        });
    }, {
        title: 'Feedback lists',
        name: 'get_feedbacks'
    }],

    "feedback/pending": ["POST", ["api"], function (req, res) {
        datatable($.models.feedbacks, req.body, {
            where: {
                status_id: 3
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Pending feedbacks', name: 'get_feedbacks'}],

    "feedback/accepted": ["POST", ["api"], function (req, res) {
        datatable($.models.feedbacks, req.body, {
            where: {
                status_id: 2
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Accepted feedbacks', name: 'get_feedbacks'}],

    "feedback/revoked": ["POST", ["api"], function (req, res) {
        datatable($.models.feedbacks, req.body, {
            where: {
                status_id: 5
            },
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {title: 'Revoked feedbacks', name: 'get_feedbacks'}],

    "user/create": ["POST", function (req, res) {
        req.checkBody({
            first_name: {
                notEmpty: true,
                errorMessage: 'First name is required'
            },
            last_name: {
                notEmpty: true,
                errorMessage: 'Last name is required'
            },
            email: {
                notEmpty: true,
                isEmail: {
                    errorMessage: 'Email is invalid'
                },
                errorMessage: 'Email is required'
            },
            password: {
                notEmpty: true,
                errorMessage: 'Password is required'
            }
        });
        req.checkBody('confirm_password', 'Password does not match').equals(req.body.password);
        // console.log(req.body);
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            }
        }, function (err, results) {
            if (results.validations.length > 0) {
                res.send(results);
            } else {
                $.models.users.register({
                    email: req.body.email,
                    password: req.body.password,
                    first_name: req.body.first_name,
                    middle_name: req.body.middle_name,
                    last_name: req.body.last_name,
                    role_id: req.body['roles'],
                    department_id: parseInt(req.body.departments, 10),
                    anonymous: 0,
                    validated: 1
                }).then(function (user) {
                    results.success = 1;
                    results.user = {
                        id: user.id,
                        email: user.email,
                        first_name: user.person.first_name,
                        middle_name: user.person.middle_name,
                        last_name: user.person.last_name,
                    }
                    res.send(results);
                }).catch(function (err) {
                    results.success = 0;
                    results.error = {
                        code: err.message
                    };
                    res.send(results);
                });
            }

        });
    }, {
        name: 'user_create'
    }],

    "user/login": ["POST", ['passport'], function (req, res) {
        res.send(res.locals['data_login']);
    }],

    "user/captcha": ["POST", function (req, res) {
        console.log($.run('captcha.init'));

        res.send({
            captcha: '',
            text: ''
        })

    }],

    "feedback/type": ["GET", function (req, res) {
        $.models.feedbacktypes.findAll().then(function (results) {
            res.send(results);
        })
    }],
    "user/test": ["GET", ['api'], function (req, res) {
        res.send({
            locals: res.locals,
            passport: req.user
        });

    }, {
        title: 'tae',
        name: 'shit',
        data: {
            'fuck': 'you'
        }
    }],

    "user/profile": ["POST", ['api'], function (req, res) {
        var buffer = msgpack.encode({
            body: req.body,
            locals: res.locals
        });
        res.send(msgpack.decode(buffer));

    }],

    "contact/submit": ["POST", function (req, res) {
        var data = {};
        req.checkBody({
            name: {
                optional: {
                    options: {
                        checkFalsy: true
                    },
                }
            },
            email: {
                notEmpty: {
                    errorMessage: 'Email is required'
                },
                isEmail: {
                    errorMessage: 'Please put a valid email'
                }
            },
            message: {
                notEmpty: true,
                errorMessage: 'Message is required'
            },
            department_id: {
                notEmpty: true,
                errorMessage: 'Please choose a department'
            }
        });


        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    data['validations'] = [];
                    if (result.array().length > 0) {
                        data['validations'] = result.array();
                        callback(true);
                    } else {
                        callback(null);
                    }
                });
            },
            user: function (callback) {
                data['user'] = {};
                if (req.isAuthenticated()) {
                    data['user'] = req.user;
                    callback(null, req.user);
                } else {
                    $.models.users.registerAnonymous({
                        email: req.body.email
                    }).then(function (user) {
                        data['user'] = user;
                        callback(null, user);
                    });
                }
            },
            submit: function (callback) {
                $.models.tickets.submit({
                    department_id: req.body.department_id,
                    name: req.body.name || '',
                    email: req.body.email,
                    subject: req.body.subject || '',
                    from: data['user'],
                    message: req.body.message,
                    status_id: 3,
                    unread: 1,
                    anonymous: req.body.anonymous,
                    /*attachment: req.body.attachment*/
                }).then(function (contact) {
                    data['message'] = contact;

                    callback(null, contact);
                })
            }
        }, function (err, results) {
            if (data.validations.length > 0) {
                res.send(data)
            } else {
                $.getNamespace('/realtime/contact_notifications').emit('notification', {
                    type: 'info',
                    message: 'A citizen has submitted a message'
                });
                res.send(data);
            }
        })
    }, {
        name: 'contact_submit_form',
        title: 'Contact Submit'
    }],

    "address/search": ["POST", function (req, res) {
        console.log($.model.db.connectors['sequelize'].Sequelize.Op);
        $.models.addresses.searchBarangay(req.body.keyword).then(function (results) {
            res.send({
                results: results
            })
        });
    }],

    "feedback/submit": ["POST", function (req, res) {
        var data = {};
        req.checkBody({
            type: {
                notEmpty: {
                    errorMessage: 'Feedback type is required'
                }
            },
            email: {
                notEmpty: {
                    errorMessage: 'Email is required'
                },
                isEmail: {
                    errorMessage: 'Please put a valid email'
                }
            },
            entity_concerned: {
                notEmpty: true,
                errorMessage: 'Concerned/Involved is required'
            },
            facts_details: {
                notEmpty: true,
                errorMessage: 'Facts/Details is required'
            },
            recommendations: {
                notEmpty: true,
                errorMessage: 'Recomendation/Suggestion is required'
            },
            department_id: {
                notEmpty: true,
                errorMessage: 'Please choose a department'
            }
        });
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    data['validations'] = [];
                    if (result.array().length > 0) {
                        data['validations'] = result.array();
                        callback(true);
                    } else {
                        callback(null);
                    }
                });
            },
            user: function (callback) {
                data['user'] = {};
                if (req.isAuthenticated()) {
                    data['user'] = req.user;
                    callback(null, req.user);
                } else {
                    $.models.users.registerAnonymous({
                        email: req.body.email
                    }).then(function (user) {
                        data['user'] = user;
                        callback(null, user);
                    });
                }
            },
            submit: function (callback) {
                var obj_submit = {
                    user: data['user'],
                    type: req.body.type,
                    entity_concerned: req.body.entity_concerned,
                    facts_details: req.body.facts_details,
                    recommendations: req.body.recommendations,
                    from: data['user'],
                    department_id: req.body.department_id,
                    name: req.body.name || '',
                    email: req.body.email,
                    office_agency: req.body.office_agency || '',
                    address: req.body.address || '',
                    status_id: 3,
                };
                $.models.feedbacks.submit(obj_submit).then(function (feedback) {
                    data['feedback'] = feedback;
                    callback(null, feedback);
                });
            }
        }, function (err, results) {
            data['success'] = 1;
            if (data.validations.length > 0) {
                res.send(data);
            } else {
                // $.sockets[2].nsp.emit('notification',{
                //     type:'info',
                //     message:'A citizen has submitted a feedback'
                // });
                $.getNamespace('/realtime/feedback_notifications').emit('notification', {
                    type: 'info',
                    message: 'A citizen has submitted a feedback'
                });
                res.send(data);
            }
        })

    }],

    "ticket/approve": ["POST", function (req, res) {
        var data = {};
        async.series({
            submit: function (callback) {
                $.models.approvedtickets.submit({
                    department_id: req.body.department_id,
                    ticket_id: req.body.ticket_id,
                    approved_by: req.body.approved_by,
                }).then(function (approve) {
                    data['approve'] = approve;
                    callback(null, approve);
                })
            }
        }, function (err, results) {
            res.send(data);
        })
    }],

    "ticket/update/:id": ["POST", ['api'], function (req, res) {
        var ticket_id = req.params['id'] || req.ticket.id;
        $.models.tickets.findOne({
            where: {
                id: ticket_id
            }
        }).then(function (result) {
            return result.update({
                status_id: req.body.status_id,
                unread: req.body.unread
            }).then(function(data) {
                res.send(data)
                $.getNamespace('/realtime/count_notifications').emit('notification');
            })
        })
    }],

    "ticket/forward": ["POST", function (req, res) {
        var data = {};
        req.checkBody({
            department_id: {
                notEmpty: true,
                errorMessage: 'Department is required'

            },
        });
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    data['validations'] = [];
                    if (result.array().length > 0) {
                        data['validations'] = result.array();
                        callback(true);
                    } else {
                        callback(null);
                    }
                });
            },
            submit: function (callback) {
                $.models.forwardhistory.submit({
                    department_id: req.body.department_id,
                    ticket_id: req.body.id,
                    forwarded_by: req.body.forwarded_by,
                    comments: req.body.comments || ''
                }).then(function (forward) {
                    data['forward'] = forward;
                    callback(null, forward);
                })
            }
        }, function (err, results) {
            res.send(data);
            $.getNamespace('/realtime/forward_notifications').emit('notification');
        })
    }],

    "ticket/forward/all/:id": ['GET', function(req,res){
        var ticket_id  = req.params['id'] || req.forwardhistory.id
        $.models.forwardhistory.findAll({
            where: {
                ticket_id: ticket_id,
            },
            order: [['updated_at', 'DESC']],
            include: [{all: true},{
                model: $.models.departments
            }]
        }).then(function(results){
            res.send(results)
        })
    }],

    "ticket/forward/delete/:id": ['DELETE', function(req,res){
        var dept_id  = req.params['id'] || req.forwardhistory.id
        $.models.forwardhistory.destroy({
          where: { id:dept_id}
        }).then(function(){
            $.models.forwardhistory.findAll({
            include: [{
                all: true
            }]
        }).then(function(results){
                res.send(results)
                $.getNamespace('/realtime/forward_notifications').emit('notification');
            })
        })
    }],

    "user/logout": ['GET', function (req, res) {
        req.logout();
        // console.log(req.session);
        // res.send({hooga:'sdfsd'})
        req.session.destroy(function (err) {
            res.send({
                redirect: '/login'
            });
        });
    }, {
        name: 'logout',
        title: 'Logout'
    }],

    "messages/all": ['POST', ['role'], function (req, res) {
        var user = req.user;
        var Op = $.model.db.connectors['sequelize'].Op;
        //console.log(Op);
        var role = res.locals.role;
        var data = {},
            options = {};
        data['user'] = user;
        if (res.locals.role.role == 'Admin') {
            options['include'] = [{
                all: true
            }];
        }
        if (res.locals.role.role == 'Officers') {
            // options['where'] = {
            //
            // };
            options['include'] = [{
                all: true
            }];
        }
        $.models.tickets.findAll(options).then(function (results) {
            data['messages'] = results;
            data['options'] = options;
            res.send(data);
        })


    }],

    "message/view": ['POST', ['api', 'role', 'department'], function () {
        req.checkBody({
            message_id: {
                notEmpty: {
                    errorMessage: 'Message id is required'
                },
            },
            reply: {
                notEmpty: {
                    errorMessage: 'Reply is required'
                },
            }
        });
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    data['validations'] = [];
                    if (result.array().length > 0) {
                        data['validations'] = result.array();
                        callback(true);
                    } else {
                        callback(null);
                    }
                });
            },
            user: function (callback) {
                data['user'] = {};
                if (req.isAuthenticated()) {
                    data['user'] = req.user;
                    callback(null, req.user);
                } else {
                    $.models.users.registerAnonymous({
                        email: req.body.email
                    }).then(function (user) {
                        data['user'] = user;
                        callback(null, user);
                    });
                }
            },
            reply: function (callback) {
                var obj_submit = {
                    user: data['user'],
                    message_id: req.body.message_id,
                    reply: req.body.reply
                };
                $.models.replies.addReply(obj_submit).then(function (reply) {
                    data['reply'] = reply;
                    callback(null, reply);
                });
            }
        }, function (err, results) {
            data['success'] = 1;
            res.send(data);
        });

    }],

    "report/charts": ['POST', ['api', 'admin'], function (req, res) {
        async.series({
            user_registrations: function (callback) {
                $.models.users.findAndCountAll().then(function (result) {
                    callback(null, result.count);
                });
            },
            tickets: function (callback) {
                $.models.tickets.findAndCountAll().then(function (result) {
                    callback(null, result.count);
                });
            },
            feedback: function (callback) {
                $.models.feedbacks.findAndCountAll().then(function (result) {
                    callback(null, result.count);
                });
            },
            department_users: function (callback) {
                $.models.departments.findAndCountAll({
                    include: [{
                        all: true
                    }]
                }).then(function (result) {
                    var departments = [];
                    result.rows.forEach(function (department) {
                        var users = [];
                        department.users.forEach(function (user) {
                            if (user.role_id != 3 || user.role_id != 4) {
                                users.push({
                                    id: user.id
                                })
                            }
                        });
                        departments.push({
                            id: department.id,
                            name: department.name,
                            user_count: users.length
                        });
                    });
                    callback(null, departments);
                });
            },
            department_tickets: function (callback) {
                $.models.departments.findAndCountAll({
                    include: [{
                        all: true
                    }]
                }).then(function (result) {
                    var departments = [];
                    result.rows.forEach(function (department) {
                        var approvedtickets = [];
                        department.approvedtickets.forEach(function (approvedticket) {
                            if (approvedticket) {
                                approvedtickets.push({
                                    id: approvedticket.id
                                });
                            }
                        });
                        departments.push({
                            id: department.id,
                            name: department.abbreviation,
                            long_name: department.name,
                            ticket_count: approvedtickets.length
                        });
                    });
                    callback(null, departments);
                });
            }

        }, function (err, results) {
            res.send(results);
        })
    }],
    "ticket/statuses": ['POST', ['api'], function (req, res) {
        async.series({
            ongoing: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 1
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            /*resolved: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 2
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },*/
            anonymous: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 3,
                        anonymous: 1
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            pending: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 3,
                        anonymous: 0
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            closed: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 4
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            revoked: function (callback) {
                $.models.tickets.findAndCountAll({
                    where: {
                        status_id: 5
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
        }, function (err, results) {
            res.send(results);
        })
    }],

    "feedback/statuses": ['POST', ['api'], function (req, res) {
        async.series({
            accepted: function (callback) {
                $.models.feedbacks.findAndCountAll({
                    where: {
                        status_id: 2
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            pending: function (callback) {
                $.models.feedbacks.findAndCountAll({
                    where: {
                        status_id: 3
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
            revoked: function (callback) {
                $.models.feedbacks.findAndCountAll({
                    where: {
                        status_id: 5
                    },
                }).then(function (result) {
                    callback(null, result.count);
                });
            },
        }, function (err, results) {
            res.send(results);
        })
    }],

    "message/reply": ['POST', ['api', 'role', 'department'], function (req, res) {
        var data = {};
        req.checkBody({
            reply: {
                notEmpty: {
                    errorMessage: 'Reply is required'
                },
            }
        });
        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    data['validations'] = [];
                    if (result.array().length > 0) {
                        data['validations'] = result.array();
                        callback(true);
                    } else {
                         callback(null);
                    }
                });
            },
            submit: function (callback) {
                var obj_submit = {
                    ticket_id: req.body.ticket_id,
                    from: req.body.from,
                    type: req.body.type,
                    message: req.body.reply
                };
                $.models.messages.submit(obj_submit).then(function (reply) {
                    data['reply'] = reply;
                    callback(null, reply);
                });
            }
        }, function (err, results) {
            data['success'] = 1;
            if (data.validations.length > 0) {
                res.send(data);
            } else {
                if(req.user.role_id == 3){
                    $.getNamespace('/realtime/reply_notifications').emit('notification', {
                    type: 'warning',
                    message: 'A citizen has submitted a reply'
                });
                res.send(data);
                } else {
                    $.getNamespace('/realtime/reply_notifications_client').emit('notification', {
                    type: 'warning',
                    message: 'A new reply is submitted'
                });
                res.send(data);
                }
            }
        });
    }],

    "email/compose": [function (req, res) {

        res.send({
            message: 'sent'
        });
    }],

    "email-body": [function (req, res) {
        // res.send();

    }],

    "user/chat/submit": ["POST", ["api"], function (req, res) {
        req.checkBody({
            subject: {
                notEmpty: true,
                errorMessage: 'Subject is required'
            }
        });

        async.series({
            validations: function (callback) {
                req.getValidationResult().then(function (result) {
                    callback(null, result.array());
                });
            },
            chat: function (callback) {
                var departments = _.filter(req.body.departments, function (department) {
                    return department.selected;
                });
                $.models.chatsessions.startChat({
                    user_id: req.user.id,
                    subject: req.body.subject,
                    departments: departments
                }).then(function (results) {
                    var data = {
                        room: "",
                        chat_id: results[0].chat_id,
                        user_id: results[0].user_id,
                        status: results[0].status,
                        is_created: results[1],
                        subject: req.body.subject,
                        departments: departments
                    };
                    callback(null, data);
                });
            }
        }, function (err, async_results) {

            $.models.chatsessions.getList().then(function (results) {
                var chatsessions = results.map(function (result) {
                    return {
                        id: result.id,
                        chat_id: result.chat_id,
                        user: result.user,
                        status: result.status,
                        subject: result.subject,
                        departments: result.departments,
                        created_at: result.created_at,
                        updated_at: result.updated_at
                    };
                });

                var socket_chat = $.getNamespace('/realtime/chat');

                socket_chat.to('waiting_room').emit('list_update', {
                    error: 0,
                    data: {
                        chat_list: chatsessions
                    },
                    message: 'New client has queued'
                });
                res.send(async_results);
            });


        });
    }, {
        title: 'Chat submit',
        name: "chat_submit"
    }],

    "chat-session": ["POST", function (req, res) {
        $.models.chatsessions.checkChat({user_id: req.user.id}).then(function (results) {

            var data = {};
            if(results) {
                data = {
                    chat_id: results.chat_id,
                    user_id: results.user_id,
                    status: results.status,
                    subject: results.subject,
                    departments: results.departments.split(',')
                };
            }else{
                data.departments = [];
            }
            // console.log
            res.send(data);
        });

    }],

    "chat/list":["get",function (req,res) {

    }],
    "chat/accept":["post",function (req,res) {
        console.log(req.body);
        var h_id = "";
        var chat_data = null;
        $.models.chatsessions.findOne({
           where:{
               id:req.body.chat.id
           }
        }).then(function (chat) {
            return chat.update({
                status:3
            });
        }).then(function (chat) {
            var dept_arr = chat.departments.split(',');
            var arr_id = [chat.id,chat.user_id];
            if(Array.isArray(dept_arr)){
                arr_id = arr_id.concat(dept_arr);
            }

            h_id = hashids.encode(arr_id);
            chat_data = chat;
            if(chat.chat_id){
                return $.models.chatsupports.findOne({
                    where:{
                        id:chat.chat_id
                    }
                });
            }else{
                return $.models.chatsupports.create({
                    reference_id:h_id,
                    subject:'',
                    user_id:chat_data.user_id,
                    status:3
                });
            }


        }).then(function (chat_support) {

            if(chat_support.room_id){
                return $.models.chatrooms.create({
                    type:'support'
                }).then(function (chatroom) {
                    return chat_support.update({
                        room_id:chatroom.id
                    });
                });
            }else{
                return new Promise(function (resolve,reject) {
                    resolve(chat_support);
                })
            }

        }.bind(this)).then(function (chat_support) {
            console.log(chat_support);
            // console.log(chat_data);
            // var chatsupport_id = {
            //     reference_id:h_id,
            //     room_id:room.id,
            //     subject:'',
            //     user_id:chat_data.user_id,
            //     status:3
            // };


            res.send({
                err:0,
                message:"Chat accepted",
                data:{
                    reference_id:h_id
                }
            });
        }.bind(this));

    }]


    // "midware/unload/:midware": ["GET", function (req, res) {
    //     $.middleware.unload(null, req.params.midware);
    //     res.send(req.params.midware + ' middleware unloaded')
    // }],
    //
    // "midware/load/:midware": ["GET", function (req, res) {
    //     $.middleware.load(null, req.params.midware);
    //     res.send(req.params.midware + ' middleware loaded')
    // }],
    //
    // "midware": ["GET", function (req, res) {
    //     res.send({stack: $.middleware.getStack(), locals: res.locals});
    // }],
    //
    // "model": ["GET", function (req, res) {
    //     $.models.persons.findAll().then(function (results) {
    //     })
    //
    //     res.send({test: true});
    // }]


};

