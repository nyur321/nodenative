"use strict";
var async = require('async');
var departments = require('../resources/departments');
var addresses = require('../resources/addresses');
var feedbacktypes = require('../resources/feedbacktypes');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
module.exports = {

    onModelsLoad: function (cb) {

        async.series([
            function (callback) {
                $.models.roles.bulkCreate([
                    {id: 1, role: 'Admin', description: 'Administrator'},
                    {id: 2, role: 'Officers', description: 'Officers'},
                    {id: 3, role: 'Citizens', description: 'Citizens'},
                    {id: 4, role: 'Anonymous', description: 'Anonymous User'},
                ], {
                    updateOnDuplicate: ['role', 'description']
                }).then(function (roles) {
                    callback();
                })
            },
            function (callback) {
                $.models.chatstatus.bulkCreate([
                    {id: 1, status: 'queued', label:'Queued', description: 'Chat is still queued'},
                    {id: 2, status: 'pending', label:'Pending', description: 'Chat is pending for acceptance'},
                    {id: 3, status: 'ongoing', label:'On-going', description: 'On going chat'},
                    {id: 4, status: 'closed', label:'Closed', description: 'Closed / finished chat'},
                ], {
                    updateOnDuplicate: ['status','label' ,'description']
                }).then(function (status) {
                    callback();
                })
            },
            function (callback) {
                $.models.feedbacktypes.bulkCreate(feedbacktypes.rows, {
                    updateOnDuplicate: ['type']
                }).then(function (types) {
                    callback();
                })
            },
            function (callback) {
                $.models.departments.bulkCreate(departments.rows, {
                    updateOnDuplicate: ['department','abbreviation' ,'description']
                }).then(function (roles) {
                    callback();
                })
            },
            function (callback) {
                $.models.addresses.findOne({where:{id:1}}).then(function (results) {
                    if(results){
                        callback();
                    }else{
                        var rows = addresses.rows;
                        var addresses_chunks = _.chunk(rows,1000);

                        async.each(addresses_chunks,function (row,cb) {
                            $.models.addresses.bulkCreate(row, {
                                updateOnDuplicate: ['reg_num','area_num' ,'area_code','name','type','classification']
                            }).then(function () {
                                cb();
                            });

                        },function (err) {
                            callback();

                        });
                    }
                });






            },
            function (callback) {
                $.models.users.upsert({
                    id: 1,
                    email: 'admin@gmail.com',
                    password: '$2a$10$RLIJaeq3YBt1DQ5TASb.NespILJEhp8KxDaNnGJHLTaBuP4UjAXWq',//querty
                    validated: 1,
                    anonymous: 0,
                    role_id: 1,
                    person_id: 1,
                }).then(function () {

                    callback();
                });
            },
            function (callback) {
                $.models.persons.upsert({
                    id: 1,
                    user_id: 1,
                    first_name: 'Administrator',
                    last_name: 'Account',
                }).then(function () {    
                    callback();
                });
            },
            function (callback) {
                $.models.ticketstatus.bulkCreate([{
                    id: 1,
                    status: 'Ongoing',
                    label: 'Ongoing',
                    description: 'An open ticket is a newly submitted ticket from Citizens.',
                },{
                    id: 2,
                    status: 'Accepted',
                    label: 'Accepted',
                    description: 'The pending feedback ticket is accepted',
                },{
                    id: 3,
                    status: 'Pending',
                    label: 'Pending',
                    description: 'resolution is still pending.',
                },{
                    id: 4,
                    status: 'Closed',
                    label: 'Closed',
                    description: 'Concern or issue is closed.',
                },{
                    id: 5,
                    status: 'Revoked',
                    label: 'Revoked',
                    description: 'Concern or issue is revoked.',
                }],{
                    updateOnDuplicate: ['status','label' ,'description']
                }).then(function () {
                    callback();
                });
            },
            function (callback){
                $.models.messagetype.bulkCreate([{
                    id: 1,
                    type: 'reply',
                    label: 'Reply',
                    description: 'Reply to tickets.',
                },{
                    id: 2,
                    type: 'chat',
                    label: 'Chat',
                    description: 'Chat message.',
                },{
                    id: 3,
                    type: 'address',
                    label: 'Address',
                    description: 'Addressing.',
                }],{
                    updateOnDuplicate: ['status','label', 'description']
                }).then(function () {
                    callback();
                });
            }
        ], function () {
            cb(null);
        });


    },

    onControllersLoad: function (cb) {
        cb(null);
    },

    onRoutesLoad: function (cb) {
        cb(null);
    },

    onSocketLoad: function (cb) {
        cb(null);
    },

    onPluginsLoad: function (cb) {
        cb(null);
    },

    onServerLoad: function (cb) {
        cb(null);
    }


};