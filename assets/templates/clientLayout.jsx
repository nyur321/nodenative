const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
module.exports = createReactClass({
    displayName: 'Layout',
    render: function() {
        let index = 'title lols';
        let bundle_js = index.site_url+'/bundle.js';
        return (
            <html>
            <head>
                <title>{index.title}</title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

            </head>
            <body className="hold-transition skin-blue layout-top-nav">
            <div className="wrapper">

                <div className ="content-wrapper">
                    <div className="container">
                        {this.props.children}
                    </div>
                </div>
            </div>
            <script dangerouslySetInnerHTML={{__html: 'window.PROPS=' + JSON.stringify(index)}}></script>
            
            <script src={bundle_js} />
            </body>
            </html>
        );
    }
});
