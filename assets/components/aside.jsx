const React = require('react');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const Link = ReactRouter.Link;
var axios = require('axios');

var Aside = createReactClass({
    getInitialState: function (data) {
        return {
            ishidden: false
        }
    },
    componentDidMount: function () {
        var index = this.props.index;
        var count = this.props.count;
        axios({
            method: 'post',
            url: '/api/ticket/statuses',
        }).then(function (response) {
            var count = response.data;
            this.setState({
                tanonymous: count.anonymous || 0,
                tpending: count.pending || 0,
                tongoing: count.ongoing || 0,
                tresolved: count.resolved || 0,
                tclosed: count.closed || 0,
                trevoked: count.revoked || 0
            });
        }.bind(this));
        axios({
            method: 'post',
            url: '/api/feedback/statuses',
        }).then(function (response) {
            var count = response.data;
            this.setState({
                fpending: count.pending || 0,
                faccepted: count.accepted || 0,
                frevoked: count.revoked || 0
            });
        }.bind(this));
        this.setState({
            ishidden: index.data.user.role_id == 2 ? true : false,
        });
    },
    render: function () {
        var isAdmin = this.props.isAdmin;
        if (isAdmin) {
            return (
                <aside className="main-sidebar">
                    <section className="sidebar">
                        <ul className="sidebar-menu" data-widget="tree">
                            <li className="header active" hidden={this.state.ishidden}>
                                <i className="fa fa-eye">&nbsp;</i>OVERVIEW
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/reports">
                                    <i className="fa fa-bar-chart"></i>
                                    <span>Reports</span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/walk-in">
                                    <i className="fa fa-male"></i>
                                    <span>Walk-in</span>
                                </Link>
                            </li>
                            {/*<li className="header"><i className="fa fa-inbox">&nbsp;</i>CHATS</li>
                            <li><Link to="/dashboard/chat_support"><i className="fa fa-bar-chart"></i><span>Chat Support</span></Link></li>*/}
                            {/*<li className="header active"><i className="fa fa-inbox">&nbsp;</i>TICKETS</li>*/}
                            {/*<li hidden={this.state.ishidden}><Link to="/dashboard/messages/resolved"><i className="fa fa-circle-o text-green"></i>Resolved<span className="pull-right-container"><span className="label label-success pull-right" id="ticket_resolved">{this.state.tresolved}</span></span></Link></li>*/}
                            {/*<li className="treeview" >
                                <Link to="/dashboard/messages">
                                    <i className="fa fa-envelope"></i>
                                    <span>Contact us</span>
                                    <span className="pull-right-container">
                                      <i className="fa fa-angle-left pull-right"></i>
                                    </span>
                                </Link>
                                <ul className="treeview-menu">
                                    <li><Link to="/dashboard/messages"><i className="fa fa-circle-o text-blue"></i>All</Link></li>
                                    <li hidden={this.state.ishidden}><Link to="/dashboard/messages/anonymous"><i className="fa fa-circle-o text-green"></i>Anonymous<span className="pull-right-container"><span className="label label-success pull-right" id="ticket_anonymous">{this.state.tanonymous}</span></span></Link></li>
                                    <li hidden={this.state.ishidden}><Link to="/dashboard/messages/pending"><i className="fa fa-circle-o text-red"></i>Pending<span className="pull-right-container"><span className="label label-danger pull-right" id="ticket_pending">{this.state.tpending}</span></span></Link></li>
                                    <li hidden={this.state.ishidden}><Link to="/dashboard/messages/ongoing"><i className="fa fa-circle-o text-yellow"></i>Ongoing<span className="pull-right-container"><span className="label label-warning pull-right" id="ticket_ongoing">{this.state.tongoing}</span></span></Link></li>
                                    <li hidden={this.state.ishidden}><Link to="/dashboard/messages/closed"><i className="fa fa-circle-o text-violet"></i>Closed<span className="pull-right-container"><span className="label label-info pull-right" id="ticket_closed">{this.state.tclosed}</span></span></Link></li>
                                    <li hidden={this.state.ishidden}><Link to="/dashboard/messages/revoked"><i className="fa fa-circle-o text-red"></i>Revoked<span className="pull-right-container"><span className="label label-danger pull-right" id="ticket_revoked">{this.state.trevoked}</span></span></Link></li>
                                </ul>
                            </li>
                            <li className="treeview"  hidden={this.state.ishidden}>
                                <Link to="/dashboard/feedbacks">
                                    <i className="fa fa-tasks"></i>
                                    <span>Feedbacks</span>
                                        <span className="pull-right-container">
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </span>
                                </Link>
                                 <ul className="treeview-menu">
                                    <li><Link to="/dashboard/feedbacks"><i className="fa fa-circle-o text-blue"></i>All</Link></li>
                                    <li><Link to="/dashboard/feedbacks/pending"><i className="fa fa-circle-o text-red"></i>Pending<span className="pull-right-container"><span className="label label-danger pull-right" id="feedback_pending">{this.state.fpending}</span></span></Link></li>
                                    <li><Link to="/dashboard/feedbacks/accepted"><i className="fa fa-circle-o text-yellow"></i>Accepted<span className="pull-right-container"><span className="label label-warning pull-right" id="feedback_accepted">{this.state.faccepted}</span></span></Link></li>
                                    <li><Link to="/dashboard/feedbacks/revoked"><i className="fa fa-circle-o text-red"></i>Revoked<span className="pull-right-container"><span className="label label-danger pull-right" id="feedback_revoked">{this.state.frevoked}</span></span></Link></li>
                                </ul>
                            </li>*/}
                            <li className="header">
                                <i className="fa fa-envelope">&nbsp;</i>CONTACT US
                            </li>
                            <li>
                                <Link to="/dashboard/messages"><i className="fa fa-circle-o text-blue"></i>All</Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/messages/anonymous"><i className="fa fa-circle-o text-green"></i>Anonymous
                                    <span className="pull-right-container">
                                        <span className="label label-success pull-right" id="ticket_anonymous">{this.state.tanonymous}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/messages/pending"><i className="fa fa-circle-o text-red"></i>Pending
                                    <span className="pull-right-container">
                                        <span className="label label-danger pull-right" id="ticket_pending">{this.state.tpending}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/messages/ongoing"><i className="fa fa-circle-o text-yellow"></i>Ongoing
                                    <span className="pull-right-container">
                                        <span className="label label-warning pull-right" id="ticket_ongoing">{this.state.tongoing}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/messages/closed"><i className="fa fa-circle-o text-violet"></i>Closed
                                    <span className="pull-right-container">
                                        <span className="label label-info pull-right" id="ticket_closed">{this.state.tclosed}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/messages/revoked"><i className="fa fa-circle-o text-red"></i>Revoked
                                    <span className="pull-right-container">
                                        <span className="label label-danger pull-right" id="ticket_revoked">{this.state.trevoked}</span>
                                    </span>
                                </Link>
                            </li>
                            <li className="header" hidden={this.state.ishidden}>
                                <i className="fa fa-tasks">&nbsp;</i>FEEDBACKS
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/feedbacks" ><i className="fa fa-circle-o text-blue"></i>All</Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/feedbacks/pending"><i className="fa fa-circle-o text-red"></i>Pending
                                    <span className="pull-right-container">
                                        <span className="label label-danger pull-right" id="feedback_pending">{this.state.fpending}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/feedbacks/accepted"><i className="fa fa-circle-o text-yellow"></i>Accepted
                                    <span className="pull-right-container">
                                        <span className="label label-warning pull-right" id="feedback_accepted">{this.state.faccepted}</span>
                                    </span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/feedbacks/revoked"><i className="fa fa-circle-o text-red"></i>Revoked
                                    <span className="pull-right-container">
                                        <span className="label label-danger pull-right" id="feedback_revoked">{this.state.frevoked}</span>
                                    </span>
                                </Link>
                            </li>
                            <li className="header active" hidden={this.state.ishidden}>
                                <i className="fa fa-user">&nbsp;</i>USERS
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/users_all"><i className="fa fa-users"></i>
                                    <span>All</span>
                                </Link>
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/users_create"><i className="fa fa-user-plus"></i>
                                    <span>Create</span>
                                </Link>
                            </li>
                            <li className="header active" hidden={this.state.ishidden}>
                                <i className="fa fa-arrows">&nbsp;</i>OTHERS
                            </li>
                            <li hidden={this.state.ishidden}>
                                <Link to="/dashboard/FAQ"><i className="fa fa-info-circle"></i>
                                    <span>FAQ's</span>
                                </Link>
                            </li>
                        </ul>
                    </section>
                </aside>
            )
        } else {
            return null;
        }
    }
});

module.exports = Aside;