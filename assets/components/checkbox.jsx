const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
module.exports = createReactClass({
    displayName: 'Checkbox.jsx',
    getInitialState: function(data) {
       return {
          isChecked: false,
          label: "haha",
       }
    },
    handleSubmit(e){
        e.preventDefault();
        
    },
    componentDidMount: function () {
     
    },
    toggleCheckboxChangea: function() {
    const { handleCheckboxChange, label } = this.props;

    this.setState(({ isChecked }) => (
      {
        isChecked: !isChecked,
      }
    ));

    handleCheckboxChange(label);
  },
    handleInput: function(e) {
     
    },
    render: function() {
        const { label } = this.props;
        const { isChecked } = this.state;
        return (
             <div className="checkbox">
              <label>
                <input type="checkbox" value={label} checked={isChecked} onChange={this.toggleCheckboxChange}/>
                {label}
              </label>
            </div>
        );
    }
});
