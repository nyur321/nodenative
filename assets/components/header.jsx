const React = require('react');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const Link = ReactRouter.Link;
var Logout = require('./logout.jsx');
var axios = require('axios');

var Header = createReactClass({
    render: function () {
        var isAdmin = this.props.isAdmin;
        var index = this.props.index;
        var isCitizen = this.props.isCitizen;
        if (isAdmin) {
            return (
                <header className="main-header">
                    <Link to="/dashboard" className="logo">
                        <span className="logo-mini"><b><i className="fa fa-address-book-o"></i>&nbsp;</b></span>
                        <span className="logo-lg">{/*<i className="fa fa-address-book-o"></i>&nbsp;*/}<b>Beta Version</b></span>
                    </Link>
                    <nav className="navbar navbar-static-top">
                        <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </a>
                        <Logout index={index}/>
                    </nav>
                </header>
            )
        }  else if(isCitizen){
            return (
                <header className="main-header">
                    <nav className="navbar navbar-static-top">
                        <div className="container">
                            <div className="navbar-header">
                                <Link to ="/client" className="navbar-brand">{/*<i className="fa fa-address-book-o"></i>&nbsp;*/}
                                    <b>Beta Version</b></Link>
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse">
                                    <i className="fa fa-bars"></i>
                                </button>
                            </div>
                            <Logout index={index}/>
                        </div>
                    </nav>
                </header>
            )
        } else {
            return (
                <header className="main-header">
                    <nav className="navbar navbar-static-top">
                        <div className="container">
                            <div className="navbar-header">
                                <a className="navbar-brand">{/*<i className="fa fa-address-book-o"></i>&nbsp;*/}
                                    <b>Beta Version</b></a>
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse">
                                    <i className="fa fa-bars"></i>
                                </button>
                            </div>
                            <div className="collapse navbar-collapse pull-left" id="navbar-collapse">
                                <ul className="nav navbar-nav">
                                    <li>
                                        <Link to='/'>Home <span className="sr-only">(current)</span></Link>
                                    </li>
                                    <li>
                                        <Link to='/contact'>Contact Us<span className="sr-only"></span></Link>
                                    </li>
                                    <li>
                                        <Link to='/feedback'>Feedback<span className="sr-only"></span></Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>
            )
        }

    }

});

module.exports = Header;