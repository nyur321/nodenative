const React = require('react');
const createReactClass = require('create-react-class');

module.exports = createReactClass({
    displayName: 'selection_box',
    getInitialState: function (data) {
        return {
            name:'checkbox-list-component',
            data:[],
            styles: {
                ul:{
                    backgroundColor:"#428BCA",
                }
            },
            element_class: {
                ul: "list-group checked-list-box",
                li:"list-group-item"
            }
        }
    },
    componentWillMount:function () {
        var data = this.props.data,new_data = [];
        if(Array.isArray(data)){
            new_data = data.map(function(d,key){
                d.selected = false;
                d.settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };
                d.iconClass = "glyphicon glyphicon-unchecked";
                d.css = {
                    cursor:'pointer',
                };
                d.key = key;
                return d;
            });


        }


        this.setState({name:this.props.name});
        this.setState({data:Object.assign(this.state.data,new_data)});

    },
    handleClick:function (e) {
        var key = parseInt($(e.target).attr('id').replace('item-list-prop-',''),10);
        if(this.state.data[key].selected){
            $(e.target).children('input').attr('selected',false);
            $(e.target).removeClass('active');
            $(e.target).children('span').removeClass('glyphicon-check');
            $(e.target).children('span').addClass('glyphicon-unchecked');
            this.state.data[key].selected = false;
        }else{
            $(e.target).children('input').attr('selected',true);
            $(e.target).addClass('active');
            $(e.target).children('span').addClass('glyphicon-check');
            $(e.target).children('span').removeClass('glyphicon-unchecked');
            this.state.data[key].selected = true;
        }
        this.props.onSelect(e,this.state.data,key);
    },
    handleChange:function(e){
        var key = parseInt($(e.target).attr('id').replace('item-list-prop-',''),10);
        this.props.onSelect(e,this.state.data,key);
    },
    render:function () {

        return (
            <ul style={this.state.styles.ul} className={this.state.element_class.ul}>
                {this.state.data.map(function (v, j) {
                    return (
                        <li className="list-group-item" id={"item-list-prop-"+j} key={j} onClick={this.handleClick} style={v.css}>
                            <span style={{left:'-5px'}} className={v.iconClass}></span>
                                <input type="checkbox" selected={v.selected} onChange={this.handleChange} name={this.state.name} value={v.value} key={j}
                                       className="hidden"/>
                                {v.label}
                        </li>
                    )
                }.bind(this))}

            </ul>
        )
    }
});

