const React = require('react');
const ReactRouter = require('react-router');
const createReactClass = require('create-react-class');
const Link = ReactRouter.Link;
var Logout = require('./logout.jsx');
module.exports = createReactClass({
    render: function(){
        function Navigation(props) {
            var isAuthenticated = props.isAuthenticated;
                if (isAuthenticated) {
                    return (  
                        <div className="collapse navbar-collapse pull-left" id="navbar-collapse"> 
                            <ul className="nav navbar-nav">
                                <li> 
                                    <Link to='/'>Home <span className="sr-only">(current)</span></Link>
                                </li>
                                        
                                <li>
                                    <Link to='/contact'>Contact Us<span className="sr-only"></span></Link>
                                </li>
                            </ul>
                        </div>
                    );
                }
            return (
                    <Logout/>
            );
        }
        return (
           <header className="main-header">
                <nav className="navbar navbar-static-top">
                    <div className="container">
                        <div className="navbar-header">
                            <a className="navbar-brand"><i className="fa fa-address-book-o"></i>&nbsp;<b>OPACD</b></a>
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i className="fa fa-bars"></i>
                            </button>
                        </div>
                        <div className="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to='/'>Home <span className="sr-only">(current)</span></Link>
                                </li>

                                <li>
                                    <Link to='/contact'>Contact Us<span className="sr-only"></span></Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
});
