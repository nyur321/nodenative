const React = require('react');
const ReactRouter = require('react-router');
const Router = ReactRouter.Router;
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const Link = ReactRouter.Link;

module.exports = createReactClass({
    render: function(){
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                    <b>Powered by MITD</b>
                </div>
                <strong>Copyright &copy; 2017 <a href="http://www.baguio.gov.ph">Baguio City Hall</a>.</strong> All rights
                reserved.
            </footer>
        );
    }
});
