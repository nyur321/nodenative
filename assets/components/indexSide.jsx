const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
module.exports = createReactClass({
    displayName: 'Index',
    render: function () {
        var isAdmin = this.props.isAdmin;
        var authenticated = this.props.isAuthenticated;
        var Loginbox = function(){
            if(authenticated){
                return (   
                    <div>
                        <Link to="/client" className="btn btn-primary btn-block">Back to Main</Link>
                    </div>
                );
            }else{
                return (
                    <div>
                        <p className="text-muted text-center">Please login or create a new account to be able to experience the following services:</p>
                        <hr/>
                        <div>
                            <p><i className="fa fa-check-square-o">&nbsp;Track and monitor your transaction</i></p>
                            <p><i className="fa fa-check-square-o">&nbsp;Interactive communication</i></p>
                            <p><i className="fa fa-check-square-o">&nbsp;Real-time notification</i></p>
                            <p><i className="fa fa-check-square-o">&nbsp;View your message/feedback history</i></p>
                        </div>
                        <hr/>
                        <Link to="/login" className="btn btn-primary btn-block"><b>Login</b></Link>
                        <hr/>
                        <Link to="/register" className="btn btn-primary btn-block"><b>Register a new account</b></Link>
                    </div>
                );
            }
        };
        if(isAdmin){
            return null;
        }else{
            return (
                <div className="col-md-4">
                    <div className="box box-info">
                        <div className="box-body box-profile">
                            <img className="profile-user-img img-responsive img-circle img-large" src="/public/img/baguio-seal.png"/>
                            <h3 className="text-center">Online Public Assistance and Complaints Desk</h3>
                            <Loginbox/>
                        </div>
                    </div>
                </div>
            );
        }

    }
});

