const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
module.exports = createReactClass({
    getInitialState: function () {
        return {
            id:'modal_main',
            title: 'Default title',
            body: <p>This is the main modal</p>,
        }
    },
    componentDidMount: function () {
        this.setState({title:this.props.title});
        this.setState({id:this.props.id});
        this.setState({body:this.props.body});
    },
    render: function () {
        return (
            <div className="modal fade modal-component" tabIndex="-1" role="dialog" id={this.state.id} ref={this.state.id}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title">{this.state.title}</h4>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        <div className="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});