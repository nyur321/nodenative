const React = require('react');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const Link = ReactRouter.Link;
const axios = require('axios');
module.exports = createReactClass({
     getInitialState: function(data) {
        return {
            loading: true
        }
    },
    componentDidMount() {
        setTimeout(() => this.setState({ loading: false }), 1500); 
    },
    handleClick() {
        axios({
          method: 'get',
          url: '/api/user/logout'
        }).then(function() {
            browserHistory.push('/login');
            alert('Are you sure you want to logout?');
        })
    },
    render: function(){
        var { loading } = this.state;
        if(loading) {
            return <div className="fa fa-spinner fa-spin pull-right"></div>
        }
        return (
            <header className="main-header">
              <nav className="navbar navbar-static-top">
                  <div className="container">
                      <div className="navbar-header">

                          <a className="navbar-brand">{/*<i className="fa fa-address-book-o"></i>*/}&nbsp;<b>Beta Version</b></a>
                          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                              <i className="fa fa-bars"></i>
                          </button>
                      </div>
                      <div className="navbar-custom-menu pull-right">
                        <ul className="nav navbar-nav">
                          <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className= "fa fa-user-circle"></i>&nbsp;Firstname Lastname <span className="caret"></span></a>
                              <ul className="dropdown-menu" role="menu">
                                <li><Link to="/dashboard/profile"><i className="fa fa-user-o"></i>View Profile</Link></li>
                                <li className="divider"></li>
                                <li><a onClick={this.handleClick}><i className="fa fa-power-off"></i>Logout</a></li>
                              </ul>
                          </li>
                        </ul>
                      </div>
                  </div>
              </nav>
          </header>
        );
    }
});
