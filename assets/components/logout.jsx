const React = require('react');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const Link = ReactRouter.Link;
var axios = require('axios');

class Logout extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true
        }

    }
    componentDidMount(){
        this.setState({loading: false});
    }
    handleClick(){
        axios({
            method: 'get',
            url: '/api/user/logout'
        }).then(function () {
            window.location.href = '/login';
        })
    }
    render() {
        var {loading} = this.state;
        var index = this.props.index;
        if (loading) {
            return <div className="fa fa-spinner fa-spin pull-right"></div>
        }
        return (
            <div className="navbar-custom-menu pull-right">
                <ul className="nav navbar-nav">
                    <li className="dropdown user user-menu">
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i
                            className="fa fa-user-o"></i> &nbsp; {index.user.name.first_name} &nbsp; {index.user.name.last_name}
                            <span className="caret"></span></a>
                        <ul className="dropdown-menu" role="menu">

                            <li className="user-header">
                                <p>
                                    {index.user.name.first_name} &nbsp; {index.user.name.last_name}&nbsp;-&nbsp;{index.user.role_name}
                                    <small>{index.user.department_name ? index.user.department_name : ''}</small>
                                </p>
                            </li>
                            <li className="user-body">
                                <div className="row">
                                    <div className="col-xs-4 text-center">

                                    </div>
                                    <div className="col-xs-4 text-center">

                                    </div>
                                    <div className="col-xs-4 text-center">

                                    </div>
                                </div>
                            </li>
                            <li className="user-footer">
                                <div className="pull-left">
                                    <Link to="/dashboard/profile" className="btn btn-default btn-flat"><i
                                        className="fa fa-user-o">&nbsp;</i>View Profile</Link>
                                </div>
                                <div className="pull-right">
                                    <a onClick={this.handleClick} className="btn btn-default btn-flat"><i
                                        className="fa fa-power-off">&nbsp;</i>Logout</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        )
    }
}

module.exports = Logout;