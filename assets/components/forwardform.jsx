const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const bootbox = require('bootbox');
var IndexSide = require('../components/indexSide.jsx');

var Checkbox = createReactClass({
    getInitialState: function (data) {
        return {
            isChecked: false,
            departments_options: [],
            department_id: ''
        }
    },
    componentDidMount() {
        var label = this.props.label;
        var department = this.props.department;
        this.setState({
            label: label,
            value: department.value
        });
    },
    handleChange () {
      var handleCheckboxChange = this.props.handleCheckboxChange;
      var department = this.props.department;
      this.setState({isChecked: !this.state.isChecked});
      handleCheckboxChange(department);
    },
    render: function () {
        return (
            <div className="checkbox">
              <label>
                <input type="checkbox" value={this.state.value} name="department_id" checked={this.state.isCheked} onChange={this.handleChange}/>
                {this.state.label}
              </label>
            </div>
        )
    }
});
module.exports = createReactClass({
      displayName: 'Forward Message',
      getInitialState: function(data) {
        var results = this.props.results;
        var index = this.props.index.data;
        return {
          index:[],
          departments_options: [],
          isChecked: false,
          forwardhistories:[],
          id: results.id,
          form: {
            id: results.id,
            department_id: '',
            forwarded_by: index.user.id,
            comments: '',
          },
        }
      },
      componentWillMount () {
        this.selectedCheckboxes = new Set();
      },
      componentDidMount: function () {
        var index = this.props.index;
        this.setState({
            form: Object.assign(this.state.form), 
            departments_options: index.data.forms.departments,
        });
        axios({
            method: "GET",
            url: "/api/ticket/forward/all/" + this.state.id
        }).then(function (response) {
            this.setState({
              forwardhistories: response.data
            })
        }.bind(this));
      },
      toggleCheckbox (department) {
         if (this.selectedCheckboxes.has(department)) {
            this.selectedCheckboxes.delete(department);
          } else {
            this.selectedCheckboxes.add(department);
          }
      },
      handleSubmit(e){
        e.preventDefault();
        var results = this.props.results;
        var index = this.props.index.data; 
        for (const department of this.selectedCheckboxes) {
          this.setState({ isSubmitting: true });
          var data = {
              id: this.state.form.id,
              department_id: department.value,
              forwarded_by: this.state.form.forwarded_by,
              comments: this.state.form.comments,
          }
          axios({
            method: 'post',
            url: '/api/ticket/forward',
            data: data,
          }).then(function(response){
              this.setState({
                isSubmitting: false,
              });
              NotificationManager.success('Successfully forwarded to' + ' ' +department.label, 'Forward Form');
          }.bind(this));
          axios({
            method: "GET",
            url: "/api/ticket/forward/all/" + results.id
          }).then(function (response) {
              this.setState({
                forwardhistories: response.data
              })
          }.bind(this));
        }
      },
      removeDepartment(e) {
        e.preventDefault();
        var dept_id = $(e.target).attr('data-dept-id');
        var ticket_id = $(e.target).attr('data-dept-ticket_id');
        axios({
          method: "delete",
          url: "/api/ticket/forward/delete/" + dept_id
        })
        axios({
          method: "GET",
          url: "/api/ticket/forward/all/" + ticket_id
        }).then(function (response) {
            this.setState({
              forwardhistories: response.data
            })
        }.bind(this));
        NotificationManager.info('Department is Successfully Removed');
      },
      handleInput: function(e) {
        var input = {};
          if (e.target.nodeName == 'SELECT') {
              input[e.target.name] = e.target.value;
          } else {
              input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
          }
        this.setState({form: Object.assign(this.state.form, input)});
      },
      render: function() {
          var onchange = this.toggleCheckbox;
          var index = this.props.index;
          var removeDepartment = this.removeDepartment;
          var Checkboxz = function(props){
            return(
              <p className="text-muted">{props.name} <button data-dept-id={props.id} data-dept-ticket_id={props.ticket_id} onClick={removeDepartment} className="pull-right"><i className="fa fa-close text-danger"></i> Remove</button></p>
            )
          } 
          return (
            <section>
              <form onSubmit={this.handleSubmit} ref="forward_form">
                  <div className="box-body">
                      <div className="box-body">
                         <div className="form-group">
                              <label>Comment</label>
                              <textarea className="form-control" rows="3" placeholder="Your comment here" name="comments"  onChange={this.handleInput}/>
                              <span className="error"></span>
                          </div>
                          <div className="form-group" hidden={this.state.forwardhistories.length > 0 ? false : true}>
                            <div className="bg-blue">
                                <p className="text-center">Forwarded to:</p>
                            </div>
                           {this.state.forwardhistories.map(function(dept,j) {
                              return <Checkboxz name={dept.department.name} key={j} id={dept.id} ticket_id={dept.ticket_id}/>
                            })}
                          </div>
                          <div className="bg-black">
                            <p className="text-center">Forward this message?</p>
                          </div>
                          <div className="box-group" id="accordion">
                              <p className="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                  Select Department &nbsp;
                                  <i className="fa fa-angle-down"></i>
                                </a>
                              </p>
                              <div id="collapseOne" className="panel-collapse collapse">
                                <div className="box-body">
                                    <div className="form-group">
                                        <div className="checkbox" >
                                           {this.state.departments_options.map(function(department,i) {
                                              return <Checkbox department={department} label={department.label} key={i} handleCheckboxChange={onchange} index={index}/>
                                            })}
                                        </div>
                                        <div className="row">
                                          <button type="submit" className="form-control btn-flat btn btn-primary btn-block" disabled={this.state.isSubmitting}>Forward</button>
                                        </div>
                                        <span className="error"></span>
                                    </div>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </form>
            </section>            
          );
      }
});
