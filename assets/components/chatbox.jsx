const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const moment = require('moment');
import StarRatingComponent from 'react-star-rating-component';

class RatingBox extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        var rating = 0;

        return (
            <div>

            </div>
        )
    }

}
class ChatBox extends React.Component {
    constructor(props) {
        super(props);

        var messages = [];

        this.state = {
            room:'',
            name: '',
            subject: '',
            departments: [],
            messages: messages,
            chat_info: '',
            chat_message:'',
            index:0,
            user_id:null,
            chat_enable:true,
            chat_show_rating:false,
            chat_show_overlay:false,
            chat_overlay_content:""
        }
    }

    sendButton(e) {
        var messages = this.state.messages;
        var message_id = this.state.index+1;
        if(typeof this.props.onUpdate == 'function'){
            this.props.onUpdate({
                id:message_id,
                all:this.state.messages
            });
        }
        var message = {
            id:message_id,
            message: this.state.chat_message,
            type:'message',
            date: '',
            from: {
                name: 'You',
                avatar: ''
            },
            time: moment().toDate()
        };
        messages.push(message);
        this.setState({messages:Object.assign(messages,this.state.messages)});
        this.setState({chat_message:''});
        this.setState({index:message_id});

        if(typeof this.props.onAction == 'function'){
            this.props.onAction({
                type:'send',
                data:{
                    current:message,
                    messages:this.state.messages
                }
            });
        }

    }

    inputTyping(e) {

        this.setState({chat_info: 'You are typing'});
        setTimeout(function () {
            this.setState({chat_info: ''})
        }.bind(this), 3000);
        this.setState({chat_message:e.target.value});
    }

    inputFocus(e) {
        // console.log(e.target.relatedTarget);
        // e.target

    }

    updateMessage(e){


    }

    inputBlur(e){


    }

    chatTyping(e){
        if(e.key == 'Enter'){
            this.sendButton(e);
        }
    }

    componentDidMount() {
        // console.log(this.props);
        var messages = this.state.messages;

        messages = messages.concat(this.props.messages);
        // console.log(messages);
        // this.setState({messages:Object.assign(messages,this.state.messages)});



    }

    pushMessage() {
        $('#chat-input-info').html('You are typing')
    }

    onStarClick(new_rating){
        console.log('Rating is ');
        console.log(new_rating);
    }
    endChat(e){
        if(this.state.chat_show_overlay){
            this.setState({chat_show_overlay:false});
            this.setState({chat_enable:true});

        }else{
            this.setState({chat_show_overlay:true});
            this.setState({chat_enable:false});
        }


    }

    printChat(e){
        if(this.state.chat_show_rating){
            this.setState({chat_show_rating:false});
        }else{
            this.setState({chat_show_rating:true});
        }


    }

    render() {

        var messages = this.props.messages || this.state.messages;
        var departments = this.props.departments || this.state.departments;
        var chat_info = this.props.chat_info || this.state.chat_info;
        var chat_show_overlay = this.props.chat_show_overlay || this.state.chat_show_overlay;
        var chat_show_rating = this.props.chat_show_rating || this.state.chat_show_rating;
        var chat_rating_content = <div>{chat_show_rating && <div className="chat-rating-container"><StarRatingComponent
                name="rate1"
                starCount={5}
                value={0}
                onStarClick={this.onStarClick.bind(this)}
            /></div>}</div>;

        var chat_overlay_content = this.props.chat_overlay_content || this.state.chat_overlay_content;

        return ( <div className="box box-success">
                <div className="box-header">
                    {this.props.chat_name || this.state.name} - {this.props.chat_subject || this.state.subject}
                    <div className="pull-right box-tools">
                        <div className="btn-group">
                            <button type="button" className="btn btn-flat btn-danger" onClick={this.endChat.bind(this)}>End Chat</button>
                            <button type="button" className="btn btn-flat btn-primary" onClick={this.printChat.bind(this)}>Print</button>
                        </div>
                    </div>

                </div>
                <div className="box-body chat-container" id="chat-box">
                    {messages.map(function (message, key) {
                        return ( <div className="item" key={key}>
                            <p key={key}>
                                <a href="#" className="name" key={key}>
                                    <small className="text-muted pull-right">
                                        <i className="fa fa-clock-o"></i> {message.time}
                                    </small>
                                    {message.type == 'system' ? <span style={{color:'red'}}>System</span> : <span>{message.from.name}</span>}
                                </a><br/>{message.message}
                            </p>
                        </div>);
                    })}
                    {chat_show_overlay && <div className="chat-overlay" id="chat-overlay">
                        {chat_overlay_content}
                    </div>}
                    <div className="chat-notification" id="chat-input-info">{chat_info}</div>
                </div>
                <div className="box-footer">
                    <div className="input-group">
                        <input onFocus={this.inputFocus.bind(this)} onBlur={this.inputBlur.bind(this)} onKeyPress={this.chatTyping.bind(this)} onChange={this.inputTyping.bind(this)}
                               className="form-control ${this.state.chat_enable ? '' : 'disabled'}" readOnly={!this.state.chat_enable} placeholder="Type message..." id="" value={this.state.chat_message}/>
                        <div className="input-group-btn">
                            <button type="button" onClick={this.sendButton.bind(this)} className="btn btn-success btn-flat chat-send-btn">
                                <i className="fa fa-paper-plane"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};


module.exports = ChatBox;