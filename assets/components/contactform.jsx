const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
var IndexSide = require('../components/indexSide.jsx');
module.exports = createReactClass({
      displayName: 'Contact Us',
      getInitialState: function(data) {
        var index = this.props.index;
          return {
              title:'',
              index:[],
              departments_options: [],
              ishidden: false,
              form: {
                id: '',
                status_id: 3,
                department_id: 1,
                name: index.authenticated ? index.user.name.first_name + ' ' + index.user.name.last_name: '',
                email: index.authenticated ? index.user.email : '',
                subject: '',
                message: '',
                anonymous: index.user.anonymous
              },
          }
      },
      handleSubmit(e){
        e.preventDefault();
        //inputs   
        var form_data = this.state.form; 
        this.setState({isSubmitting:true})
        axios({
          method: 'post',
          url: '/api/contact/submit',
          data: form_data,
        }).then(function(response){
            if(response.data.validations.length > 0) {
                $(this.refs.contact_us_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                this.setState({isSubmitting:false})  
                NotificationManager.error('Please check your input fields', 'Something went wrong...');
            } else {
                var index = this.props.index;
                  this.setState({
                    isSubmitting: false,
                    form: {
                      id: '',
                      name: index.authenticated ? index.user.name.first_name + '' + index.user.name.last_name: '',
                      email: index.authenticated ? index.user.email : '',
                      subject: '',
                      message: '',
                      upload: '',
                      anonymous: index.user.anonymous
                    }
                })
                $(this.refs.contact_us_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                          
                NotificationManager.success('Successfully submited', 'Contact Form');
            }
        }.bind(this));
       /* var data = new FormData();
        data.append('file', this.fileInput.files[0]);
        axios.post('/api/uploads', data).then((response) => {
          console.log(response);
        }); */
      },
      componentDidMount: function () {
        var index = this.props.index;
        this.setState({
            form: Object.assign(this.state.form), 
            ishidden: index.authenticated,
            departments_options: index.data.forms.departments
        });
       /* axios({
            method: "GET",
            url: '/api/captcha'
        }).then(function(response){
            this.setState({
              captcha: response
            });
        }.bind(this));*/
      },
      handleInput: function(e) {
        var input = {};
        input[e.target.name] = e.target.value;
        this.setState({form: Object.assign(this.state.form, input)});
      },
      render: function() {
          return (
              <div className="box box-primary">
                <div className="box-header with-border">
                      <div className="text-center">
                          <h4>Do you have any questions or concerns? Please let us know</h4>
                      </div>
                  </div>
                  <form onSubmit={this.handleSubmit} ref="contact_us_form">
                    <div className="box-body">
                        {/*<div className="bg-black">
                              <p className="text-center">Do you have any question or concern?</p>
                        </div>*/}
                        <span>
                              <p className="text-danger">Fields with * symbol are required</p>
                        </span>
                           {/*<div className="form-group">
                                <label>Choose Department</label>
                                <select className="form-control" value={this.state.form.departments} name="department_id" onChange={this.handleInput}>
                                    {this.state.departments_options.map(function (v, j) {
                                        return <option key={j} value={v.value}>{v.label}</option>
                                    })}
                                </select>
                                <span className="error"></span>
                            </div>*/}
                            <div className="form-group" hidden={this.state.ishidden}>
                                <label>Name</label>
                                <input type="text" className="form-control" name="name" value={this.state.form.name} onChange={this.handleInput}/>
                                <span className="error"></span>
                            </div>
                            <div className="form-group" hidden={this.state.ishidden}>
                                <label>Email *</label> <span className= "text-warning">-- Note: Any reply from us will be send to this email</span>
                                <input type="text" className="form-control" name="email" value={this.state.form.email} onChange={this.handleInput}/>
                                <span className="error"></span>
                            </div>
                            <div className="form-group">
                                <label>Subject</label> 
                                <input type="text" className="form-control" name="subject" value={this.state.form.subject} onChange={this.handleInput}/>
                            </div>
                          {/*  <div className="form-group">
                                <label>Attach File (If Applicable)</label> 
                                <input type="file" className="form-control" ref={input => {this.fileInput = input}}/>
                            </div>*/}
                            <div className="form-group">
                                <label>Message *</label>
                                <textarea className="form-control" rows="5" placeholder="Your Question/Concern/Request here" name="message" value={this.state.form.message} onChange={this.handleInput}/>
                                <span className="error"></span>
                            </div>
                            {/*<div className="form-group">
                                <label>CAPTCHA</label>
                                <p className="text-muted">This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.</p>
                                <h4>{captcha}</h4>
                                <input type="text" className="form-control"/>
                                <span className="error"></span>
                            </div>*/}
                      </div>
                      <div className="box-footer">
                            <div className="pull-right">
                                <button type="submit" className="btn btn-block btn-primary" disabled={this.state.isSubmitting}>&nbsp;Submit Form</button>
                            </div>
                      </div>
                  </form> 
             </div>            
          );
      }
});
