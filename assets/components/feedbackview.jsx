const React = require('react');
var createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const axios = require('axios');
const bootbox = require('bootbox');
module.exports = createReactClass({
      displayName: 'Feedback',
      getInitialState: function(data) {
      var results = this.props.results;
      var department = this.props.department;
      var feedbacktype = this.props.feedbacktype;
      return {
            title:'',
            index:[],
            results:[],
            id: '',
            type: feedbacktype.type,
            entity_concerned:  results.entity_concerned,
            facts_details:  results.facts_details,
            recommendations: results.recommendations,
            name: results.name,
            department: department.name,
            office_agency: results.office_agency,
            address: results.address,
            email: results.email,
          }
      },
      componentDidMount: function () {
          axios({
            method: 'post',
            url: '/api/feedback/statuses',
        }).then(function (response) {
            this.setState({
              feedback: response.data
            });
        }.bind(this));
      },
      render: function() {
        var index = this.props.index.data;
        var results = this.props.results;
        var feedback = this.state.feedback;

        var Accept = function() {
        bootbox.confirm({
          message: "Accept this Feedback??",
          size: 'small',
          buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
            label: 'No',
            className: 'btn-danger'
          }
        },
        callback: function (result) {
          if (result == true) {
            axios({
            method: "post",
            url: "/api/feedback/update/" + results.id,
            data: {status_id: 2}
            }).then(function (response) {
                NotificationManager.success('Ticket message is successfully accepted');
                $('#view_feedback_modal').modal('toggle');
                $('#feedback_pending').text(feedback.pending-1);
                $('#feedback_accepted').text(feedback.accepted+1);
                var table = $('.pending_feedbacks').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/feedback/pending",
                        "type": "POST"
                    },
                });
                table.ajax.reload();
            })
          }
        }
      }); 
    }
    var Revoked = function() {
      bootbox.confirm({
        message: "Revoke this feedback?",
        size: 'small',
        buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
        },
        callback: function (result) {
          if (result == true) {
            axios({
            method: "post",
            url: "/api/feedback/update/" + results.id,
            data: {status_id: 5}
            }).then(function (response) {
                NotificationManager.success('Ticket message is successfully revoked');
                $('#view_feedback_modal').modal('toggle');
                $('#feedback_pending').text(feedback.pending-1);
                $('#feedback_revoked').text(feedback.revoked+1);
                var table = $('.pending_feedbacks').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/feedback/pending",
                        "type": "POST"
                    },
                });
                table.ajax.reload();
            })
          }
        }
      }); 
    }
    var SetStatus = function() {
      if(index.user.role_id == 1) {
        if(results.status_id == 3) {
          return (
           <div className="row">
              <div className="col-md-6">
                <button type="submit" className="btn btn-flat btn-success btn-block" onClick={Accept}>Accept</button>
              </div>
              <div className="col-md-6">
                <button className="btn btn-flat btn-warning btn-block" onClick={Revoked}>Revoke</button>
              </div>
            </div>
          )
        } else {
          return null
        }
      } else {
        return null;
      }
    }
    return (
      <div>
          <div className="box-body">
              <div className="bg-black">
                  <p className="text-center">Feedback Details</p>
              </div>
              <div className="form-group">
                  <p><strong>Type of Feedback:</strong>&nbsp;{this.state.type}</p>
              </div>
               <div className="form-group">
                  <p><strong >Office concerned or involved:</strong>&nbsp;{this.state.department}</p>
              </div>
              <div className="form-group">
                  <p><strong >Person/ Unit concerned or involved:</strong>&nbsp;{this.state.entity_concerned}</p>  
              </div>
              <div className="form-group">
                  <p><strong>Facts or Details Surrounding the incident:</strong>&nbsp;{this.state.facts_details}</p>
              </div>
              <div className="form-group">
                <strong>Recommendation(s)/ Suggestion(s)/ Desired Action from our Office:</strong>
                <p>&nbsp;{this.state.recommendations}</p>
              </div>
              <div className="bg-black">
                  <p className="text-center">Personal Information of the Sender</p>
              </div>
              <div className="form-group">
                  <p><strong>Full Name:</strong>&nbsp;{this.state.name}</p>
              </div>
              <div className="form-group">
                  <p><strong>Office/Agency:</strong>&nbsp;{this.state.office_agency}</p>
              </div>
              <div className="form-group">
                  <p><strong>Email Address:</strong>&nbsp;{this.state.email}</p>
              </div>
              <div className="form-group">
                  <p><strong>Contact Number:</strong>&nbsp;{this.state.contact}</p>
              </div>
              <div className="form-group">
                  <p><strong>Address:</strong>&nbsp;{this.state.address}</p>
              </div>
              <br/>
          </div>
          <div>
              <SetStatus />
          </div>
      </div>
    );
  }
});
