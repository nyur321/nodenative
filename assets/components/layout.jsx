const React = require('react');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const Link = ReactRouter.Link;
var io = require('socket.io-client');
var axios = require('axios');
var Footer = require('./footer.jsx');
var Aside = require('./aside.jsx');
var Header = require('./header.jsx');
class Layout extends React.Component{
    constructor(props) {
    super(props);
    this.state = {
        isAdmin: '',
        isCitizen: '',
        index: '',
        authenticated: this.props.index.authenticated,
        path: this.props.location.pathname,
        role_id: this.props.index.data.user.role_id,
        count:[],
        ishidden:true
      };
    }
    isAdmin() {
        let index = this.props.index;
        return (index.authenticated && index.data.user.role_id != 3 ? true : false);

    }
    isCitizen() {
        let index = this.props.index;
        return (index.authenticated && index.data.user.role_id == 3 ? true : false);

    }
    index() {
        let index = this.props.index;
        return (index)

    }
    componentDidMount() {
        this.setState({
            ishidden: this.state.authenticated
        });
        if(this.state.authenticated && this.state.path == "/login" || this.state.path == "/register") {
            if(this.state.role_id == 1 || this.state.role_id == 2){
                browserHistory.push('/dashbaord')
            } else if(this.state.role_id == 4) {
                browserHistory.push('/register')
            } else {
                browserHistory.push('/client')
            } 
        }

        if(this.state.authenticated && this.state.role_id == 1){
            var ticket = io.connect('/realtime/contact_notifications');
            var feedback = io.connect('/realtime/feedback_notifications');
            var reply = io.connect('/realtime/reply_notifications');
            var count_status = io.connect('/realtime/count_notifications');
            
            count_status.on('notification',function (notification) {
                axios({
                    method: 'post',
                    url: '/api/ticket/statuses',
                }).then(function (response) {
                    var count = response.data;
                    $('#ticket_anonymous').text(count.anonymous);
                    $('#ticket_pending').text(count.pending);
                    $('#ticket_ongoing').text(count.ongoing);
                    $('#ticket_closed').text(count.closed);
                    $('#ticket_revoked').text(count.revoked);
                });
            });
            
            ticket.on('notification',function (notification) {
                NotificationManager[notification.type || 'info'](notification.message);
                $.playSound('/public/sound/notification_sound.mp3')
                var table = $('.pending_tickets').DataTable();
                table.ajax.reload();

                axios({
                    method: 'post',
                    url: '/api/ticket/statuses',
                }).then(function (response) {
                    var count = response.data;
                    $('#ticket_anonymous').text(count.anonymous);
                    $('#ticket_pending').text(count.pending);
                });
            });
            feedback.on('notification',function (notification) {
                NotificationManager[notification.type || 'info'](notification.message);
                $.playSound('/public/sound/notification_sound.mp3')
                var table = $('.pending_feedbacks').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/feedback/pending",
                        "type": "POST"
                    },
                });
                table.ajax.reload();
                axios({
                    method: 'post',
                    url: '/api/feedback/statuses',
                }).then(function (response) {
                    var count = response.data;
                    $('#feedback_pending').text(count.pending);
                });
            });
            reply.on('notification',function (notification) {
                NotificationManager[notification.type || 'warning'](notification.message);
                $.playSound('/public/sound/notification_sound.mp3')
                var ongoing = $('.ongoing_tickets').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/ticket/ongoing",
                        "type": "POST"
                    },
                });
                ongoing.ajax.reload();
                var messages = $('.messages_list').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/ticket/client/all",
                        "type": "POST"
                    },
                });
                messages.ajax.reload();

            });
        }
        if(this.state.authenticated && this.state.role_id == 3){
            var reply = io.connect('/realtime/reply_notifications_client');
            reply.on('notification',function (notification) {
                NotificationManager[notification.type || 'warning'](notification.message);
                $.playSound('/public/sound/notification_sound.mp3')
                var table = $('.messages_list').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/ticket/client/all",
                        "type": "POST"
                    },
                });
                table.ajax.reload();
            });
        }

        if(this.state.authenticated && this.state.role_id == 2){
            var reply = io.connect('/realtime/forward_notifications');
            reply.on('notification',function (notification) {
                var table = $('.messages_list').DataTable({
                    "processing": true,
                    "retrieve": true,
                    "serverSide": true,
                    "ordering" : false,
                    "ajax": {
                        "url": "/api/ticket/forward/all",
                        "type": "POST"
                    },
                });
                table.ajax.reload();
            });
        }
    }
    render() {
        let bundle_js = '/public/bundle.js';
        var bodyClass = 'hold-transition skin-blue ' + (this.isAdmin() ? 'sidebar-mini' : 'layout-top-nav');
        let index = this.props.index;
        return (
            <html>
            <head>
                <title>{index.title}</title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
                <script dangerouslySetInnerHTML={{__html: 'window.PROPS=' + JSON.stringify(index)}}></script>
                <link rel="stylesheet" href="/public/bundle.css"/>
                <link rel="icon" href="/public/img/baguio-seal.png"/>
            </head>
            <body className={bodyClass}>
                <div className="wrapper">
                    <Header index={this.index()} isAdmin={this.isAdmin()} isCitizen={this.isCitizen()}/>
                    <Aside count={this.state.count} isAdmin={this.isAdmin()} index={this.index()}/>
                    <div className="content-wrapper">
                        <section className="section header-banner" hidden={this.state.ishidden}>
                            <div className="main-container container">
                                <div className="row row-banner">
                                    <header role="banner" id="page-header">
                                        <div className="section master-head">
                                            <img src="/public/img/baguio-seal.png" alt="Home"/>
                                            <div className="master-head-txt">
                                                <h5>Republic of the Philippines</h5>
                                                <h1>The City Government of Baguio</h1>
                                                <h4>Official Website</h4>
                                            </div>
                                        </div>
                                        <div className="section banner">
                                        </div>
                                    </header> 
                                </div>
                            </div>
                        </section>
                        <section className="content" >
                            {this.props.children}
                        </section>
                    </div>
                    <Footer/>
                </div>
                <div>
                    <NotificationContainer/>
                </div>
            <script src="/public/bundle.js"></script>
            </body>
            </html>
        );
    }
} 
module.exports = Layout;
