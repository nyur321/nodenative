const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
module.exports = createReactClass({
    displayName: 'Feedback',
    getInitialState: function(data) {
      var index = this.props.index;
        return {
            title:'',
            index:[],
            feedbacktypes: [],
            departments_options: [],
            ishidden: false,
            form: {
              id: '',
              type: '',
              entity_concerned: '',
              department_id:1,
              facts_details: '',
              recommendations: '',
              name: index.authenticated ? index.user.name.first_name + ' ' + index.user.name.last_name: '',
              office_agency: index.authenticated ? index.user.office_agency: '',
              email: index.authenticated ? index.user.email : '',
              // contact: index.authenticated ? index.user.contact : '',
              address: index.authenticated ? index.user.address : ''
            },
        }
    },
   handleSubmit(e){
      e.preventDefault();
      this.setState({isSubmitting: true})
      var form_data = this.state.form; 
      axios({
        method: 'post',
        url: '/api/feedback/submit',
        data: form_data,
      }).then(function(response){
          if(response.data.validations.length > 0) {
              $(this.refs.feedback_form).find('input,select,textarea').each(function (i,v) {
                 var obj = _.find(response.data.validations,function (o) {
                     return o.param == $(v).attr('name');
                 });
                 if(obj){
                     $(v).siblings('span.error').html(obj.msg);
                 }else{
                     $(v).siblings('span.error').html('');
                 }
              });
              this.setState({isSubmitting:false})
              NotificationManager.error('Please check your input fields', 'Something went wrong...');
          } else {
              var index = this.props.index;
              this.setState({
                  isSubmitting: false,
                  form: {
                      id: '',
                      type: '',
                      entity_concerned: '',
                      facts_details: '',
                      recommendations: '',
                      name: index.authenticated ? index.user.name.first_name + ' ' + index.user.name.last_name: '',
                      office_agency: index.authenticated ? index.user.office_agency: '',
                      email: index.authenticated ? index.user.email : '',
                      contact: index.authenticated ? index.user.contact : '',
                      address: index.authenticated ? index.user.address : '',
                  }
              })
              $(this.refs.feedback_form).find('input,select,textarea').each(function (i,v) {
                 var obj = _.find(response.data.validations,function (o) {
                     return o.param == $(v).attr('name');
                 });
                 if(obj){
                     $(v).siblings('span.error').html(obj.msg);
                 }else{
                     $(v).siblings('span.error').html('');
                 }
              });
              NotificationManager.success('Successfully submited', 'Feedback Form');
          }
      }.bind(this));
    },
    componentDidMount: function () {
      var index = this.props.index;
      this.setState({
          form: Object.assign(this.state.form), 
          ishidden: index.authenticated,
          feedbacktypes:index.data.forms.feedbacktypes,
          departments_options: index.data.forms.departments
      });
    },
    handleInput: function(e) {
      var input = {};
        if (e.target.nodeName == 'SELECT') {
            input[e.target.name] = e.target.value;
        } else {
            input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
        }
      this.setState({form: Object.assign(this.state.form, input)});
    },
    render: function() {
        return (
            <div className="box box-primary">
                <div className="box-header with-border">
                    <div className="text-center">
                        <h4>How we have serve you? Please let us know your feedback</h4>
                    </div>
                </div>
                  <form onSubmit={this.handleSubmit} ref="feedback_form" autoComplete="off">
                      <div className="box-body">
                            {/*<div className="bg-black">
                                  <p className="text-center">Please let us know how we have served you</p>
                            </div>*/}
                            <span>
                                  <p className="text-danger">Fields with * symbol are required</p>
                            </span>
                                <div className="form-group">
                                      <label>Type of Feedback *</label>
                                      <select className="form-control" name="type" onChange={this.handleInput}>
                                          <option value="">-- Select --</option>
                                          {this.state.feedbacktypes.map(function (v, i) {
                                              return <option key={i} value={v.value}>{v.label}</option>
                                          })}
                                      </select>
                                      <span className="error"></span>
                                </div>
                            <div className="form-group">
                                <label htmlFor="in">Persons/ Units concerned or involved *</label>
                                <textarea  className="form-control" rows="2" name="entity_concerned" value={this.state.form.entity_concerned} onChange={this.handleInput}/>
                                <span className="error"></span>
                            </div>
                            {/*<div className="form-group">
                                <label>Choose Department</label>
                                <select className="form-control" value={this.state.form.departments} name="department_id" onChange={this.handleInput}>
                                    {this.state.departments_options.map(function (v, j) {
                                        return <option key={j} value={v.value}>{v.label}</option>
                                    })}
                                </select>
                                <span className="error"></span>
                            </div>*/}
                            <div className="form-group">
                                  <label>Facts or Details Surrounding the incident *</label>
                                  <textarea className="form-control" rows="3" name="facts_details" value={this.state.form.facts_details} onChange={this.handleInput}/>
                                  <span className="error"></span>
                            </div>
                            <div className="form-group">
                                <label>Recommendation/ Suggestion/ Desired Action from our Office *</label>
                                <textarea className="form-control" rows="3" name="recommendations" value={this.state.form.recommendations} onChange={this.handleInput}/>
                                <span className="error"></span>
                            </div>
                            <div className="bg-black" hidden={this.state.ishidden}>
                                  <p className="text-center">Your Personal Information</p>
                            </div>
                            <span>
                                  <p className="text-danger" hidden={this.state.ishidden}>Fields with * symbol are required</p>
                            </span>
                            <div className="form-group has-feedback" hidden={this.state.ishidden}>
                                  <label>Name</label>
                                  <input type="text" className="form-control" name="name" value={this.state.form.name}  onChange={this.handleInput}/>
                            </div>
                            <div className="form-group has-feedback" hidden={this.state.ishidden}>
                                  <label>Office/Agency (If applicable)</label>
                                  <input type="text" className="form-control" name="office_agency" value={this.state.form.office_agency} onChange={this.handleInput}/>
                            </div>
                            <div className="form-group has-feedback" hidden={this.state.ishidden}>
                                  <label>Email *</label>
                                  <input type="text" className="form-control" name="email" value={this.state.form.email}  onChange={this.handleInput}/>
                                  <span className="error"></span>
                            </div>
                           {/* <div className="form-group has-feedback" hidden={this.state.ishidden}>
                                  <label>Contact Number (If any)</label>
                                  <input type="number" className="form-control" name="contact" value={this.state.form.contact}  onChange={this.handleInput}/>
                            </div>*/}
                            <div className="form-group has-feedback" hidden={this.state.ishidden}>
                                  <label>Address</label>
                                  <input type="text" className="form-control" name="address" value={this.state.form.address}  onChange={this.handleInput}/>
                            </div>
                      </div>
                      <div className="box-footer">
                            <div className="pull-right">
                                  <button type="submit" className="btn btn-block btn-primary" disabled={this.state.isSubmitting}>&nbsp;Submit Form</button>
                            </div>
                      </div> 
                </form>
            </div>
        );
    }
});