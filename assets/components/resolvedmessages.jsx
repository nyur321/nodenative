const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const Moment = require('moment');
$.DataTables = require('datatables.net');
var Modal = require('../components/modal.jsx');
var Messageview = require('../components/messageview.jsx');
var Forwardform = require('../components/forwardform.jsx');
module.exports = createReactClass({
      displayName: 'Resolved Messages',
       getInitialState: function () {
        return {
            department: [],
            user: [],
            results: [],
            person:[],
            forward_details:[],
            approved_tickets:[],
            data:{},
            email:''
        }
    },
    componentWillUnmount: function () {

    },
    componentDidMount: function () {
            $(this.refs.resolved_tickets).DataTable({
                "processing": true,
                "ordering" : true,
                "retrieve": true,
                "paging": true,
                "ajax": {
                    "url": "/api/ticket/resolved",
                    "type": "POST"
                },
                "columns": [
                    {
                        "data": "id",
                        "render": function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {"data": "subject",
                        "render": function(data) {
                            return data.substr(0,15) + (data.length > 15 ? '...' : '')
                        }
                    },
                    {
                        "data": "message",
                        "render": function(data) {
                            return data.substr(0,25) + (data.length > 25 ? '...' : '')
                        }

                    },
                    {
                        "data": "user",
                        "render": function(data, type, full, meta) {
                           return data.email
                        }
                    },
                    {
                        "data": "created_at",
                        "render": function(data, type, full, meta) {
                          return  Moment(data).format('lll');
                        }
                    },
                    {
                        "data": null,
                        "render": function (data, type, full, meta) {
                          return (
                              '<a data-message-id="' + data.id + '" class="btn btn-xs btn-flat btn-success open-message-modal">open</a>'
                          )
                        }
                    }
                ]
            });
        $(this.refs.resolved_tickets).on('click','.open-message-modal',function (e) {
            e.preventDefault();
            var message_id = $(e.target).attr('data-message-id');
            $('#view_message_modal').modal('show');
            axios({
                method: "GET",
                url: "/api/ticket/get/" + message_id
            }).then(function (response) {
                this.setState({
                  results: response.data,
                  department: response.data.department,
                  person: response.data.user.person,
                  user: response.data.user,
                  forward_details:response.data.forwardhistories,
                  approved_tickets:response.data.approvedtickets
                });
            }.bind(this));
        }.bind(this));
      },
      render: function() {
        var Messageformz = function () {
            return <Messageview  index={this.props.index} results={this.state.results} approved_tickets={this.state.approved_tickets} forward_details={this.state.forward_details} department={this.state.department}  user={this.state.user} person={this.state.person}/>
        }.bind(this);
         var Forwardformz = function () {
            return <Forwardform  index={this.props.index} results={this.state.results} department={this.state.department}  user={this.state.user}/>
        }.bind(this);
        return (
          <div className="box box-info">
            <div className="box-header with-border">
                <h3 className="box-title"><i className="fa fa-inbox"></i> Resolved Messages</h3>
            </div>
            <div className="box-body table-responsive">
                <table className="table table-striped table-condensed resolved_tickets" ref="resolved_tickets" width="100%">
                    <thead>
                      <tr>
                          <th>No.</th>
                          <th>Subject</th>
                          <th>Message</th>
                          <th>Email</th>
                          <th>Date Sent</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>No.</th>
                          <th>Subject</th>
                          <th>Message</th>
                          <th>Email</th>
                          <th>Date Sent</th>
                          <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
              <Modal id="view_message_modal" title="View Message" results={this.state.results} department={this.state.department} forward_details={this.state.forward_details}  user={this.state.user} person={this.state.person}>
                    <Messageformz/>
              </Modal>
              <Modal id="forward_message_modal" title="Forward Message" results={this.state.results} department={this.state.department}  user={this.state.user}>
                    <Forwardformz/>
              </Modal>
              <div>
                <NotificationContainer/>
              </div>
          </div>
        );
    }
});