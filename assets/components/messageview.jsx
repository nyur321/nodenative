const React = require('react');
const createReactClass = require('create-react-class');
const {NotificationContainer, NotificationManager} = require('react-notifications');
const axios = require('axios');
const Moment = require('moment');
const bootbox = require('bootbox');
module.exports = createReactClass({
	displayName: 'Message View',
	getInitialState: function(data) {
      var results = this.props.results;
      /*var department = this.props.department;*/
      var person = this.props.person;
      var index = this.props.index.data;
      var user = this.props.user;
      return {
            title:'',
            user:[],
            forward_details:[],
            reply:[],
            id: results.id,
            message: results.message,
            first_name:  person == null ? user.email : person.first_name,
            last_name: person == null ? '' : person.last_name,
            subject:  results.subject,
            concerned_department: [],
            created_at: Moment(results.created_at).format('LLL'),
            default_message: 'Thank you for sending us your message.',
            default_department: 'OPACD',
            default_date: Moment(results.created_at).calendar(),
            ishidden: false,
            ticket:[],
            attachment:[],
            form: {
            	id:'',
              	type: 1,
              	from:index.user.id,
              	reply: '',
              	ticket_id: results.id,
                to: user.email,
                subject: results.subject
            }
        }
	},
	handleSubmit(e){
      e.preventDefault();
      var index = this.props.index.data;
      var user = this.props.user;
      var results = this.props.results;
      this.setState({ isSubmitting: true }); 
      var form_data = this.state.form; 
      if(index.user.role_id ==3){
        axios({
          method: 'post',
          url: "/api/ticket/update/" + this.state.id,
          data: {unread: 1},
        })
      }
      if(index.user.role_id !=3){
        axios({
          method: 'post',
          url: "/api/ticket/update/" + this.state.id,
          data: {unread: 2},
        })
      }


      axios({
        method: 'post',
        url: '/api/message/reply',
        data: form_data,
      }).then(function(response){
          if(response.data.validations.length > 0) {
              $(this.refs.reply_form).find('input,select,textarea').each(function (i,v) {
                 var obj = _.find(response.data.validations,function (o) {
                     return o.param == $(v).attr('name');
                 });
                 if(obj){
                     $(v).siblings('span.error').html(obj.msg);
                 }else{
                     $(v).siblings('span.error').html('');
                 }
             });
            this.setState({
                isSubmitting: false
            });
            NotificationManager.error('Please check your input fields', 'Something went wrong...');
          } else {
            var results = this.props.results;
            var index = this.props.index.data;
            this.setState({
                isSubmitting: false,
                form: {
                  id:'',
                    type: 1,
                    from:index.user.id,
                    reply: '',
                    ticket_id: results.id,
                },
            });
		        axios({
		            method: "GET",
		            url: "/api/reply/get/" + this.state.id
		        }).then(function (response) {
		            this.setState({
		              reply: response.data,
		            });
		        }.bind(this));
            $(this.refs.reply_form).find('input,select,textarea').each(function (i,v) {
              var obj = _.find(response.data.validations,function (o) {
                   return o.param == $(v).attr('name');
               });
               if(obj){
                   $(v).siblings('span.error').html(obj.msg);
               }else{
                   $(v).siblings('span.error').html('');
               }
            });
            NotificationManager.success('Successfully Submitted', 'Reply Form');
            
            var _id = index.user.role_id == 1 ? '.ongoing_tickets' : '.messages_list'; 
            var table = $(_id).DataTable();
            table.ajax.reload();
	        }
	    }.bind(this));
       if(index.user.role_id !=3){
        // console.log(this.state.form.reply)
        axios({
          method: 'post',
          url: '/api/send/mail',
          data: {
            to: user.email,
            subject: results.subject,
            reply: this.state.form.reply
          }
        })
      }
    },

    uploadFile(e){
        var results = this.props.results;
        var data = new FormData();
        data.append('file', this.fileInput.files[0]);
        axios.post('/api/uploads/' + results.id, data).then((response) => {
          axios({
            method: "GET",
            url: "/api/uploads/get/" + this.state.id
          }).then(function (response) {
              this.setState({
                attachment: response.data
              });
          }.bind(this));
          NotificationManager.success('Successfully Uploaded');
        }); 
        console.log(data)
         
    },

	  componentDidMount: function () {
        var forward_details = this.props.forward_details;
        var concerned_department = this.props.approved_tickets;
        var results = this.props.results
        this.setState({
          forward_details: forward_details,
          concerned_department: concerned_department,
          ishidden: results.status_id == 4 ? true : false || results.status_id == 5 ? true : false 
        })
        axios({
            method: 'post',
            url: '/api/ticket/statuses',
        }).then(function (response) {
            this.setState({
              ticket: response.data
            });
        }.bind(this));
        axios({
            method: "GET",
            url: "/api/reply/get/" + this.state.id
        }).then(function (response) {
            this.setState({
              reply: response.data
            });
        }.bind(this));
       /* axios({
            method: "GET",
            url: "/api/uploads/get/" + this.state.id
        }).then(function (response) {
            this.setState({
              attachment: response.data
            });
        }.bind(this));*/
      },
    handleInput: function(e) {
      var input = {};
        if (e.target.nodeName == 'SELECT') {
            input[e.target.name] = e.target.value;
        } else {
            input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
        }
      this.setState({form: Object.assign(this.state.form, input)});
    },
	  render: function() {
      var forward_details = this.state.forward_details.length == 0 ? true : false;
      var index = this.props.index.user;
      var results = this.props.results;
      var reply = this.state.reply;
      var ticket = this.state.ticket;
      var list_department = this.props.index.data.forms.departments;
      var concerned_dept = this.state.concerned_department.map(function(dept){
        return dept.department.abbreviation.toString()
      });
      var Approved = function() {
        var dept = list_department.map(function(dept, i) {
          var text = dept.label;
          var value = dept.value;
          return ({text,value})
        });
        bootbox.prompt({
          title: "Please choose concerned department/s",
          inputType: 'checkbox',
          size: 'small',
          inputOptions: dept,
          callback: function (result_id) {
            var department_id = result_id.forEach(function(haha) {
              if(haha){
                axios({
                  method: 'post',
                  url: '/api/ticket/approve',
                  data: {
                    ticket_id: results.id,
                    department_id: haha,
                    approved_by: index.id
                  },
                })
                axios({
                  method: "post",
                  url: "/api/ticket/update/" + results.id,
                  data: {
                    status_id: 1
                  }
                }).then(function (response) {
                    $('#view_message_modal').modal('toggle');
                    var table = $('.pending_tickets').DataTable();
                    table.ajax.reload();
                })
              }
            });
          }
      });
    }
    var Revoked = function() {
      bootbox.confirm({
        message: "Revoke this ticket message?",
        size: 'small',
        buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
        },
        callback: function (result) {
          if (result == true) {
            axios({
            method: "post",
            url: "/api/ticket/update/" + results.id,
            data: {status_id: 5}
            }).then(function (response) {
                NotificationManager.success('Ticket message is successfully revoked');
                $('#view_message_modal').modal('toggle');
                var table = $('.pending_tickets').DataTable();
                table.ajax.reload();
            })
          }
        }
      }); 
    }
    var Closed = function() {
      bootbox.confirm({
        message: "Set this to Closed?",
        size: 'small',
        buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
        },
        callback: function (result) {
          if (result == true) {
            axios({
            method: "post",
            url: "/api/ticket/update/" + results.id,
            data: {status_id: 4}
            }).then(function (response) {
                NotificationManager.success('Successfully set to Closed');
                $('#view_message_modal').modal('toggle');
                if(response.data.anonymous == 0){
                  var table = $('.ongoing_tickets').DataTable();
                  table.ajax.reload();
                } else {
                  var table = $('.pending_tickets').DataTable();
                  table.ajax.reload();
                }
            })
          }
        }
      }); 
    }
    var DefaultMessage = function(props) {
      if(index.role_id == 3) {
        return (
          <div className="direct-chat-msg right">
            <div className="direct-chat-info clearfix">
              <span className="direct-chat-name pull-right">{props.from}</span>
              <span className="direct-chat-timestampt pull-left">{props.date}</span>
            </div>
            <div className="direct-chat-text">
              {props.message}
            </div>
          </div>
        )
      } else {
        return null;
      }
    }
    var Reply = function(props) {
        var direct_chat_msg = 'direct-chat-msg';
        var direct_chat_name = 'direct-chat-name';
        var direct_chat_timestamp = 'direct-chat-timestamp';
        return (
            <div className={props.id == index.id ? 'direct-chat-msg right':'direct-chat-msg'}>
              <div className="direct-chat-info clearfix">
                <span className={props.id == index.id ? 'direct-chat-name pull-right':'direct-chat-name pull-left'}>{props.fname} {props.lname}</span>
                <br/>
                <span className={props.id == index.id ? 'direct-chat-name pull-right text-muted':'direct-chat-name pull-left text-muted'}>{props.sender}</span>
                <span className={props.id == index.id ? 'direct-chat-timestampt pull-left':'direct-chat-timestampt pull-right'}>{props.sent}</span>
              </div>
              <div className="direct-chat-text">
                {props.reply}
              </div>
            </div>
        )
    }
    var Attachment = function(props) {
    var direct_chat_msg = 'direct-chat-msg';
    var direct_chat_name = 'direct-chat-name';
    var direct_chat_timestamp = 'direct-chat-timestamp';
        return (
            <div className={props.id == index.id ? 'direct-chat-msg right':'direct-chat-msg'}>
              <div className="direct-chat-info clearfix">
                <span className={props.id == index.id ? 'direct-chat-name pull-right':'direct-chat-name pull-left'}>{props.fname} {props.lname}</span>
                <br/>
                <span className={props.id == index.id ? 'direct-chat-name pull-right text-muted':'direct-chat-name pull-left text-muted'}>{props.sender}</span>
                <span className={props.id == index.id ? 'direct-chat-timestampt pull-left':'direct-chat-timestampt pull-right'}>{props.sent}</span>
              </div>
              <div className="direct-chat-text">
                {props.details}
              </div>
            </div>
        )
    }
    var ForwardDetails = function(props) {
      if(index.role_id != 3) {
        return (
          <div>
              <ul className="todo-list">
                <li>
                  <p><strong>Forwarded to:</strong> {props.department} on {props.date}</p>
                  <p hidden={props.comment == '' ? true : false} ><strong>Comment:</strong> &nbsp;{props.comment}</p>
                </li>
              </ul>
          </div>
        )
      } else {
        return (
          <div>
            <div className="form-group">
                <p><strong>Forwarded to:</strong> {props.department} on {props.date}</p>
            </div>
          </div>
        )
      }
    }
    var SetStatus = function() {
      if(index.role_id == 1) {
        if(results.status_id == 3 && results.anonymous == 0) {
          return (
            <div className="box-body">
              <button type="submit" className="btn btn-success btn-xs" onClick={Approved}><i className="fa fa-thumbs-o-up"></i> Approve</button> &nbsp;
              <button className="btn btn-warning  btn-xs" onClick={Revoked}><i className="fa fa-thumbs-o-down"></i> Revoke</button>
              <span className="text-muted pull-right">{index.role_id == 3 ? (reply.length)+1 : reply.length} Reply</span>
            </div>
          )
        } else if(results.status_id == 1) {
          return(
            <div className="box-body">
              <button className="btn btn-flat btn-success btn-xs" onClick={Closed}><i className="fa fa-times-circle"></i> Set to Close</button>
              <span className="text-muted pull-right">{index.role_id == 3 ? (reply.length)+1 : reply.length} Reply</span>
            </div>
          )
        } else if(results.status_id == 3 && results.anonymous == 1) {
           return (
            <div className="box-body">
              <button type="submit" className="btn btn-flat btn-success btn-xs" onClick={Closed}><i className="fa fa-times-circle"></i> Set to Close</button> &nbsp;
              <button className="btn btn-flat btn-warning btn-xs" onClick={Revoked}><i className="fa fa-thumbs-o-down"></i> Revoke</button>
              <span className="text-muted pull-right">{index.role_id == 3 ? (reply.length)+1 : reply.length} Reply</span>
            </div>
          )
        } else {
          return null
        }
      } else {
        return null;
      }
    }
		return (
			  <div>
            <div className="box-body">
      					<div className="bg-black">
      					    <p className="text-center">Message Details</p>
      					</div>
                <p><strong>From:</strong> {this.state.first_name} {this.state.last_name}</p>
                <p><strong>Date Sent:</strong> {this.state.created_at}</p>
                <p hidden={concerned_dept.length > 0 ? false : true}><strong>Concerned Department/Office:</strong> {concerned_dept.join(", ")}</p> 
                <p><strong>Subject:</strong> {this.state.subject}</p>
                <p><strong>Message:</strong> {this.state.message}</p>
                <div className="bg-green" hidden={forward_details}>
                    <p className="text-center">Forward Details</p>
                </div>
                {this.state.forward_details.map(function(forward,j) {
                  return <ForwardDetails department={forward.department.name} first_name ={forward.user.person.first_name} last_name ={forward.user.person.last_name} key={j} comment={forward.comments} date={Moment(forward.created_at).format('LLL')}/>
                })}   
            </div>
            <SetStatus />
            <div className="box-footer">
                <div className="direct-chat direct-chat-primary">
                  <div className="box-body">
                    <div className="direct-chat-messages">
                      <DefaultMessage message={this.state.default_message} from={this.state.default_department} date={this.state.default_date}/>
    	                {this.state.reply.map(function(reply,i) {
    	                	return <Reply reply={reply.message} key={i} id={reply.from} fname={reply.user.person.first_name} lname={reply.user.person.last_name} sender={reply.user.department == null ? reply.user.email : reply.user.department.name} sent={Moment(reply.created_at).calendar()}/>
    	                })}
                     {/* {this.state.attachment.map(function(attachment,i) {
                        return <Attachment details={attachment.details} key={i} sent={Moment(reply.created_at).calendar()}/>
                      })}*/}
                    </div>
                  </div>
                </div>
                <div className="row" hidden={this.state.ishidden}>
                	<form onSubmit={this.handleSubmit} ref="reply_form">
                      <div className="form-group margin-bottom-one">
                        <div className="col-sm-9">
                          <textarea className="form-control" rows="3" name="reply" placeholder="Your reply here ..." value={this.state.form.reply} onChange={this.handleInput}/>
                          <span className="error"></span>
                        </div>
                        <div className="col-sm-3">
                          <button type="submit"  className="form-control btn-flat btn btn-primary btn-block" disabled={this.state.isSubmitting}>Send</button>
                         {/* <div className="file-upload">
                              <label className="hover" htmlFor="file-input"><p><i className="fa fa-upload bg-green"></i> Click here to upload!</p></label> 
                              <input id="file-input" type="file" ref={input => {this.fileInput = input}} onChange={this.uploadFile} data-id={this.state.id}/>
                              <p>Upload File(Max file size is 5mb)</p>
                          </div>*/}
                        </div>
                      </div>
  	              </form>
                </div>
            </div>
        </div>
		);
	}
});
