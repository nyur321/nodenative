const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');

module.exports = createReactClass({
    displayName: 'Client',
    getInitialState: function () {
        return {
            type: 'create',
            title: '',
            modal: false,
            departments_options: [],
            roles_options: [],
            feedbacktypes: [],
            action: "",
            method: "POST",
            notify_message: '',
            notify_class: 'hidden callout',
            notify_show: false,
            form: {
                id: '',
                email: '',
                departments: 0,
                roles: 2,
                first_name: '',
                middle_name: '',
                last_name: '',
                password: '',
                confirm_password: '',
            },


        }
    },
    componentWillUnmount: function () {

    },
    handleSubmit: function (e) {
        e.preventDefault();
        var form_data = this.state.form;
       // console.log(form_data)
        // var form = $(this.refs.create_user_form).attr('action')
       // console.log(this.state.action);
        //console.log(this.state.type);
        this.setState({isSubmitting: true})

        axios({
            method: this.state.method,
            url: this.state.action,
            data: form_data,
        }).then(function (response) {
            console.log(response);
            $(this.refs.user_form).find('input,select,textarea').each(function (i, v) {
                $(v).siblings('span.error').html('');
            });
            // var notify_options = {
            //     type: 'success',
            //     message: '',
            // };
            if (response.data.validations) {
                if (response.data.validations.length > 0) {
                    $(this.refs.user_form).find('input,select,textarea').each(function (i, v) {
                        var obj = _.find(response.data.validations, function (o) {
                            return o.param == $(v).attr('name');
                        });
                        if (obj) {
                            $(v).siblings('span.error').html(obj.msg);
                        } else {
                            $(v).siblings('span.error').html('');
                        }
                    });
                    NotificationManager.error('Please check your input fields', 'Something went wrong...');
                }
            }

            if (response.data.success) {
                if(this.state.type == 'create'){
                    // notify_options['message'] = 'account for "' + response.data.user.email + '" created';
                    this.setState({
                        form: Object.assign(this.state.form, {
                            id: '',
                            email: '',
                            departments: 0,
                            roles: 0,
                            first_name: '',
                            middle_name: '',
                            last_name: '',
                            password: '',
                            confirm_password: '',
                        })
                    })
                    NotificationManager.success(response.data.user.email, 'Successfully created'); 
                }
                if(this.state.type == 'update'){
                    NotificationManager.info(response.data.user.email, 'Successfully updated');
                    // this.setState({
                    //     form: Object.assign(this.state.form, {
                    //         email: '',
                    //         departments: 0,
                    //         roles: 2,
                    //         first_name: '',
                    //         middle_name: '',
                    //         last_name: '',
                    //         password: '',
                    //         confirm_password: '',
                    //     })
                    // })
                }


            }
            // if (response.data.error) {
            //     notify_options['type'] = 'danger';
            //     notify_options['message'] = 'An error occured "' + response.data.error.code + '"';
            // }
            // this.notify(notify_options);
        }.bind(this));

    },
    notify: function (options) {
        var notifyClass = 'callout callout-' + options.type;
        this.setState({notify_message: options.message});
        this.setState({notify_class: notifyClass});
    },
    handleInput: function (e) {
        var input = {};
        if (e.target.nodeName == 'SELECT') {
            input[e.target.name] = e.target.value;
        } else {
            input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
        }

        if (e.target.name == 'roles') {
            if (e.target.value == '3' || e.target.value == '4') {
                this.setState({form: Object.assign(this.state.form, {departments: 0})});
            }
        }

        this.setState({form: Object.assign(this.state.form, input)});
    },
    componentDidMount: function () {
        $(this.refs.user_form).find('.notify-modal').hide();
        var index = this.props.index;
        var data = this.props.data;
        var action = null;
        var type = this.props.type || 'create';
        var user_id = this.props.user_id;
        var input = {
            departments: index.data.forms.departments[0].value.toString(),
            roles: index.data.forms.roles[0].value.toString(),
        };


        this.setState({method: this.props.method});
        this.setState({form: Object.assign(this.state.form, input)});
        this.setState({departments_options: index.data.forms.departments});
        this.setState({roles_options: index.data.forms.roles});
        this.setState({feedbacktypes: index.data.forms.feedbacktypes});

        if (data) {
            this.setState({form: Object.assign(this.state.form, data)});
        }

        switch (type) {
            case 'create':
                action = '/api/user/create';
                break;
            case 'update':
                action = '/api/user/update';
                break;
        }


        this.setState({type: type});
        this.setState({action: action});


    },
    render: function () {

        return (
            <form action={this.state.action} onSubmit={this.handleSubmit}  ref="user_form">
                <div className="row">
                    <div className="col-sm-12">
                        <div className={this.state.notify_class}>
                            {this.state.notify_message}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label>Email</label>
                            <input className="form-control" value={this.state.form.email}
                                   onChange={this.handleInput} name="email" type="text"/>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label>First name</label>
                            <input className="form-control" value={this.state.form.first_name}
                                   onChange={this.handleInput} name="first_name" type="text"/>
                            <span className="error"></span>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label>Middle name</label>
                            <input className="form-control" value={this.state.form.middle_name}
                                   onChange={this.handleInput} name="middle_name" type="text"/>
                            <span className="error"></span>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label>Last name</label>
                            <input className="form-control" value={this.state.form.last_name}
                                   onChange={this.handleInput} name="last_name" type="text"/>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label>Choose Department</label>
                             <select className="form-control" value={this.state.form.departments} name="departments"
                                    onChange={this.handleInput}
                                    ref="departments_select">
                                <option value="0">N/A</option>
                                {this.state.departments_options.map(function (v, j) {
                                    return <option key={j} value={v.value}>{v.label}</option>
                                })}
                            </select>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label>Choose Role</label>
                            <select className="form-control" value={this.state.form.roles} name="roles"
                                    onChange={this.handleInput}
                                    ref="roles_select">
                                {this.state.roles_options.map(function (v, i) {
                                    return <option key={i} value={v.value}>{v.label}</option>
                                })}
                            </select>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label>Password</label>
                            <input id="password" type="password" name="password"
                                   value={this.state.form.password} onChange={this.handleInput}
                                   className="form-control"/>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label>Confirm Password</label>
                            <input id="confirm_password" type="password" name="confirm_password"
                                   value={this.state.form.confirm_password} onChange={this.handleInput}
                                   className="form-control"/>
                            <span className="error"></span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <input type="submit" value={this.state.type.toUpperCase()} id="create-user-btn"
                               className="btn btn-flat btn-primary pull-right"/>
                    </div>
                </div>
            </form>
        );
    }
})
;