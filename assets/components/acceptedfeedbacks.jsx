const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
const Moment = require('moment');
$.DataTables = require('datatables.net');
var Modal = require('../components/modal.jsx');
var Feedbackview = require('../components/feedbackview.jsx');
module.exports = createReactClass({
      displayName: 'Accepted Feedbacks',
       getInitialState: function () {
        return {
            results: [],
            department: [],
            feedbacktype: [],
            index:{},
            data:{},
        }
    },
    componentWillUnmount: function () {

    },
    componentDidMount: function () {
        $(this.refs.accepted_feedbacks).DataTable({
            "processing": true,
            "ordering" : true,
            "retrieve": true,
            "paging": true,
            "ajax": {
                "url": "/api/feedback/accepted",
                "type": "POST"
            },
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                  "data": "feedbacktype",
                  "render": function (data, type, full, meta) {
                      return data.type;
                  }
                },
                {
                  "data": "email"
                },
                {
                  "data": "created_at",
                  "render": function (data, type, full, meta) {
                      return Moment(data).format('lll');
                  }
                },
                {
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<a data-feedback-id="' + data.id + '" class="btn btn-xs btn-flat btn-success open-feedback-modal">open</a>';
                    }
                }
            ]
        });
        $(this.refs.accepted_feedbacks).on('click','.open-feedback-modal',function (e) {
            e.preventDefault();
            var feedback_id = $(e.target).attr('data-feedback-id');

            $('#view_feedback_modal').modal('show');
            axios({
                method: "GET",
                url: "/api/feedback/get/"+feedback_id
            }).then(function (response) {
                this.setState({
                  results: response.data,
                  department: response.data.department,
                  feedbacktype: response.data.feedbacktype
                });
            }.bind(this));
        }.bind(this));
    },
      render: function() {
         var Feedbackformz = function () {
            return <Feedbackview index={this.props.index} results={this.state.results} department={this.state.department}  feedbacktype={this.state.feedbacktype}/>
        }.bind(this);
        return (
          <div className="box box-info">
            <div className="box-header with-border">
                <h3 className="box-title"><i className="fa fa-inbox"></i> Accepted Feedbacks</h3>
            </div>
            <div className="box-body">
                <table className="table table-striped accepted_feedbacks" ref="accepted_feedbacks" width="100%">
                    <thead>
                      <tr>
                          <th>No.</th>
                          <th>Feedback Type</th>
                          <th>Email</th>
                          <th>Date Sent</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>No.</th>
                          <th>Feedback Type</th>
                          <th>Email</th>
                          <th>Date Sent</th>
                          <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
              <Modal id="view_feedback_modal" title="View Feedback" results={this.state.results} department={this.state.department}  feedbacktype={this.state.feedbacktype}>
                    <Feedbackformz />
              </Modal>
          </div>
        );
    }
});