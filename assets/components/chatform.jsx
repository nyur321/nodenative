const React = require('react');
import {Redirect} from 'react-router';
const createReactClass = require('create-react-class');
const axios = require('axios');
var SelectionBox = require('./selectionbox.jsx');
var ChatBox = require('./chatbox.jsx');
var _ = require('lodash');
const io = require('socket.io-client');
module.exports = createReactClass({
    displayName: 'chat_form',
    getInitialState: function (data) {
        var index = this.props.index;
        return {
            departments: index.data.forms.departments,
            department_names:'',
            form: {
                subject: '',
                departments: [],
            },
            chat_overlay:null,
            user: {},
            room: null,
            ready:false,
            loading:true,
            open_chat: false,
            current:{
                id:0,
                name: '',
                subject: '',
                departments: [],
                messages:[{
                    id:0,
                    message: 'An officer will be with you for a moment',
                    type:'message',
                    date: '',
                    from: {
                        name: 'System',
                        avatar: ''
                    },
                    time: '4:00'
                }]
            }
        }
    },
    submitChat: function (e) {
        e.preventDefault();
        axios({
            method: 'post',
            url: '/api/user/chat/submit',
            data: {
                subject: this.state.form.subject,
                departments: this.state.departments
            }
        }).then(function (response) {
            var chat = response.data.chat;
            console.log(response.data);
            if(typeof response.data == 'object'){
                var department_names = chat.departments.map(function (department) {
                    return department.label;
                }.bind(this));
                this.setState({department_names:department_names.join(',')});
            }

            this.setState({ready:true});


        }.bind(this));


    },
    componentDidMount: function () {
        this.setState({departments: Object.assign(this.state.departments, this.props.index.data.forms.departments)});
        var socket = io.connect('/realtime/chat');

        axios({
            url: '/api/chat-session',
            method: 'post'
        }).then(function (response) {
            this.setState({loading:false});
            var form = {subject:response.data.subject,departments:response.data.departments.map(function(department_id){
                return parseInt(department_id);
            })};

            switch(response.data.status){
                case 1:
                    this.setState({ready: true});
                    this.setState({form: Object.assign(this.state.form, form)});
                    break;
                case 2:

                    break;
                case 3:
                    this.setState({ready: true});
                    this.setState({open_chat: true});
                    break;
                default:

                    break;
            }

            socket.emit('start_queue',{

            },function () {

            });

            socket.on('message',function (res) {
                // console.log(res.data.messages);

            });


        }.bind(this));
    },
    handleInput: function (e) {
        var input = {};
        if (e.target.nodeName == 'SELECT') {
            input[e.target.name] = e.target.value;
        } else {
            input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
        }
        this.setState({form: Object.assign(this.state.form, input)});


    },
    handleChange: function (e, data, key) {
        // console.log(e);

        // console.log(data);
        // console.log(key);
    },
    startChat: function (e) {

        this.setState({open_chat: true});
    },
    popoutClosed: function (e) {
        console.log('Chat window closed');
    },
    render: function () {
        var departments = this.state.departments;
        var redirect = this.state.redirect;
        var index = this.props.index;

        if (!this.state.loading) {
            return (
                <div className="row">
                    <div className="col-sm-12">
                        <div className="box box-primary">
                            <div className="box-header">
                                <h3>Chat Request</h3>
                            </div>
                            <div className="box-body">
                                {!this.state.ready && <form onSubmit={this.submitChat}>
                                    <div className="form-group">
                                        <label>Subject</label>
                                        <input type="text" className="form-control" onChange={this.handleInput}
                                               name="subject"/>
                                    </div>
                                    <SelectionBox name="departments" onSelect={this.handleChange} data={departments}/>
                                    <div className="form-group">
                                        <input type="submit" className="btn btn-primary btn-flat"
                                               value="Submit Inquiry"/>
                                    </div>
                                </form>}

                                {this.state.ready &&
                                <p>Chat is ready. Reference no. {this.state.room}<br/> Subject: {this.state.form.subject}<br/>
                                    Departments: {this.state.form.departments.map(function (department) {
                                        if(typeof department == 'object'){
                                            return department.label;
                                        }else{
                                            console.log(_.find(index.departments,function (dept) {
                                                return dept.value == department;
                                            }));
                                        }

                                    }.bind(this))} <br/>
                                </p>}
                                {((!this.state.open_chat) && this.state.ready) && (
                                    <button className="btn btn-flat btn-primary" onClick={this.startChat}>Start chatting</button>
                                )}

                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        {this.state.open_chat && (
                            <ChatBox index={this.props.index}
                                     room={this.state.room}
                                     onUpdate={this.updateMessage}
                                     chat_show_overlay={this.state.chat_overlay}
                                     messages={this.state.current.messages}/>
                        )}
                    </div>
                </div>
            );

        }


        if (this.state.loading) {
            return (
                <div className="box box-primary">
                    <div className="text-center">
                        <h4>CHAT REQUEST</h4>
                    </div>
                    <div className="box-body">
                        Loading ...
                    </div>
                </div>
            );
        }

    }
});
