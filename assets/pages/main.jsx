const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
const Moment = require('moment');
$.DataTables = require('datatables.net');
const {NotificationContainer, NotificationManager} = require('react-notifications');
var Feedbackform = require('../components/feedbackform.jsx');
var Contactform = require('../components/contactform.jsx');
var Modal = require('../components/modal.jsx');
var Messageview = require('../components/messageview.jsx');

module.exports = createReactClass({
    displayName: 'Mainbox',
    getInitialState: function () {
        return null
    },
    componentWillUnmount: function () {
        
    },
    componentDidMount: function () {

    },
    render: function() {
        return (
                    <section className="content">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="box box-solid">
                                  <div className="box-header with-border">
                                    <h3 className="box-title">Ticket Lists</h3>
                                    <div className="box-tools">
                                      <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                                      </button>
                                    </div>
                                  </div>
                                  <div className="box-body no-padding">
                                    <ul className="nav nav-pills nav-stacked">
                                      <li><Link to="/client/messages"><i className="fa fa-envelope-o"></i>Message List</Link></li>
                                      <li><Link to="/client/feedbacks"><i className="fa fa-file-text-o"></i> Feedback List</Link></li>
                                      {/*<li><Link to=""><i className="fa fa-comments-o"></i> Chat Request List</Link></li>*/}
                                    </ul>
                                  </div>
                                </div>
                                <div className="box box-solid">
                                  <div className="box-header with-border">
                                      <h3 className="box-title">New Transaction</h3>
                                      <div className="box-tools">
                                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                                        </button>
                                      </div>
                                  </div>
                                  <div className="box-body no-padding">
                                    <ul className="nav nav-pills nav-stacked">
                                      <li><Link to="/client/contact"><i className="fa fa-plus-square-o text-yellow"></i> Contact Us</Link></li>
                                      <li><Link to="/client/feedback"><i className="fa fa-plus-square-o text-green"></i> New Feedback</Link></li>
                                      {/*<li><Link to="/client/chat"><i className="fa fa-plus-square-o text-light-blue"></i>New Chat Request</Link></li>*/}
                                    </ul>
                                  </div>
                                </div>
                            </div>
                            <div className="col-md-9">
                              {this.props.children}
                            </div> 
                        </div>
                        <div>
                          <NotificationContainer/>
                        </div>
                    </section>
                    
            );
       }
});
                                       