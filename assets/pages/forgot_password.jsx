const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const Link = require('react-router').Link;
const {NotificationContainer, NotificationManager} = require('react-notifications');
var IndexSide = require('../components/indexSide.jsx');
var Feedbackform = require('../components/feedbackform.jsx');
module.exports = createReactClass({
    displayName: 'Index',
    getInitialState: function(data) {
        return {
            email: this.props.email || '',
        }
    },

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    },
    componentDidMount() {
       
    },
    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            isSubmitting: true
        })
        axios({
            method: 'POST',
            url: '/api/user/forgot',
            data: {email: this.state.email}
        }).then(function(response){
            console.log(response)
            if(response.data == "success"){
                axios({
                  method: 'POST',
                  url: '/api/send/token',
                  data: {email: this.state.email}
                }).then(function(result){
                    NotificationManager.success('Password Reset Link is sucessfully sent to your email address');
                    this.setState({isSubmitting: false})
                }.bind(this))
            }
            if(response.data == "err"){
                NotificationManager.warning('Email not found', 'Warning...');
                this.setState({
                    isSubmitting: false
                })
            }
            if(response.data.validations.length > 0) {
                $(this.refs.login_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                this.setState({isSubmitting:false})
                NotificationManager.error('Please check your input fields', 'Something went wrong...');
            } else {
                null
            }
        }.bind(this));
    },
    render: function() {
        return (
            <div className="login-box">
                <div className="login-logo">
                    <Link to="/"><i className="fa fa-address-book-o"></i>&nbsp;<b>OPACD</b></Link>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Forgot password</p>
                    <form onSubmit={this.handleSubmit} ref="login_form">
                        <div className="form-group has-feedback">
                            <input name="email" className="form-control" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange} />
                            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <button type="submit" className="btn btn-primary btn-block btn-flat" disabled={this.state.isSubmitting}>Reset password</button>
                            </div>
                        </div>
                    </form>
                    <Link to ="/login">Back to login</Link><br/>
                </div>
                <div>
                    <NotificationContainer/>
                </div>
            </div>
        );
    }
});