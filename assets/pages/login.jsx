const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const Link = require('react-router').Link;
var ReactRouter = require('react-router');
var browserHistory = ReactRouter.browserHistory;
const {NotificationContainer, NotificationManager} = require('react-notifications');
module.exports = createReactClass({
    displayName: 'Login',
    getInitialState: function(data) {
        return {
            email: this.props.email || '',
            password: this.props.password || '',
            loading: true,
        }
    },

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    },
    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    },
    componentDidMount() {
        this.setState({ loading: false }); 
        /*axios({
            method: 'get',
            url:'/api/'
        })*/
    },
    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            isSubmitting: true
        })
        var data = {
            email: this.state.email,
            password: this.state.password
        }
        axios({
            method: 'post',
            url: '/api/user/login',
            data: data,
        }).then(function (response) {
            if(response.data.validations.length > 0) {
                $(this.refs.login_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                this.setState({
                    isSubmitting: false
                })
                NotificationManager.error('Please check your input fields', 'Something went wrong...');
            } else {
                if(response.data.user) {
                    var user = response.data.user;
                    if(user.role_id == 3 && user.validated == 1 ) {
                        window.location.href='/client';
                    } else if(user.role_id == 1 && user.validated == 1) {
                        window.location.href='/dashboard';
                    } else if(user.role_id == 2 && user.validated == 1) {
                        window.location.href='/dashboard/messages';
                    } else {
                       browserHistory.push('/verify')
                    }
                } else {
                    this.setState({
                        isSubmitting: false
                    })
                    NotificationManager.warning('Please check your credentials', 'Something went wrong...');
                }
            }
        }.bind(this));
    },

    render: function() {
        const { loading } = this.state;
        if(loading) {
            return (
                <div className="loader"></div>
            )
        }
        return (
            <div className="login-box">
                <div className="login-logo">
                    <Link to="/"><i className="fa fa-address-book-o"></i>&nbsp;<b>OPACD</b></Link>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Sign in to start your session</p>
                    <form onSubmit={this.handleSubmit} ref="login_form">
                        <div className="form-group has-feedback">
                            <input name="email" className="form-control" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange} />
                            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password" name="password" className="form-control" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange} />
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <button type="submit" className="btn btn-primary btn-block btn-flat" disabled={this.state.isSubmitting}>Sign In</button>
                            </div>
                        </div>
                    </form>
                    <Link to ="/forgot">I forgot my password</Link><br/>
                    <Link to ="/register" className="text-center">Register a new account</Link>
                </div>
                <div>
                    <NotificationContainer/>
                </div>
            </div>
        );
    }
});
