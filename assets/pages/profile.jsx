const React = require('react');
const createReactClass = require('create-react-class');
var IndexSide = require('../components/indexSide.jsx');
module.exports = createReactClass({
    displayName: 'Profile',
    getInitialState: function (data) {
        var index = this.props.index;
        return {
            redirect: false,
            departments: index.data.forms.departments,
            form: {
                email: index.user.email || '',
                first_name: index.user.name.first_name || '',
                middle_name: index.user.name.middle_name || '',
                last_name: index.user.name.last_name || '',
                address: '',
                office_agency: '',
            },
            room: null
        }
    },
    isAdmin: function () {
        let index = this.props.index;
        return (index.authenticated && index.user.role_id != 3 ? true : false);
    },
    isAuthenticated: function () {
        let index = this.props.index;
        return (index.authenticated);
    },
    componentDidMount: function () {

    },
    handleInput: function (e) {
        var input = {};
        if (e.target.nodeName == 'SELECT') {
            input[e.target.name] = e.target.value;
        } else {
            input[e.target.name] = e.target.value == "" ? e.target.selected : e.target.value;
        }
        this.setState({form: Object.assign(this.state.form, input)});
    },
    render: function () {
        var index = this.props.index;
        return (
            <div className="container">
                <section className="content">
                    <div className="col-sm-8">
                        <div className="nav-tabs-custom">
                            <ul className="nav nav-tabs">
                                <li className="active"><a href="#profile-details" data-toggle="tab">Details</a></li>
                                <li><a href="#profile-update" data-toggle="tab">Update</a></li>
                            </ul>
                            <div className="tab-content">
                                <div className="active tab-pane" id="profile-details">
                                    <form className="form-horizontal">
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Name</label>
                                            <div className="col-sm-9">
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control form-read-only" readOnly="readOnly" id="first_name"
                                                               onChange={this.handlerInput} placeholder=""
                                                               value={this.state.form.first_name}/>
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control form-read-only" readOnly="readOnly" id="middle_name"
                                                               onChange={this.handlerInput} placeholder=""
                                                               value={this.state.form.middle_name}/>
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control form-read-only" readOnly="readOnly" id="last_name"
                                                               onChange={this.handlerInput} placeholder=""
                                                               value={this.state.form.last_name}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Email</label>
                                            <div className="col-sm-9">
                                                <input type="email" readOnly="readOnly" className="form-control form-read-only" id="email"
                                                       onChange={this.handlerInput} placeholder=""
                                                       value={this.state.form.email}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Address</label>
                                            <div className="col-sm-9">
                                                <input type="text" className="form-control form-read-only" readOnly="readOnly" id="inputExperience"
                                                          placeholder="N/A" onChange={this.handlerInput} value={this.state.form.address}></input>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Office/Agency</label>
                                            <div className="col-sm-9">
                                                <input type="text" className="form-control form-read-only" readOnly="readOnly" id="office_agency"
                                                          placeholder="N/A" onChange={this.handlerInput} value={this.state.form.office_agency}></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane" id="profile-update">
                                    <form className="form-horizontal">
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Name</label>

                                            <div className="col-sm-9">
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control" name="first_name" id="first_name"
                                                               onChange={this.handleInput}  placeholder="First Name"
                                                               value={this.state.form.first_name}/>
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control" name="middle_name" id="middle_name"
                                                               onChange={this.handleInput}  placeholder="Middle Name"
                                                               value={this.state.form.middle_name}/>
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <input type="text" className="form-control" name="last_name" id="last_name"
                                                               onChange={this.handleInput}  placeholder="Last name"
                                                               value={this.state.form.last_name}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Email</label>

                                            <div className="col-sm-9">
                                                <input type="email" className="form-control" name="email" id="email"
                                                       onChange={this.handleInput}  placeholder="Email"
                                                       value={this.state.form.email}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Address</label>
                                            <div className="col-sm-9">
                                                <input type="text" className="form-control" id="address" name="address"
                                                       onChange={this.handleInput}  placeholder="Type your Barangay or City(Syudad)"
                                                       value={this.state.form.address}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-sm-3 control-label">Office/Agency</label>
                                            <div className="col-sm-9">
                                                <input type="text" className="form-control" id="office_agency" name="office_agency"
                                                       onChange={this.handleInput}  placeholder="Type your Office/Agency"
                                                       value={this.state.form.office_agency}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="col-sm-offset-2 col-sm-10">
                                                <button type="submit" className="btn btn-flat btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <IndexSide isAdmin={this.isAdmin()} isAuthenticated={this.isAuthenticated()}/>
                </section>
            </div>
        );
    }
});
