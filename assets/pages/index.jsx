const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const Link = require('react-router').Link;
var IndexSide = require('../components/indexSide.jsx');
var Feedbackform = require('../components/feedbackform.jsx');

module.exports = createReactClass({
    displayName: 'Index',
    isAdmin: function () {
        let index = this.props.index;
        return (index.authenticated && index.user.role_id != 3 ? true : false);
    },
    isAuthenticated: function () {
        let index = this.props.index;
        return(index.authenticated);
    },
    render: function() {
        return (
                <div className ="container">
                    <section className="content">
                      <div className={(this.isAdmin() ? 'col-md-12' : 'col-md-8')}>  
                          {/*<Feedbackform index={this.props.index} />*/}
                          <div className="content-wrapper">
                            <section className="content-header">
                              <h1 className="text-blue">
                                Welcome to the Online Public Assistance and Complaints Desk of the City Government of Baguio.
                              </h1>
                            </section>
                            <section className="content text-muted text-left">
                                The Online Public Assistance and Complaint Desk (OPACD) is a Customer Relationship Management System
                                that allows the members of the public to submit inqueries, complaints, or suggestions through web forms.
                            </section>
                            <section className="content-header">
                              <div className="row">
                                <div className="col-md-6 col-sm-5 col-xs-12">
                                  <div className="info-box">
                                    <span className="info-box-icon bg-green"><i className="fa fa-envelope-o"></i></span>
                                    <div className="info-box-content">
                                      <Link to="/contact">
                                        <span className="info-box-text">Contact Us</span>
                                      </Link>
                                        <span className="info-box-number"><p className="text-muted">Message us and let us know your concerns</p></span>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-6 col-sm-5 col-xs-12">
                                  <div className="info-box">
                                    <span className="info-box-icon bg-aqua"><i className="fa fa-inbox"></i></span>
                                    <div className="info-box-content">
                                      <Link to="/feedback">
                                        <span className="info-box-text">Feedback</span>
                                      </Link>
                                        <span className="info-box-number"><p className="text-muted">Drop your comments and suggestions</p></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </section>
                           {/* <section className="content text-muted text-left" >
                              - Receive ticket complaints or suggestions from members of the public.
                              <br/>
                              - Assist by providing information concerning the department from our staffs.
                              <br/>
                              - Register tickets and responds by replying through the system and email.
                            </section>*/}
                        </div>
                      </div>
                      <IndexSide isAdmin={this.isAdmin()} isAuthenticated={this.isAuthenticated()} />
                  </section>
            </div>
        );
    }
});