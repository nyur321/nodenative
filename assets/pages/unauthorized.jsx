const React = require('react');
const createReactClass = require('create-react-class');
const ReactRouter = require('react-router');
const Link = ReactRouter.Link;
module.exports = createReactClass({
    displayName: 'Unauthorized',
    componentDidMount:function () {
    },
    render: function() {
        var user = this.props.user
        var Warning = function() {
            if(user.authenticated){
                if(user.role_id !=3){
                    return(
                       <div className="content-wrapper">
                            <section className="content-header">
                              <h1>
                                403 Forbidden Page
                              </h1>
                            </section>
                            <section className="content">
                              <div className="error-page">
                                <h2 className="headline text-red">403</h2>
                                <div className="error-content">
                                  <h3><i className="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
                                  <p>
                                    Sorry, Your account don't need to access this resource.  <Link to="/dashboard">Go back to your page</Link>
                                  </p>
                                </div>
                              </div>
                            </section>
                        </div>
                    )
                } else if(user.role_id == 3){
                    return(
                       <div className="content-wrapper">
                            <section className="content-header">
                              <h1>
                                401 Unauthorized Page
                              </h1>
                            </section>
                            <section className="content">
                              <div className="error-page">
                                <h2 className="headline text-red">401</h2>
                                <div className="error-content">
                                  <h3><i className="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
                                  <p>Sorry, You don't have sufficient permission to access this page. <Link to="/client">Go back to your page</Link></p>
                                </div>
                              </div>
                            </section>
                        </div>
                    )  
                }
            } else {
                return(
                    <div className="content-wrapper">
                        <section className="content-header">
                          <h1>
                            401 Unauthorized Page
                          </h1>
                        </section>
                        <section className="content">
                          <div className="error-page">
                            <h2 className="headline text-red">401</h2>
                            <div className="error-content">
                                <h3><i className="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
                                <p>Sorry, You don't have sufficient permission to access this page. 
                                    Please <Link to="/login">Login</Link> or <Link to="/register">Register</Link>
                                </p>
                            </div>
                          </div>
                        </section>
                    </div>
                )
            }
        }
        return (
            <div>
                <Warning />
            </div>
        );
    }
});
