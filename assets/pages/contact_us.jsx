const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const {NotificationContainer, NotificationManager} = require('react-notifications');
var IndexSide = require('../components/indexSide.jsx');
var Contactform = require('../components/contactform.jsx');
module.exports = createReactClass({
    displayName: 'Contact Us',
    isAdmin: function () {
        let index = this.props.index;
        return (index.authenticated && index.user.role_id != 3 ? true : false);
    },
    isAuthenticated: function () {
        let index = this.props.index;
        return(index.authenticated);
    },
    render: function() {
          return (
                <div className ="container">
                      <section className="content">
                         <div className="col-md-8">
                            <Contactform index={this.props.index} />
                          </div>
                            <IndexSide isAdmin={this.isAdmin()} isAuthenticated={this.isAuthenticated()}/>
                      </section>
                      <div>
                          <NotificationContainer/>
                      </div>
                </div>
          );
      }
});
