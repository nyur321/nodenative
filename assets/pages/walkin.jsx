const React = require('react');
const axios = require('axios');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
//var Basic = require('react-d3-basic');
//var LineChart = require('react-d3-basic').LineChart;
//var BarChart = require('react-d3-basic').BarChart;
var ColumnGroup = createReactClass({
    displayName: 'columnGroup',
    render: function () {
        var group_offset = this.props.segment;
        var arr = this.props.data;
        var format = this.props.format;
        var newArr = [];
        arr.forEach(function (a) {
            newArr.push(a);
        });
        var colDivs = Math.ceil(12 / group_offset);
        var classNameCustom = 'col-sm-' + colDivs;
        var divArray = [];
        var divKey = group_offset - 1;
        var division = Math.ceil(arr.length / group_offset);
        while (newArr.length > 0) {
            divArray.push(newArr.splice(0, division));
        }

        return (
            <div className="row">
                {divArray.map(function (da,key) {
                    var array_args = ['div', {key:key,className: classNameCustom}];
                    da.forEach(function (dd) {
                        array_args.push(format(dd));
                    });
                    return React.createElement.apply(this, array_args);
                })}
            </div>
        );


    }
});

const CustomTooltip = createReactClass({
    render: function () {
        const {active} = this.props;
        const finalStyle = {
            margin: 0,
            padding: 10,
            backgroundColor: '#fff',
            border: '1px solid #ccc',
            whiteSpace: 'nowrap',

        };
        const itemStyle = {
            listStyle: 'none'
        };
        if (active) {
            const {payload, label} = this.props;
            var intro = '';
            var unitLabel = label;
            var data = {};
            if (payload) {
                if (payload.hasOwnProperty(0)) {
                    data = payload[0].payload;
                    intro = data[this.props.dataIntro];
                }

            }
            if (typeof this.props.unitLabel == 'function') {
                unitLabel = this.props.unitLabel(data);
            }
            return (
                <div className="custom-tooltip" style={finalStyle}>
                    <p className="intro">{`${intro}`}</p>
                    <ul style={{margin: 0, padding: 0}}>
                        <li style={itemStyle}>{unitLabel}</li>
                    </ul>
                </div>
            );
        }

        return null;
    }
});

module.exports = createReactClass({
    displayName: 'Reports',
    getInitialState: function () {
        return {
            tickets: 0,
            user_registrations: 0,
            feedbacks: 0,
            department_tickets: [],
            ishidden: false
        }
    },
    componentDidMount: function () {
        var index = this.props.index;
        this.setState({
            ishidden: index.data.user.role_id == 2 ? true : false
        });
        axios({
            method: 'post',
            url: '/api/report/charts',
        }).then(function (response) {
            var data = response.data;
            this.setState({tickets: data.tickets || 0});
            this.setState({user_registrations: data.user_registrations || 0});
            this.setState({feedbacks: data.feedbacks || 0});
            this.setState({department_tickets: data.department_tickets || []});
            // this.setState({department_messages: data.department_messages || []});

        }.bind(this));
    },
    separateCols: function (arr, format, group_offset) {
        var newArr = [];
        var jaja = arr.map(function (a) {
            newArr.push(a);
            return a.long_name;
        })
        var colDivs = Math.ceil(12 / group_offset);
        var classNameCustom = 'col-sm-' + colDivs;
        var divArray = [];
        var divKey = group_offset - 1;
        var division = Math.ceil(arr.length / group_offset);
        while (newArr.length > 0) {
            divArray.push(newArr.splice(0, division));
        }


        return divArray.map(function (da, key) {
            var str = '<div key="'+key+'" class="' + classNameCustom + '">';
            str += da.map(format).join('');

            str += '</div>';

            return str


        }).join('');

    },
    render: function () {
        return (
            <div className="row">
                <section className="col-md-12 connectedSortable ui-sortable">
                    <div className="box box-info">
                        <div className="box-header with-border">
                            <h3 className="box-title">Frontline Services</h3>
                        </div>
                        <div className="box-body">
                            <ColumnGroup data={this.state.department_tickets} format={function (data,key) {
                                return React.createElement('p', {key:key}, data.long_name + ' ' + data.ticket_count);
                            }} segment={2} />
                        </div>
                    </div>

                </section>
            </div>
        );
    }
});