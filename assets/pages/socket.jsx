const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
var io = require('socket.io-client');
module.exports = createReactClass({
    displayName: 'Personnel',
    componentDidMount: function () {
        console.log(this.props.index.site_url+'');
        var sockets = io.connect('/realtime/chat');
        sockets.on('message',function (socket) {
            console.log(socket);
        });
    },
    render: function () {
        var chat_item = function (options) {
              return (
                  <div class="item" message_id={options.id}>
                      <img src="/public/dist/img/user4-128x128.jpg" alt="user image" class="online"/>
                          <p class="message">
                              <a href="#" class="name">
                                  <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>{options.date.time}</small>
                                  {options.from.name}
                              </a>
                              {options.message}
                          </p>
                  </div>
              );
        };
        return (
            <div className="row">
                <section className="col-lg-7">
                    <div className="box box-success">
                        <div className="box-header">

                        </div>
                        <div className="box-body">

                        </div>
                        <div className="box-footer">

                        </div>
                    </div>
                </section>
            </div>
        );
    }
});