const React = require('react');
const createReactClass = require('create-react-class');
const ReactRouter = require('react-router');
const {NotificationContainer, NotificationManager} = require('react-notifications');
module.exports = createReactClass({
    displayName: 'Unauthorized',
    componentDidMount:function () {
    },
    render: function() {
        return (
            <div className="content-wrapper">
              <section className="content-header">
                <h1>
                  Activate Account
                </h1>
              </section>
              <section className="content">
                <div className="error-page">
                  <h2 className="headline text-red"><i className="fa fa-lock"></i></h2>
                  <div className="error-content">
                    <h3><i className="fa fa-warning text-info"></i> Please Verify Your Email Address</h3>
                    <p>Please log in to your email account and find the email we have just sent you. Just click the link in that email to activate your account.</p>
                    <div className="row">
                      <div className="col-md-6">
                        <p><i className="fa fa-google text-blue"></i> <a href="https://mail.google.com">Sign-in to Gmail</a></p>
                      </div>
                      <div className="col-md-6">
                        <p><i className="fa fa-yahoo text-purple"></i> <a href="https://login.yahoo.com">Sign-in to Yahoo</a></p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
          </div>
        );
    }
});
