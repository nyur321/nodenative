const React = require('react');
import {Redirect} from 'react-router';
const createReactClass = require('create-react-class');
var IndexSide = require('../components/indexSide.jsx');
var ChatForm = require('../components/chatform.jsx');
module.exports = createReactClass({
    displayName: 'chat_request',
    isAdmin: function () {
        let index = this.props.index;
        return (index.authenticated && index.user.role_id != 3 ? true : false);
    },
    isAuthenticated: function () {
        let index = this.props.index;
        return (index.authenticated);
    },
    render: function () {
        return (
            <div className="container">
                <section className="content">
                    <div className='col-md-8'>
                        <ChatForm index={this.props.index}/>
                    </div>
                    <IndexSide isAdmin={this.isAdmin()} isAuthenticated={this.isAuthenticated()}/>
                </section>
            </div>
        );
    }
});