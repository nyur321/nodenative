const React = require('react');
var createReactClass = require('create-react-class');
module.exports = createReactClass({
      displayName: 'Feedback',
      render: function() {
            return (
                  <section className="content">
                        <div className="box box-primary">
                              <div className="box-header with-border">
                                    <div className="text-center">
                                          <h4>FEEDBACK FORM</h4>
                                    </div>
                              </div>
                              <div className="box-body">
                                    <div className="bg-black">
                                          <p className="text-center">Feedback Details</p>
                                    </div>
                                    <div className="form-group">
                                          <div className="form-group">
                                                <label>Type of Feedback: &nbsp; </label> 
                                                Suggestion
                                          </div>
                                    </div>
                                    <div className="form-group">
                                          <label htmlFor="in">Person(s)/ Unit(s)/ Office concerned or involved: &nbsp;</label>
                                          Management Information Technology
                                    </div>
                                    <div className="form-group">
                                          <label>Facts or Details Surrounding the incident: &nbsp;</label>
                                          The server of GSOIS always shutsdown
                                    </div>
                                    <div className="form-group">
                                          <label>Recommendation(s)/ Suggestion(s)/ Desired Action from our Office: &nbsp;</label>
                                          Purchase new server
                                    </div>
                                    <div className="bg-black">
                                          <p className="text-center">Personal Information of the Sender</p>
                                    </div>
                                    <div className="form-group">
                                          <label>Full Name: &nbsp;</label>
                                          Unknown
                                    </div>
                                    <div className="form-group">
                                          <label>Office/Agency: &nbsp;</label>
                                          Uknown
                                    </div>
                                    <div className="form-group">
                                          <label>Email Address: &nbsp;</label>
                                          unknown@yahoo.com
                                    </div>
                                    <div className="form-group">
                                          <label>Contact Number: &nbsp;</label>
                                          09876543456
                                    </div>
                                    <div className="form-group">
                                          <label>Address: &nbsp;</label>
                                          City Hall loop, Baguio City
                                    </div>
                                    <br/>
                              </div>
                        </div>
                  </section>
            );
      }
});
