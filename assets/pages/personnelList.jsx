const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
module.exports = createReactClass({
	displayName: 'Personnel',
	render: function() {
    	return (
	        <div className="box box-info">
	            <div className="box-header with-border">
	            	<h3 className="box-title">List of Personnels</h3>
	            	<div className="pull-right">
		            	<form className="navbar-form navbar-left" role="search">
				            <div className="form-group">
				            	<input type="text" className="form-control" id="navbar-search-input" placeholder="Search" />
				            </div>
			         	</form>
		         	</div>
	            </div>
	            <div className="box-body table-responsive no-padding">
	            	<table className="table table-hover" width="100%">
		                <tbody>
		                <tr>
		                  <th>ID</th>
		                  <th>Full Name</th>
		                  <th>Email</th>
		                  <th>Designation</th>
		                  <th>Date Created</th>
		                  <th>Status</th>
		                  <th>Action</th>
		                </tr>
		                <tr>
		                  <td>183</td>
		                  <td>John Doe</td>
		                  <td>john@yahoo.com</td>
		                  <td>City Budget Office</td>
		                  <td>13 Sept 2017</td>
		                  <td>Active</td>
		                  <td><button className="btn btn-xs btn-warning">Deactivate</button></td>
		                </tr>
		            </tbody>
	              	</table>
	            </div>
	        </div>
		);    
    }
});