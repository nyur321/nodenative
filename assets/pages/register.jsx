const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const Link = require('react-router').Link;
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
const _ = require('lodash');
const randtoken = require('rand-token');
const {NotificationContainer, NotificationManager} = require('react-notifications');
module.exports = createReactClass({
    displayName: 'Register',
    getInitialState: function(data) {
        return {
            first_name: this.props.first_name || '', 
            middle_name: this.props.middle_name || '', 
            last_name: this.props.last_name || '', 
            email: this.props.email || '',
            password: this.props.password || '',
            password_confirmation: this.props.password_confirmation || '',
            loading: true
        } 
    },
    handleFirstNameChange(event) {
        this.setState({first_name: event.target.value});
    },
    handleMiddleNameChange(event) {
        this.setState({middle_name: event.target.value});
    },
    handleLastNameChange(event) {
        this.setState({last_name: event.target.value});
    },
    handleEmailChange(event) {
        this.setState({email: event.target.value});
    },
    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    },
    handlePasswordConfirmChange(event) {
        this.setState({password_confirmation: event.target.value});
    },
    componentDidMount() {
        /*setTimeout(() => this.setState({ loading: false }), 1000); */
        this.setState({ loading: false});
    },
    handleSubmit(event) {
        event.preventDefault();
        // console.log(this.state);
        this.setState({isSubmitting: true})
        var data = {
            first_name: this.state.first_name,
            middle_name: this.state.middle_name,
            last_name: this.state.last_name,
            email:this.state.email,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
        }
        axios({
            method: 'post',
            url: '/api/user/register',
            data: data,
        }).then(function(response) {
            //console.log(response);
            if(response.data.validations.length > 0) {
                $(this.refs.register_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                this.setState({isSubmitting:false})
                NotificationManager.error('Please check your input fields', 'Something went wrong...');
            } else if(response.data.error) {
                this.setState({isSubmitting:false})
                NotificationManager.error(response.data.error.code, 'Something went wrong...');
            } else {
                this.setState({
                    isSubmimtting: false,
                })
                axios({
                  method: 'POST',
                  url: '/api/send/verification',
                  data: data
                })
                NotificationManager.success('Verification Link is sucessfully sent to your email address');
                browserHistory.push('/verify');
            }
        }.bind(this));
    },
    render: function() {
        const { loading } = this.state;
        if(loading) {
            return (
                <div className="loader"></div>
            )
        }
        return (
            <div className="register-box">
                <div className="register-logo">
                    <Link to="/"><i className="fa fa-address-book-o"></i>&nbsp;<b>OPACD</b></Link>
                </div>
                <div className="register-box-body">
                    <p className="login-box-msg">Register an Account</p>
                    <form onSubmit={this.handleSubmit} ref="register_form">
                        <div className="form-group has-feedback">
                            <input type="text"  name ="first_name" className="form-control" placeholder="First Name"  value={this.state.first_name} onChange={this.handleFirstNameChange} />
                            <span className="glyphicon glyphicon-user form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="text"  name ="mddle_name" className="form-control" placeholder="Middle Name"  value={this.state.middle_name} onChange={this.handleMiddleNameChange} />
                            <span className="glyphicon glyphicon-user form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="text"  name ="last_name" className="form-control" placeholder="Last Name"  value={this.state.last_name} onChange={this.handleLastNameChange} />
                            <span className="glyphicon glyphicon-user form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input name ="email"  className="form-control" placeholder="Email"  value={this.state.email} onChange={this.handleEmailChange} />
                            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password"  name="password" className="form-control" placeholder="Password"  value={this.state.password} onChange={this.handlePasswordChange} />
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password" className="form-control" name="password_confirmation"  placeholder="Confirm Password" value={this.state.password_confirmation} onChange={this.handlePasswordConfirmChange} />
                            <span className="glyphicon glyphicon-unlock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <button type="submit" className="btn btn-primary btn-block btn-flat" disabled={this.state.isSubmitting}>Register</button>
                            </div>
                        </div>
                    </form>
                    <Link to="/login" className="text-center">I already have an account</Link>
                </div>
                <div>
                    <NotificationContainer/>
                </div>
            </div>
        );
    }
});
