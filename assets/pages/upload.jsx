const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');

module.exports = createReactClass({
    displayName: 'Index',
    getInitialState: function(data) {
        return {
          fileName: this.props.fileName || '',
          files:[]
        }
    },
    handleFilenameChange(event){
        this.seState({fileName: event.target.value});
    },
    handleSubmit(event) {
        event.preventDefault();

        axios({
          method: 'post',
          url: '/api/upload',
          data: {fileName: this.state.fileName}
        }).then(function (response) {
          //console.log(response);
        })
       
    },
          
    render: function() {
        return (
            <div className ="container">
                <form onSubmit={this.handleSubmit} encType="multipart/form-data">
                  <h2>Select file to upload:</h2>
                  <br/>
                  <input type="file" name="fileName"/>
                  <br/>
                  <button type="submit">Submit</button>
                </form>
            </div>
        );
    }
});