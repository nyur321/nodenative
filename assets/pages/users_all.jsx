const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
$.DataTables = require('datatables.net');
var Modal = require('../components/modal.jsx');
var Userform = require('../components/userform.jsx');

module.exports = createReactClass({
    displayName: 'Client',
    getInitialState: function () {
        return {
            users: [],
            data:{},
            update_url:"",
            user_id:"1"
        }
    },
    componentWillMount: function () {

    },
    componentDidMount: function () {
        $(this.refs.users_list).DataTable({
            "processing": true,
            "ordering" : true,
            "retrieve": true,
            "paging": true,
            "ajax": {
                "url": "/api/user/all",
                "type": "POST"
            },
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {"data": "email"},
                {
                    "data": "person",
                    "render": function (data, type, full, meta) {
                        return data == null ? 'N/A': data.first_name;
                    }
                },
                {
                    "data": "person",
                    "render": function (data, type, full, meta) {
                        return data == null ? 'N/A': data.last_name;
                    }
                },
                {
                    "data": "anonymous",
                    "render": function (data, type, full, meta) {
                        return data == 1 ? 'Anonymous': 'Registered'
                    }
                },
                {
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<a data-user-id="' + data.id + '" class="btn btn-xs btn-flat btn-success open-user-modal">open</a>';
                    }
                }
            ]
        });


        $(this.refs.users_list).on('click','.open-user-modal',function (e) {
            e.preventDefault();
            var data = {
                id:1,
                email: '',
                departments: 1,
                roles: 3,
                first_name: '',
                middle_name: '',
                last_name: '',
                password: '',
                confirm_password: '',
            };
            var user_id = $(e.target).attr('data-user-id');

            $('#edit_user_modal').modal('show');
            axios({
                method: "GET",
                url: "/api/user/get/"+user_id
            }).then(function (response) {
                // console.log(response.data);
                data['id'] = response.data.id;
                data['email'] = response.data.email;
                data['first_name'] = response.data.first_name || "";
                data['middle_name'] = response.data.middle_name || "";
                data['last_name'] = response.data.last_name || "";
                data['departments'] = response.data.department_id != "" ? response.data.department_id : 0;
                data['roles'] = response.data.role_id != "" ? response.data.role_id : 3;
                data['passport'] = "";
                data['confirm_password'] = "";

                console.log(data);
                this.setState({data:Object.assign(this.state.data,data)});
            }.bind(this));
        }.bind(this));


    },
    render: function () {
        var Userformz = function () {
            return <Userform index={this.props.index} type="update" data={this.state.data} method="POST"/>
        }.bind(this);
        return (
            <div className="box box-info">
                <div className="box-header with-border">
                    <h3 className="box-title">List of Clients</h3>
                </div>
                <div className="box-body">
                    <table className="table table-striped" id="users-dt" ref="users_list" width="100%">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Email</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>User Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Email</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>User Type</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <Modal id="edit_user_modal" title="Edit User" data={this.state.data}>
                    <Userformz/>
                </Modal>

            </div>
        );
    }
})
;