const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const io = require('socket.io-client');
var ChatBox = require('../components/chatbox.jsx');
const _ = require('lodash');
var moment = require('moment');
module.exports = createReactClass({
    displayName: 'Messages',
    getInitialState: function () {
        return {
            chat_status: {},
            chat_lists: [],
            current: {},
            chat_overlay: false,
            overlay_content: ""
        }
    },
    componentWillUnmount: function () {

    },
    acceptChat: function (e) {
        var key = $(e.target).attr('data-key');
        if (this.state.chat_lists[key].accepted) {
            console.log(this.state.chat_lists[key].name + ' is already accepted');
        } else {
            axios({
                url: '/api/chat/accept',
                method: 'post',
                data: {
                    chat: this.state.chat_lists[key]
                }
            }).then(function (response) {
                console.log(response.data);
                var modified_chat_lists = this.state.chat_lists;
                modified_chat_lists[key].accepted = true;
                this.setState({chat_lists: Object.assign(this.state.chat_lists, modified_chat_lists)});
                this.setState({current: Object.assign(this.state.current, this.state.chat_lists[key])});


                if (this.state.chat_lists[key].accepted) {
                    this.setState({chat_overlay: false});
                } else {
                    this.setState({chat_overlay: true});

                }

            }.bind(this));
        }


    },
    updateMessage(data){
        // console.log('data');
        // console.log(data);

    },
    viewChat: function (e) {
        var key = $(e.target).attr('data-key');
        this.setState({current: Object.assign(this.state.current, this.state.chat_lists[key])});

        if (this.state.chat_lists[key].accepted) {
            this.setState({chat_overlay: false});
        } else {
            this.setState({chat_overlay: true});
            this.setState({overlay_content: this.state.current.name});
            this.setState({
                overlay_content: <div className="chat-dialog-box">
                    <p><strong>{this.state.current.name}</strong> wants to chat</p>
                    <p><strong>Subject</strong>: {this.state.current.subject}</p>
                    <ul>
                        {this.state.current.departments.map(function (chat, key_li) {
                            return <li key={key_li}>{chat.name}</li>
                        })}
                    </ul>
                    <button data-key={key} onClick={this.acceptChat} className="btn btn-flat btn-primary">Accept Chat
                    </button>
                </div>
            });
        }
    },
    infoChat: function (e) {
        var key = $(e.target).attr('data-key');
        // console.log(this.state.chat_lists[key]);


    },
    updateChatList: function (err, data) {
        var departments = this.props.index.data.forms.departments;
        if (err) {
            console.log(err);
        } else {
            if (Array.isArray(data.data.chat_list)) {
                var cl = data.data.chat_list.map(function (chat_list) {
                    var depts = chat_list.departments.split(',').map(function (d) {
                        var key_department = _.find(departments, function (dd) {
                            return dd.value == parseInt(d, 10);
                        });

                        return {
                            id: key_department.value,
                            name: key_department.label
                        };

                    });

                    var ret_data = {
                        id: chat_list.id,
                        name: chat_list.user.person.first_name + ' ' + chat_list.user.person.last_name,
                        subject: chat_list.subject,
                        departments: depts,
                        message_index: 0,
                        // accepted: (status == 3 || status == 1 ? false : true),
                        accepted: false,
                        messages: [
                            //     {
                            //     id:0,
                            //     message: 'dsfgsfrgh helloooo',
                            //     type:'message',
                            //     date: '',
                            //     from: {
                            //         name: 'Poncio pilato',
                            //         avatar: ''
                            //     },
                            //     time: '4:00'
                            // }
                        ]
                    };

                    return ret_data;

                });
                this.setState({chat_lists: Object.assign(this.state.chat_lists, cl)});
            }
        }
    },
    componentDidMount: function () {

        this.setState({current: Object.assign(this.state.current, this.state.chat_lists[0])});
        var departments = this.props.index.data.forms.departments;
        if (this.state.current) {

        }
        var socket = io.connect('/realtime/chat');
        socket.emit('wait_for_request', {chat_lists: this.state.chat_lists}, this.updateChatList);
        socket.on('message_update',function (data) {
             console.log(data);
        });
        socket.on('list_update',function (data) {
           console.log(data.chat_lists);
        });


    },
    onAction: function (e) {
        switch (e.type) {
            case 'send':
                console.log(e.data);
                break;
            default:

                break;
        }
    },
    onTyping: function (e) {
        console.log(e)
    },
    render: function () {

        return (
            <div className="row">
                <div className="col-sm-5">
                    <div className="box box-solid">
                        <div className="box-header with-border">
                            <h3 className="box-title">Chat Lists</h3>

                            <div className="box-tools pull-right" data-toggle="tooltip" title=""
                                 data-original-title="Status">
                                <div className="btn-group" data-toggle="btn-toggle">
                                    <button type="button" className="btn btn-default btn-sm active"><i
                                        className="fa fa-square text-green"></i></button>
                                    <button type="button" className="btn btn-default btn-sm"><i
                                        className="fa fa-square text-red"></i></button>
                                </div>
                            </div>
                        </div>
                        <div className="box-body no-padding">
                            <table className="table table-striped">
                                <tbody>
                                {this.state.chat_lists.map(function (chat, key) {
                                    return (
                                        <tr key={key}>
                                            <td>{chat.name}</td>
                                            <td>
                                                <div className="btn-group pull-right" role="group" aria-label="">
                                                    <button type="button"
                                                            className="btn btn-flat btn-default chat-view-btn"
                                                            onClick={this.viewChat} data-key={key}>View
                                                    </button>
                                                    <button type="button"
                                                            className="btn btn-flat btn-default chat-info-btn"
                                                            onClick={this.infoChat} data-key={key}>Info
                                                    </button>
                                                    <button type="button"
                                                            className="btn btn-flat btn-default chat-accept-btn"
                                                            onClick={this.acceptChat} data-key={key}>Accept
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                }.bind(this))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-sm-7">
                    <ChatBox onAction={this.onAction}
                             typing={this.onAction}
                             messages={this.state.current.messages}
                             chat_name={this.state.current.name}
                             chat_show_overlay={this.state.chat_overlay}
                             chat_overlay_content={this.state.overlay_content}
                             chat_subject={this.state.current.subject}/>
                </div>
            </div>
        );
    }
});