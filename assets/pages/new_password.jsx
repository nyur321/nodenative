const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
const Link = require('react-router').Link;
const {NotificationContainer, NotificationManager} = require('react-notifications');
const ReactRouter = require('react-router');
const browserHistory = ReactRouter.browserHistory;
var IndexSide = require('../components/indexSide.jsx');
var Feedbackform = require('../components/feedbackform.jsx');
module.exports = createReactClass({
    displayName: 'Index',
    getInitialState: function(data) {
        return {
            password: this.props.password || '',
            verification: this.props.verification || '',
            password_confirmation: this.props.password_confirmation || ''
        }
    },

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    },
    handleVerificationChange(event) {
        this.setState({verification: event.target.value});
    },
    handlePasswordConfirmChange(event) {
        this.setState({password_confirmation: event.target.value});
    },
    componentDidMount() {
    
    },
    handleSubmit(event) {
        event.preventDefault();
        var data = {
            verification: this.state.verification,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
        }
        this.setState({
            isSubmitting: true
        })
        axios({
            method: 'POST',
            url: '/api/new_password',
            data:data
        }).then(function(response){
            console.log(response)
            if(response.data == 'err') {
                NotificationManager.warning('Token does not match', 'Warning...');
                this.setState({
                    isSubmitting: false
                })
            }
            if(response.data == 'success'){
                NotificationManager.success('Password is successfully changed');
                this.setState({
                    isSubmitting: false
                })
                browserHistory.push('/login')
            }
            if(response.data.validations.length > 0) {
                $(this.refs.login_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
                });
                this.setState({
                    isSubmitting: false
                })
                NotificationManager.error('Please check your input fields', 'Something went wrong...');
            } else {
                null
            } 
        }.bind(this))
    },
    render: function() {
        return (
            <div className="login-box">
                <div className="login-logo">
                    <Link to="/"><i className="fa fa-address-book-o"></i>&nbsp;<b>OPACD</b></Link>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">New password</p>
                    <form onSubmit={this.handleSubmit} ref="login_form">
                        <div className="form-group has-feedback">
                            <input type="text"  name="verification" className="form-control" placeholder="Verification code"  value={this.state.verification} onChange={this.handleVerificationChange} />
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password"  name="password" className="form-control" placeholder="Password"  value={this.state.password} onChange={this.handlePasswordChange} />
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password" className="form-control" name="password_confirmation"  placeholder="Confirm Password" value={this.state.password_confirmation} onChange={this.handlePasswordConfirmChange} />
                            <span className="glyphicon glyphicon-unlock form-control-feedback"></span>
                            <span className="error"></span>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <button type="submit" className="btn btn-primary btn-block btn-flat" disabled={this.state.isSubmitting}>Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                    <NotificationContainer/>
                </div>
            </div>
        );
    }
});