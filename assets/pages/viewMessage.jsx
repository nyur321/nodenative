const React = require('react');
const createReactClass = require('create-react-class');
module.exports = createReactClass({
	displayName: 'Message',
	render: function() {
		let index = this.props.index;
		let bundle_js = index.site_url+'/bundle.js';
		return (
			<div className="content">
				<section className="content-header">
					<h4>
						<i className= "fa fa-envelope"></i>&nbsp; Processing of BIR
					</h4>
			      	<ol className="breadcrumb">
			        	<li><a href="/dashboard"><i className="fa fa-dashboard"></i>Mailbox</a></li>
			        	<li><i className="fa fa-envelop"></i>Processing of BIR</li>
			      	</ol>
		    	</section>
				<section className="content">
		     		<div className="box">
		        		<div className="box-header with-border">
		          			<h4 className="box-title">
			                    <small className="text-muted">Alexander Pierce Brown - <i className="fa fa-clock-o"></i> 04:35</small>
		          			</h4>
			        	</div>
				        <div className="box-body">
				          	What are the requirements in processing BIR?
				        </div>
				        <div className="box-footer">
				          	Attached Files : <a href="#">document.doc</a>
				        </div>
			      	</div>
			      	<div className="box">
		        		<div className="box-header with-border">
		          			<h4 className="box-title">
			                    <small className="text-muted">You - <i className="fa fa-clock-o"></i> 2:16</small>
		          			</h4>
			        	</div>
				        <div className="box-body">
				          	Same..
				        </div>
				        <div className="box-footer">
				          	Attached Files : <a href="#"></a>
				        </div>
			      	</div>
		               	<div className="box-footer">
		               		<textarea rows="5" className="form-control" placeholder ="Your reply...." required/>
		               		<div className="pull-right">
			               		<button type="submit" className="btn btn-info"><i className="fa fa-paperclip"></i>&nbsp;Attach File</button>
					          	<button type="submit" className="btn btn-primary"><i className="fa fa-send"></i>&nbsp;Send</button>
				        	</div>
				        </div>
			    </section>
			</div>
		);
	}
});
