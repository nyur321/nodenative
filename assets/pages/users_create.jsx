const React = require('react');
const createReactClass = require('create-react-class');
const Link = require('react-router').Link;
const axios = require('axios');
$.DataTables = require('datatables.net');
var Modal = require('../components/modal.jsx');
var Userform = require('../components/userform.jsx');
module.exports = createReactClass({
    displayName: 'Client',
    getInitialState: function () {
        return null;
    },
    componentWillUnmount: function () {

    },
    handleInput: function (e) {
        
    },
    componentDidMount: function () {
        
        
    },
    render: function () {
        return (
            <div className="box box-info">
                <div className="box-header with-border">
                    <h3 className="box-title">Create user</h3>
                </div>
                <div className="box-body">
                    <Userform index={this.props.index} type="create" method="POST"/>
                </div>
                <Modal id="modal_users"/>
            </div>
        );
    }
})
;