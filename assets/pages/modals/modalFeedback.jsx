const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
module.exports = createReactClass({
    displayName: 'Mainbox',
    getInitialState: function(data) {
          return {
              errors: []
          }
      },
    handleTypeChange(event) {
        this.setState({feedback_type: event.target.value});
    },
    handleEntityConcernedChange(event){
        this.setState({entity_involved: event.target.value});
    },
    handleFactsDetailsChange(event){
        this.setState({details: event.target.value});
    },
    handleActionChange(event){
        this.setState({action: event.target.value});
    },
    handleSubmit(event){
        event.preventDefault();
        var data = {
          type: this.state.type,
          entity_concerned: this.state.entity_concerned,
          facts_details: this.state.facts_details,
          action: this.state.action,
      }
      axios({
        method: 'post',
        url: '/api/send/register/feedback',
        data: data,
      }).then(function(response){
          console.log(response.data.validations);
          if(response.data.validations.length > 0) {
              $(this.refs.feedback_form).find('input,select,textarea').each(function (i,v) {
                 var obj = _.find(response.data.validations,function (o) {
                     return o.param == $(v).attr('name');
                 });
                 if(obj){
                     $(v).siblings('span.error').html(obj.msg);
                 }else{
                     $(v).siblings('span.error').html('');
                 }
             });
             this.setState({
                  errors: response.data.validations
              })
            } else {
                window.location.reload();
                alert('Feedback Form is successfully submitted!'); 
            }
        }.bind(this));
    },
    render: function() {
        return (
            <div className="modal fade" id="newFeedback">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                                <h4 className="modal-title"><i className="fa fa-edit"></i>&nbsp;Feedback Form</h4>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.handleSubmit} ref="feedback_form">
                                <div className="box-body">
                                      <span>
                                            <p className="text-danger">Fields with * symbol are required</p>
                                      </span>
                                          <div className="form-group">
                                                <label>Type of Feedback *</label>
                                                <select className="form-control select2" name="type" onChange={this.handleTypeChange}>
                                                      <option value="">-- Select --</option>
                                                      <option value="1">Compliment</option>
                                                      <option value="2">Complaint</option>
                                                      <option value="3">Suggestion</option>
                                                </select>
                                                <span className="error"></span>
                                          </div>
                                      <div className="form-group">
                                          <label htmlFor="in">Person(s)/ Unit(s)/ Office concerned or involved *</label>
                                          <textarea  className="form-control" rows="2" name="entity_concerned" onChange={this.handleEntityConcernedChange}/>
                                          <span className="error"></span>
                                      </div>
                                      <div className="form-group">
                                            <label>Facts or Details Surrounding the incident *</label>
                                            <textarea className="form-control" rows="3" name="facts_details" onChange={this.handleFactsDetailsChange}/>
                                            <span className="error"></span>
                                      </div>
                                      <div className="form-group">
                                          <label>Recommendation(s)/ Suggestion(s)/ Desired Action from our Office *</label>
                                          <textarea className="form-control" rows="3" name="action" onChange={this.handleActionChange}/>
                                          <span className="error"></span>
                                      </div> 
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary"><i className="fa fa-send"></i>&nbsp;Submit</button>
                                </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});