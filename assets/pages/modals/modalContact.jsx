const React = require('react');
const createReactClass = require('create-react-class');
const axios = require('axios');
module.exports = createReactClass({
    displayName: 'Mainbox',
    getInitialState: function(data) {
          return {
              errors: []
          }
      },
    handleSubjectChange(event){
        this.setState({subject: event.target.value});
    },
    handleMessageChange(event){
        this.setState({message: event.target.value});
    },
    handleSubmit(event){
       event.preventDefault();
       event.preventDefault();
        var data = {
          subject: this.state.subject,
          message: this.state.message
        }
        axios({
          method: 'post',
          url: '/api/contact/submit',
          data: data,
        }).then(function(response){
            console.log(response.data.validations);
            if(response.data.validations.length > 0) {
                $(this.refs.contact_us_form).find('input,select,textarea').each(function (i,v) {
                   var obj = _.find(response.data.validations,function (o) {
                       return o.param == $(v).attr('name');
                   });
                   if(obj){
                       $(v).siblings('span.error').html(obj.msg);
                   }else{
                       $(v).siblings('span.error').html('');
                   }
               });
               this.setState({
                  errors: response.data.validations
                })
            } else {
                  window.location.reload();
                  alert('Feedback Form is successfully submitted!'); 
            }
        }.bind(this));
    },
    render: function() {
        return (                
            <div className="modal fade" id="askQuestion">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                                <h4 className="modal-title"><i className="fa fa-edit"></i>&nbsp;Ask for Question/Assistance</h4>
                        </div>
                        <div className="modal-body">
                          <span>
                                <p className="text-danger">Fields with * symbol are required</p>
                            </span>
                            <form onSubmit={this.handleSubmit} ref="contact_us_form">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label>Subject</label>
                                        <input type="text" className="form-control" name="subject" onChange={this.handleSubjectChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Message *</label>
                                        <textarea className="form-control" rows="5" placeholder="Your question/concern/request here" name="message" onChange={this.handleMessageChange}/>
                                        <span className="error"></span>    
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Attach files (if any)</label>
                                    <input type="file"/>
                                    <p>Max: 5MB</p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary"><i className="fa fa-send"></i>&nbsp;Submit</button>
                                </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});