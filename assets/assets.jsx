require("babel-polyfill");
const ReactDOM = require('react-dom');
const React = require('react');
const ReactRouter = require('react-router');
// const index = require('./routes/index.jsx');
const index = require('./routes/index.jsx');
const createReactClass = require('create-react-class');



require('bootstrap/dist/css/bootstrap.css');
require('css/AdminLTE.min.css');
require('css/skins/_all-skins.css');
require('datatables.net-bs/css/dataTables.bootstrap.css');
require('ionicons/dist/css/ionicons.min.css');
require('font-awesome/css/font-awesome.css');
require('morris.js/morris.css');
require('jvectormap/jquery-jvectormap.css');
require('bootstrap-datepicker/dist/css/bootstrap-datepicker.css');
require('bootstrap-daterangepicker/daterangepicker.css');
require('css/style.css');
require('css/main.css');
require('react-notifications/lib/notifications.css');


require('jquery');
require('jquery-ui');
require('jquery-ui/ui/widgets/sortable');
require('bootstrap');
require('moment');
require('react-notifications');
require('jvectormap');
require('datatables.net');
require('datatables.net-bs/js/dataTables.bootstrap.js');
require('bootstrap-datepicker');
require('bootstrap-colorpicker');
require('bootstrap-daterangepicker');
require('raphael');
require('slimscroll');
require('jquery-slimscroll');
require('select2');
require('js/adminlte.min.js');
require('js/demo.js');
require('js/jquery.playSound.js');

ReactDOM.render(
    index, document
);
