const ReactDOM = require('react-dom');
const React = require('react');
const ReactRouter = require('react-router');
const createReactClass = require('create-react-class');
const Router = ReactRouter.Router;
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const browserHistory = ReactRouter.browserHistory;
const Header = require('../../app/views/partials/clientHeader.jsx');
const Footer = require('../../app/views/partials/footer.jsx');

var Layout = module.exports = createReactClass({
    displayName: 'Layout',
    render: function () {
        let index = this.props.index;
        let bundle_js = index.site_url + '/public/bundle.js';
        return (
            <html>
            <head>
                <title></title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
                
                <link rel="stylesheet"
                      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"/>
            </head>
            <body className="hold-transition skin-blue layout-top-nav">
            <div className="wrapper">
                <Header/>
                <div className="content-wrapper">
                    <section className="content">
                        {this.props.children}
                    </section>
                </div>
                <Footer/>
            </div>
            <script dangerouslySetInnerHTML={{__html: 'window.PROPS=' + JSON.stringify(index)}}></script>
            <script src="bower_components/jquery/dist/jquery.min.js"></script>
            <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
            <script dangerouslySetInnerHTML={{__html: "$.widget.bridge('uibutton', $.ui.button);"}}></script>
            <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="/bower_components/raphael/raphael.min.js"></script>
            <script src="/bower_components/morris.js/morris.min.js"></script>
            <script src="/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
            <script src="/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
            <script src="/bower_components/moment/min/moment.min.js"></script>
            <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="/public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
            <script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
            <script src="/bower_components/fastclick/lib/fastclick.js"></script>
            <script src={bundle_js}/>
            </body>
            </html>
        );
    }
});

var Welcome = createReactClass({
    displayName: 'Index',
    render: function() {
        return (
            <div className="content">
                <section className="content">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="box box-primary">
                                <div className="box-header with-border">
                                    <div className="text-center">
                                        <h4>FEEDBACK FORM</h4>
                                    </div>
                                </div>
                                <form>
                                    <div className="box-body">
                                        <div className="bg-black">
                                            <p className="text-center">Please let us know how we have served you</p>
                                        </div>
                                                      <span>
                                                            <p className="text-danger">Fields with * symbol are required</p>
                                                      </span>
                                        <div className="form-group">
                                            <div className="form-group">
                                                <label>Type of Feedback *</label>
                                                <select className="form-control select2">
                                                    <option>-- Select --</option>
                                                    <option>Compliment</option>
                                                    <option>Complaint</option>
                                                    <option>Suggestion</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="in">Person(s)/ Unit(s)/ Office concerned or involved *</label>
                                            <textarea id ="in" className="form-control" rows="2" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Facts or Details Surrounding the incident *</label>
                                            <textarea className="form-control" rows="3" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Recommendation(s)/ Suggestion(s)/ Desired Action from our Office *</label>
                                            <textarea className="form-control" rows="3" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Attach files (if any)</label>
                                            <input type="file"/>
                                            <p>Max: 5MB</p>
                                        </div>
                                        <div className="bg-black">
                                            <p className="text-center">Your Personal Information</p>
                                        </div>
                                        <div className="form-group">
                                            <label>Full Name</label>
                                            <input type="text" className="form-control" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Office/Agency (If applicable)</label>
                                            <input type="text" className="form-control" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Email Address *</label>
                                            <input type="email" className="form-control" required/>
                                            <span className= "text-warning">-- Note: Any reply from us will be send in this email</span>
                                        </div>
                                        <div className="form-group">
                                            <label>Contact Number (If any)</label>
                                            <input type="number" className="form-control" required/>
                                        </div>
                                        <div className="form-group">
                                            <label>Address</label>
                                            <input type="text" className="form-control" required/>
                                        </div>
                                        <br/>
                                        <div className="form-group col-sm-4">
                                                            <span>
                                                                  <p className="text-success">Enter the code above here: *</p>
                                                            </span>
                                            <input type="text" className="form-control"/>
                                        </div>
                                    </div>
                                    <div className="box-footer">
                                        <div className="pull-right">
                                            <button type="submit" className="btn btn-block btn-primary"><i className="fa fa-send"></i>&nbsp;Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="callout callout-success" id="hide-mobile">
                                <div className="col-md-6">
                                    <img className="img-circle img-lg img responsive pull-left" src="/public/img/baguio-seal.png" />
                                </div>
                                <div className="col-md-6">
                                    <img className="img-circle img-lg img responsive pull-right" src="/public/img/ph-seal.png" />
                                </div>
                                <h3 className="text-center">Online Public Assistance and Complaints Desk</h3>
                            </div>
                            <div className="callout callout-info ">
                                <h4 className="text-center">Monitor Your Transaction !</h4>
                                <div>
                                    <a type="button" href="/login" className="btn btn-primary btn-block">Login</a>
                                                <span>
                                                      <p className="text-center"> or </p>
                                                </span>
                                    <a type="button" href="/register" className="btn btn-primary btn-block">Register</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
});



const routes = [{
    path: '/',
    component: Layout,
    indexRoute: { onEnter: (nextState, replace) => replace('/welcome') },
    childRoutes: [
        { path: 'welcome', component: Welcome }
    ]
}]



module.exports = (
    <Route path="/" component={Layout}>
        <IndexRedirect to="/welcome" />
        <Route path="welcome" component={Welcome} />
    </Route>
);