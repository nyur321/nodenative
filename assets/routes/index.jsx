const React = require('react');
const ReactRouter = require('react-router');
const Router = ReactRouter.Router;
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const browserHistory = ReactRouter.browserHistory;
const createReactClass = require('create-react-class');
const Link = ReactRouter.Link;
const Api = require('../library/api.js');
var api = new Api('/api/');

function createElement(Component, props) {
    props['api'] = api;
    return <Component {...props} index={window.PROPS}/>;
}

if(typeof window == 'undefined'){
    // console.log('d2 ako sa server');
    // var $ = require("jquery")
}else{
    // console.log('d2 ako sa browser');
    // var $ = require("jquery")
}



var mainIndex = require('../pages/index.jsx');
var layout = require('../components/layout.jsx');
var login = require('../pages/login.jsx');
var forgotPassword = require('../pages/forgot_password.jsx');
var register = require('../pages/register.jsx');
var main = require('../pages/main.jsx');
var messageList = require('../pages/message_all_list.jsx');
var messageClientList = require('../pages/message_client_list.jsx');
var anonymousMessages = require('../components/anonymousmessages.jsx')
var pendingMessages = require('../components/pendingmessages.jsx');
var ongoingMessages = require('../components/ongoingmessages.jsx');
var resolvedMessages = require('../components/resolvedmessages.jsx');
var closedMessages = require('../components/closedmessages.jsx');
var revokedMessages = require('../components/revokedmessages.jsx');
var feedbackList = require('../pages/feedback_list.jsx');
var pendingFeedbacks = require('../components/pendingfeedbacks.jsx');
var acceptedFeedbacks = require('../components/acceptedfeedbacks.jsx');
var revokedFeedbacks = require('../components/revokedfeedbacks.jsx');
var contact_us = require('../pages/contact_us.jsx');
var feedback = require('../pages/feedback.jsx');
var chatRequest = require('../pages/chat_request.jsx');
var profile = require('../pages/profile.jsx');
var usersAll = require('../pages/users_all.jsx');
var personnelList = require('../pages/personnelList.jsx');
var viewMessage = require('../pages/viewMessage.jsx');
var viewFeedback= require('../pages/viewFeedback.jsx');
var chatSupport = require('../pages/chat_support.jsx');
var socketPage = require('../pages/socket.jsx');
var reportPage = require('../pages/reports.jsx');
var usersCreate = require('../pages/users_create.jsx');
var contactform = require('../components/contactform.jsx');
var feedbackform = require('../components/feedbackform.jsx');
var chatform = require('../components/chatform.jsx');
var aside = require('../components/aside.jsx');
var header = require('../components/header.jsx');
var Unauthorized = require('../pages/unauthorized.jsx');
var verify = require('../pages/verify.jsx');
var changePassword = require('../pages/new_password.jsx');
var walkin = require('../pages/walkin.jsx');
// const Register = Router.createRoute({
//     path: 'register',
//     name: 'app',
//     handler: App
// });
//
// const Login= Router.createRoute({
//     name: 'login',
//     handler: Inbox,
//     parentRoute: AppRoute
// });

var routes = [{
    path: '/',
    component: layout,
    indexRoute: { component: mainIndex },
    childRoutes: [
        { path: 'contact', component: mainIndex },
    ]
},{
    path: 'login',
    component: login,
},{
    path: 'register',
    component: register,
}];

const Authorization = (allowedRoles) =>(WrappedComponent) =>
    class WithAuthorization extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                user: {
                    role_name: this.props.index.data.user.role_name,
                    authenticated: this.props.index.authenticated,
                    role_id: this.props.index.data.user.role_id

                }
            }
        }
        render() {
            const { role_name } = this.state.user
            if (allowedRoles.includes(role_name)) {
                return <WrappedComponent {...this.props} />
            } else {
                return <Unauthorized user={this.state.user} />
            }
        }
    }
    const Anonymous = Authorization(['Anonymous']);
    const Client = Authorization(['Citizens']);
    const Personnel = Authorization(['Officers', 'Admin']);
    const Admin = Authorization(['Admin']);
    module.exports = (
        <Router history={browserHistory} createElement={createElement}>
            <Route path='/' component={layout}>
                <IndexRoute component={mainIndex}></IndexRoute>
                <Route path='contact' component={contact_us}> </Route>
                <Route path='feedback' component={feedback}> </Route>
                <Route path='client' component={Client(main)}>
                    <IndexRoute component={messageClientList}></IndexRoute>
                    <Route path="messages" component={messageClientList}></Route>
                    <Route path='feedbacks' component={feedbackList}> </Route>
                    <Route path='feedback' component={feedbackform}> </Route>
                    <Route path='contact' component={contactform}> </Route>
                    <Route path='chat' component = {chatform}> </Route>
                </Route>
                <Route path='login' component = {login}> </Route>
                <Route path='forgot' component = {forgotPassword}> </Route>
                <Route path='register' component = {register}> </Route>
                <Route path='verify' component = {verify}> </Route>
                <Route path='new_password' component = {changePassword}></Route>
                <Route path='chat' component = {chatRequest}> </Route>
                <Route path="dashboard">
                    <IndexRoute component={Admin(reportPage)}></IndexRoute>
                    <Route path="reports" component={Admin(reportPage)}></Route>
                    <Route path="walk-in" component={Admin(walkin)}></Route>
                    <Route path="socket" component={socketPage}></Route>
                    <Route path="chat_support" component={chatSupport}></Route>
                    <Route path="messages">
                        <IndexRoute component={Personnel(messageList)}></IndexRoute>
                        <Route path='anonymous' component = {Admin(anonymousMessages)}></Route>
                        <Route path='pending' component = {Admin(pendingMessages)}></Route>
                        <Route path='ongoing' component = {Admin(ongoingMessages)}></Route>
                        <Route path='resolved' component = {Admin(resolvedMessages)}></Route>
                        <Route path='closed' component = {Admin(closedMessages)}></Route>
                        <Route path='revoked' component = {Admin(revokedMessages)}></Route>
                    </Route>
                    <Route path="feedbacks">
                        <IndexRoute component={Admin(feedbackList)}></IndexRoute>
                        <Route path="pending" component={Admin(pendingFeedbacks)}></Route>
                        <Route path="accepted" component={Admin(acceptedFeedbacks)}></Route>
                        <Route path="revoked" component={Admin(revokedFeedbacks)}></Route>
                    </Route>
                    <Route path="users_all" component={usersAll}></Route>
                    <Route path="users_create" component={usersCreate}></Route>
                    {/*<Route path="personnels" component={personnelList}></Route>*/}
                    <Route path="profile" component={profile}></Route>
                </Route>
            </Route>
            
        </Router>
);
