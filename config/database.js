/**
 * Database configuration
 * */

module.exports = {
    //Environment can be dev or prod
    "env": "dev",

    "dev": {
        "host": null,

        "port":process.env.DB_PORT || 3306,

        "name": process.env.DB_NAME || "opacd_new",

        "username": process.env.DB_USER || 'root',

        "password": process.env.DB_PASSWORD || null,

        "dialect": process.env.DB_DIALECT || "mysql",

        "sql_mode":"" || process.env.DB_SQL_MODE,

        "logging":false,

        "default_connector":"",


        "connectors":{
            "default":"",
            "sequelize":{},
            "mongoose":{
                "uri":""
            }
        },

        "store":"",

        "force_sync":false

    },

    "prod": {
        "host": process.env.DB_HOST || "127.0.0.1",

        "port":process.env.DB_PORT || 3306,

        "name": process.env.DB_NAME || "opacdb_new",

        "username": process.env.DB_USER || 'root',

        "password": process.env.DB_PASSWORD || '',

        "dialect": process.env.DB_DIALECT || "mysql",

        "sql_mode":"" || process.env.DB_SQL_MODE,

        "logging":false,

        "store":"",

        "force_sync":false

    }


};