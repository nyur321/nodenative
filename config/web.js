/**
 * App Configuration
 * */

module.exports = {
    "env": "dev",
    "dev": {
        "title": "Shift App",

        "host":null,

        "port": "1337",

        "proxy": null,

        "cluster":null,

        "static_dir": [
            "public",
            "uploads",
            "bower_components",
        ],

        "timezone": "Asia/Manila",

        "session":"shinjs_app_session",

        "session_secret":"abc123456",

        "log_level":"silly",

        "uploads":"/static/uploads",

        "session_store" :"database",

    },


    "prod": {
        "title": "Shift App",
        "static_dir": [
            "public"
        ],

        "host":"localhost",

        "port": null,

        "proxy": null,

        "timezone": "Asia/Manila",

        "session":"shift_session_default",

        "session_secret":"abc123456",

        "views":{
            "engine":"ejs-mate"
        }
    }
};