/**
 * Assets Configuration
 * */

module.exports = {
    "env": "dev",
    "dev": {

        "bower_url": "",

        "assets_url": "/components",

        "overrides": { //Overrides packages bower.json. This is used when a package is missing a main config
            "bootstrap": {
                "main": [
                    "dist/css/bootstrap.min.css",
                    "dist/js/bootstrap.min.js"
                ]
            },
            "socket.io-client":{
                main:"dist/socket.io.min.js"
            },
            "combodate": {
                "main": "src/combodate.js",
                "dependencies": {
                    "moment": "*",
                    "jquery": "*"
                },
            },
            "jquery-maskmoney": {
                "main":"dist/jquery.maskMoney.min.js",
                "dependencies": {
                    "jquery": "*"
                },
            },
            "blueimp-file-upload":{
                "main":[
                    "css/jquery.fileupload.css",
                    "css/jquery.fileupload-ui.css",
                    "js/jquery.fileupload.js"
                ],
                "dependencies": {
                    "jquery": "*",
                    "jquery-ui": "*"
                }
            }
        },

        "themes": [{
            name: 'default', // Set name of the theme

            "includes":{ // Assets to include to this theme. Set your own custom css and js files

                "css" : [
                    '/public/css/select2.min.css',
                    '/public/css/select2-bootstrap.min.css',
                    '/public/fonts/fonts.css',
                    '/public/css/main.css',
                    '/public/css/style.css',
                    '/public/css/form.css'
                ],

                "js" : [
                    "/public/js/main.js"
                ]
            },

            excludes: ['/AdminLTE/'] // Array of bower packages to exclude from the theme.

        },{
            name: 'admin',

            "includes":{ // Assets to include to this theme. Set your own custom css and js files

                "css" : [
                    '/components/AdminLTE/dist/css/skins/_all-skins.css',
                    "/public/css/admin.css"],

                "js" : ["/public/js/admin.js"]
            },

        }]



    }
};
