/**
 * Email Configuration
 * */

module.exports = {
    "env": "dev",
    "dev": {

        "gmail": {
            "token_dir": "/resources",

            "token_file": "",

            "secret_file": "",

            "scopes":['https://www.googleapis.com/auth/gmail.readonly']
        }

    }
};