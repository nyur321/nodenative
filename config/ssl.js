/**
 * SSL certificate
 * */

module.exports = {
    "env": "dev",

    "dev": {
        "enable":false,

        "certificates": [{
            "name": "your_certificate_name",
            "key": "",
            "cert": "",
        }]

    },

};