var webpack = require("webpack");
var assets = __dirname+'/assets';
// var BowerWebpackPlugin = require("bower-webpack-plugin");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var watchFilePlugin = require("watchfile-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    entry: ['babel-polyfill','./assets/assets.jsx'],
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js'
    },
    watch: false,
    resolve: {
        modules: ["web_modules", "node_modules", "bower_components","public"]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": "jquery",
            DataTable:'DataTable(jquery,$)'
        }),
        // new UglifyJSPlugin(),
        new ExtractTextPlugin("bundle.css"),
        new watchFilePlugin({watchFolder: "assets/", watchExtension: "jsx"})
    ],
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.(jpg|png|gif)$/,  //loads image files
                loader: 'url-loader?limit=100000',
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, //this loads from the root
                loader: 'url-loader',
                options:{
                    name: 'fonts/[name].[ext]'
                }
            },
            // {
            //     test: /\.css$/,
            //     loader: ExtractTextPlugin.extract({
            //         fallback:'style-loader',
            //         use:[{
            //             loader:'css-loader',
            //             options:{
            //                 minimize:true
            //             }
            //         }]
            //     }),

            // },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('css-loader'),

            },
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
        ]
    },
}
