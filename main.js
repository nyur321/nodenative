"use strict";

var realFs = require('fs');
var gracefulFs = require('graceful-fs');
gracefulFs.gracefulify(realFs);


var dotenv = require("dotenv"),
    app = require("express")(),
    os = require("os"),
    express = require("express"),
    async = require("async"),
    http = require("http"),
    EJS = require("ejs"),
    https = require("https"),
    nativefs = require("fs"),
    fs = require("graceful-fs"),
    util = require("util"),
    path = require("path"),
    request = require("request"),
    moment = require("moment-timezone"),
    EventEmitter = require('events'),
    loader = require("./lib/loader")(),
    xpath = require("./lib/xpath"),
    program = require("commander"),
    clusterStability = require("express-cluster-stability"),
    objectTree = require("object-tree"),
    cluster = require("cluster"),
    io = require("socket.io"),
    request = require("request"),
    multer = require("multer"),
    mkdirp = require("mkdirp"),
    dotenv = require("dotenv").config(),
    common = require("./lib/common"),
    winston = require("winston"),
    ot = new objectTree(),
    _ = require("lodash");


var Controller = loader.require("lib/controller"),
    Middleware = loader.require("lib/middleware"),
    Model = loader.require("lib/model"),
    Socket = loader.require("lib/socket"),
    Hook = loader.require("lib/hooks"),
    Defaults = loader.require("lib/defaults"),
    NotifyEvent = loader.require("lib/notifyevent"),
    Plugin = loader.require("lib/plugin");


class Shin extends EventEmitter {
    constructor(options) {
        super();
        var timezone = options.timezone || moment.tz.guess();
        moment.tz.setDefault(timezone);
        process.env.TZ = timezone;
        Object.defineProperties(this, {
            "options": {
                enumerable: false,
                writable: false,
                value: options
            },
            "root": {
                enumerable: true,
                writable: false,
                value: options.root || __dirname
            },
            "app": {
                value: express(),
                writable: true
            },
            "locals": {
                enumerable: false,
                value: {
                    "site_url": "",
                    "date": {}
                },
                writable: true
            },
            "controller": {
                enumerable: true,
                value: null,
                writable: true
            },
            "middleware": {
                enumerable: true,
                value: null,
                writable: true
            },
            "route": {
                enumerable: true,
                value: null,
                writable: true
            },
            "model": {
                enumerable: true,
                value: null,
                writable: true
            },
            "helpers": {
                enumerable: true,
                value: {},
                writable: true
            },
            "io": {
                enumerable: true,
                value: io,
                writable: true,
                configurable: true
            },
            "socket":{
                enumerable: true,
                value: null,
                writable: true
            },
            "config": {
                value: {
                    "host": "",
                    "timezone": timezone,
                    "path": {
                        "controller": path.join(options.root || __dirname, "app/controllers"),
                        "middleware": path.join(options.root || __dirname, "app/middleware"),
                        "views": path.join(options.root || __dirname, "app/views"),
                        "model": path.join(options.root || __dirname, "app/models"),
                        "assets": path.join(options.root || __dirname, "assets"),
                        "config": path.join(options.root || __dirname, "config"),
                        "plugins": path.join(options.root || __dirname, "plugins"),
                        "static": path.join(options.root || __dirname, "public"),
                        "files": path.join(options.root || __dirname, "files"),
                        "resources": path.join(options.root || __dirname, "resources"),
                        "routes": path.join(options.root || __dirname, "app/routes"),
                        "events": path.join(options.root || __dirname, "app/events"),
                    }
                },
                writable: true,
                enumerable: true
            },
            "log": {
                "value": function () {
                    var args = arguments;
                    if(cluster.isMaster){
                        winston.log(args[0], args[1]);
                    }

                },
                writable: true,
                enumerable: true
            },
            "db": {
                enumerable: true,
                value: {
                    connectors: {},
                    model_map: {},
                    models: {},
                    events: {},
                    config: {},
                    dataTypes: {}
                },
                writable: true,
                configurable: true
            },
            "hook": {
                value: null,
                writable: true,
                enumerable: true,
            },
            "hooks": {
                value: {},
                writable: true,
                enumerable: true,
            },
            "plugins": {
                enumerable: true,
                value: [],
                writable: true,
                configurable: true
            },
            "registry": {
                enumerable: true,
                value: {
                    globals: [],
                    path: ["controller", "middleware", "model", "assets", "config", "plugins", "static", "routes"],
                    extensions: [],
                    hooks: [],
                },
                writable: true,
                configurable: true
            },
            "events": {
                value: {
                    "initialized": [],
                    "start": [],
                    "controller": [],
                    "route": [],
                    "model": [],
                    "started": []
                }
            },
            "common": {
                value: common,
                writable: true,
                configurable: true,
                enumerable: true,
            },
            "$": {
                enumerable: true,
                value: {},
                writable: true,
                configurable: true
            }
        });

        //Globalize $


        //Initialize and load hooks
        this.hook = new Hook(this, Shin);
        this.hook.init();

        //Expose hooks
        this.hook.expose('c');
        this.hook.expose('addGlobal');
        this.hook.expose('setGlobal');
        this.hook.expose('getPath');
        this.hook.expose('setPath');
        this.hook.expose('notify.loadEvents');
        this.hook.expose('pushEvents');

        //getAll hooks
        this.hooks = this.hook.getAll();

        //Globalize public functions and variables
        global[options.globalSymbol || '$'] = this.$;

        //add hooks in global object
        this.addGlobal('hooks', this.hooks);

        //add root directory in global object
        this.addGlobal('root', this.root);
        this.addGlobal('common', common);
        this.addGlobal('config', this.c);
        //Initialize Notify class
        this.notify = new NotifyEvent(this);
        this.addGlobal('notify', this.notify);

        //Initialize Middleware class
        this.middleware = new Middleware(this);
        this.addGlobal('middleware', this.middleware);

        //Initialize Controller class
        this.controller = new Controller(this);

        //Initialize Model class
        this.model = new Model(this);

        //Initialize Plugin class
        this.plugin = new Plugin(this);
        this.run = this.plugin.run;
        this.addGlobal('plugin', this.plugin);
        this.addGlobal('run', this.run);

        //Load System Config
        this.loadConfig(this);

        //Load Plugins
        this.plugin.loadPlugins();
        //Load Plugins config
        this.plugin.loadConfig();
        //Fire the Before event lifecycle in Plugins
        this.plugin.loadBefore();
        //Fire initialized event
        this.notify.loadEvents('initialized', {
            type: 'procedure',
            data: null,
        });
    }


    preLoader(callback) {
        var temp_date = moment();
        var virtual_middleware_stack = [{
            name: 'site_details',
            handler: function (req, res, next) {
                res.locals['site_url'] = this.app.get("site_url");
                res.locals['date'] = {
                    'day':temp_date.format('DD'),
                    'month':temp_date.format('MM'),
                    'year':temp_date.format('YYYY'),
                    'time':temp_date.format('HH:mm:s')
                };
                next();
            }.bind(this),
        }];
        this.app.set('views', this.c('path.views'));
        this.middleware.define('defaults', virtual_middleware_stack);
        this.app.use(this.middleware.use('extensions'));
        this.app.use(this.middleware.use('defaults'));
        this.middleware.addFromFile(this.c('path.middleware'));
        callback(null);
    }

    loadConfig() {
        this.config = _.merge(Defaults, this.config);
        var config_files = {};
        var config = loader.config_file(this.config, this.c("path.config"));
        this.config = _.merge(this.config, config);
        this.loadEnv();
    }

    loadHelpers(callback) {
        this.plugin.loadHelpers();
        callback(null);
    }

    loadControllers(callback) {
        this.controller.initControllers();
        this.controllers = this.controller.controllers;
        this.addGlobal('controllers', this.controllers);
        this.addGlobal('getNamespace', this.controller.getNamespace);
        this.notify.loadEvents('controller', {
            type: 'procedure',
            data: null,
        }, function (err, results) {
            callback(null);
        });

    }

    loadRoutes(callback) {
        this.controller.initRoutes();
        this.notify.loadEvents('route', {
            type: 'procedure',
            data: null,
        }, function (err, results) {
            callback(null);
        });
    }


    loadModels(callback) {
        this.model.init(this.c('database.connectors.default'));
        this.addGlobal('model', this.model);
        this.addGlobal('models', this.model.models);
        this.model.onLoad(function (err, results) {
            this.notify.loadEvents('model', {
                type: 'procedure',
                data: null,
            }, function (err, results) {
                callback(null);
            });
        }.bind(this));

    }

    loadPlugins(callback) {
        this.plugin.init(function (err, plugins) {
            this.notify.loadEvents('plugins', {
                type: 'procedure',
                data: plugins,
            }, function (err, results) {
                callback(null);
            });
        }.bind(this));
    }

    init(options,callback) {
    
        this.notify.init();
        this.notify.addEvent('loaded',function (data) {
            switch (data.type) {
                case 'procedure':
                    this.log('info', data.name + ' has loaded');
                    break;

                case 'action':
                    this.log('info', data.name + ' is done');
                    break;
            }
        }.bind(this));


        async.series([
                this.preLoader.bind(this),
                this.loadHelpers.bind(this),
                this.loadControllers.bind(this),
                this.loadRoutes.bind(this),
                this.loadModels.bind(this),
                this.loadPlugins.bind(this)
            ],
            function (err, results) {
                Object.keys(options).forEach(function (opt) {
                    if (options[opt]) {
                        this.c('web.' + opt, options[opt]);
                    }
                });
                this.app.set("port", this.c('web.port'));
                this.app.set("host", this.c('web.host'));
                this.app.set("cluster", this.c('web.cluster'));
                this.app.set("http_protocol", this.c('web.http_protocol'));
                this.server(this);
                if(common.typeOf(callback) == 'function'){
                    callback(this);
                }
            }.bind(this));
    }


    server(own) {
        var http_server = (own.config.ssl.enable ? https : http);
        var server = http_server.createServer(own.app);
        var workers = [];
        server.on("listening", function (address) {
            
            own.plugin.loadServerStarted(server);
            if (cluster.isMaster) {

            } else {
                own.processAfter(server, false);
            }

            own.processAfter(server, true);

        });

        own.controller.attachSockets(server);
        own.addGlobal('sockets',own.controller.sockets);

        
        if (own.app.get("cluster")) {
            var cluster_options = {};
            if (common.typeOf(own.app.get('cluster')) == 'string' || common.typeOf(own.app.get('cluster')) == 'number') {
                try {
                    cluster_options['numberOfWorkers'] = parseInt(own.app.get('cluster'), 10);
                } catch (e) {
                }
            }
            clusterStability(function () {
                return server.listen(own.app.get("port"), own.app.get("host"));
            }, cluster_options, function (log) {

            });

            if (cluster.isMaster) {
                for (const id in cluster.workers) {
                    workers.push(id);
                }
            }
        } else {
            server.listen(own.app.get("port"), own.app.get("host"));
        }
    }

    processAfter(server, isMaster, worker) {
        var own = this, site_url;
        if (server.address()) {
            site_url = own.config.ssl.enable ? 'https' : 'http' + "://" + (server.address().address == "::" ? "127.0.0.1" : server.address().address) + (server.address().port == 80 ? "" : ":" + server.address().port);
        } else {
            site_url = own.config.ssl.enable ? 'https' : 'http' + "://" + own.c('web.host') || '127.0.0.1' + (own.c('web.port') == 80 ? "" : ":" + own.c('web.port'));
        }
        if (isMaster) {
            console.log('Server running @ port:' + own.app.get("port"));
        } else {
            console.log('Worker running');
        }
        own.app.set("site_url", site_url);
        own.site_url = own.app.get("site_url");

        //Error Handler
        own.app.use(function (err, req, res, next) {
            var notification_stack = '';
            notification_stack += '<ul>';
            notification_stack += '<li>' + 'Route url:' + req.url + '</li>';
            err.stack.split("\n").forEach(function (s) {
                notification_stack += '<li>' + s + '</li>';
            });
            notification_stack += '</ul>';
            EJS.renderFile(path.join(own.root, 'resources/error.html'), {data: notification_stack}, function (error, result) {
                res.status(500).send(result);
            });

        });

    }


    loadEnv() {
        var own = this;
        var env = {
            "web.port": process.env.PORT,
            "web.host": process.env.HOST,
            "web.http_proxy" : process.env.HTTP_PROXY,
            "web.https_proxy" : process.env.HTTPS_PROXY,
            "database.database": process.env.DB_NAME,
            "database.sql_engine": process.env.DB_ENGINE,
            "database.connector": process.env.DB_CONNECTOR,
            "database.host": process.env.DB_HOST,
            "database.username": process.env.DB_USER,
            "database.password": process.env.DB_PASSWORD
        };
        try {
            env['web.cluster'] = parseInt(process.env.CLUSTER, 10);
        } catch (e) {

        }


        for (var key in env) {
            if (env[key]) {
                this.c(key, env[key]);
            }
        }
    }


}

program
    .version('0.1.0')
    .option('-p, --port <n>', 'Set the port default: 3000')
    .option('-h, --host [value]', 'Set the hostname default: localhost')
    .option('-c, --cluster [type]', 'Enter clustering mode defaults to num of cpus')
    .parse(process.argv);


var shin = new Shin({
    root: __dirname,
    config_file: "",
    globalSymbol: "$"
});

shin.init({
    port: program.port,
    host: program.host,
    cluster: program.cluster
},function (system) {
    
});







