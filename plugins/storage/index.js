/**
 * This is the example module
 * @type {{init: module.exports."init", provides: module.exports."provides", hooks: module.exports."hooks", config: module.exports."config"}}
 */
var path = require('path');
var fs = require('fs');
var async = require('async');


module.exports = {

    "package": {
        dependencies: {}
    },

    "init": function (plugin, callback) {
        console.log(plugin.config)
        callback(null);
    },

    "lifecycle": {
        before: function (plugin) {
            
        },
        started: function () {

        }
    },

    "config": function () {
        return {
            name:"storage",
            inherit:{
                "resources":"storage.resources",
                "uploads":"storage.uploads"
            },
            write:true,
            config:{
                "resources":"/resources",
                "uploads":"/resources/uploads"
            }
        };
    }

};