/**
 * This is the example module
 * @type {{init: module.exports."init", provides: module.exports."provides", hooks: module.exports."hooks", config: module.exports."config"}}
 */
var path = require('path');
var fs = require('fs');
var Sequelize = require('sequelize');
var async = require('async');
const Op = Sequelize.Op;
const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
};

module.exports = {

    "package": {
        dependencies: {}
    },

    "init": function (plugin, callback) {
        callback(null);
    },

    "lifecycle": {
        before: function (plugin) {
            var config = plugin.config;
            plugin.hooks.registerOrm('sequelize', {
                instance: Sequelize,
                dataTypes:Sequelize.DataTypes,
                connector: function () {
                    return new Sequelize(config.name, config.username, config.password, {
                        host: config.host,
                        dialect: 'mysql',
                        logging: false,
                        operatorsAliases:operatorsAliases
                    });
                },
                model: function (model, sequelize) {
                    var model_seq = sequelize.define(model.name, model.attributes, model.options);
                    if (model.options.hasOwnProperty('classMethods')) {
                        if ("associate" in model.options.classMethods) {
                            model_seq.associate = model.options.classMethods.associate;
                        }
                        Object.keys(model.options.classMethods).filter(function (classMethods) {
                            return classMethods != 'associate'
                        }).forEach(function (classMethod) {
                            model_seq[classMethod] = function () {
                                return model.options.classMethods[classMethod].apply(sequelize,arguments);
                            }.bind(plugin.system);
                        });
                    }
                    return model_seq;
                },
                load: function (sequelize, models) {
                    Object.keys(models).forEach(function (modelName) {
                        if ("associate" in models[modelName]) {
                            models[modelName].associate(models);
                        }
                    });
                    return function (callback) {
                        sequelize.sync({
                            force: config.force_sync
                        }).then(function (err, results) {
                            callback(null, results)
                        }).catch(Sequelize.Error, function (err) {
                            callback(err, null);
                        })
                    };

                }
            }, config);
        },
        started: function () {

        }
    },

    "config": function () {
        return [{
            name: 'database.connectors',
            config: {
                "default": "sequelize"
            }
        }, {
            name: 'database.connectors.sequelize',
            inherit: {
                host: 'database.host',
                port: 'database.port',
                name: 'database.name',
                username: 'database.username',
                password: 'database.password',
                dialect: 'database.dialect',
            },
            config: {
                host: "127.0.0.1",
                port: "",
                name: 'sequelize_db',
                username: 'root',
                password: '',
                dialect: 'mysql',
                charset: 'utf8',
                collate: 'utf8_general_ci',
                logging: false,
                force_sync: false,
                timezone: '',
                pool: {
                    max: 5,
                    min: 0,
                    idle: 1000
                },

            },
            validate: function (config) {
                if (typeof config == 'object') {
                    for (var key_config in config) {
                        config[key_config].valid = true;
                    }
                }
                return config;
            }

        }, {
            name: 'database.connectors.mongoose',
            config: {
                uri: 'mongodb://localhost'
            },
            validate: function (config) {
                if (typeof config == 'object') {
                    for (var key_config in config) {
                        config[key_config].valid = true;
                    }
                }
                return config;
            }

        }];
    }

};