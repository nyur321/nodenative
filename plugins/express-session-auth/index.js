/**
 * This is the example module
 * @type {{init: module.exports."init", provides: module.exports."provides", hooks: module.exports."hooks", config: module.exports."config"}}
 */
var path = require('path');
var fs = require('fs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


module.exports = {

    "package": {
        "version": "0.0.1",
        "dependencies": {
            "orm": "*",
            "express-basics": "*"
        }
    },

    "init":function(plugin,callback){
        






        callback(null);
    },

    lifecycle:{
        before:function(plugin){
            
            


            




        },
        started:function(){

        }
    },

    "config": function () {
        return [{
            name: 'database.connectors.sequelize',
            inherit: {
                session: 'web.session',
                session_secret: 'web.session_secret',
            },
            config: {
                session: null,
                session_secret: "",

            }
        }]
    }


};