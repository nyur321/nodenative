"use strict";
require('babel-register')({
    presets:['react']
});

var path = require('path');
var fs = require('fs');
var babel = require("babel-core");
var react = require('react');
var express = require('express');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');
var ejs = require('ejs');
var reactRender = require(path.join(__dirname,'lib/express-react-render.jsx'));
var async = require('async');

module.exports = {

    "package":{
        dependencies:{
            "express-views":"*"
        }
    },

    "init":function(plugin,callback){

        
        callback(null);
    },
    "lifecycle":{
        before:function (plugin) {
            var render = new reactRender({
                assets:plugin.getPath('assets'),
                views:plugin.getPath('views'),
                expressRouter:express.Router()
                // routes:path.join(plugin.getPath('assets'),'routes/new_route.jsx'),
            });

            // var reactRoute = express.Router();
            // reactRoute.get('*',render.middleware);
            plugin.fn('router',function (options) {
                return render.init(options);
            });


        },
        started:function () {

        }
    },

    "config":{
        name:'react',
        config:{
            'views':'',
            'route_index':'routes/index.jsx'
        }
    }

};