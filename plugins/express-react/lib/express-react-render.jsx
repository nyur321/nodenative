"use strict";
var path = require('path');
var fs = require('fs');
var babel = require("babel-core");
var async = require("async");
var ReactDOM = require("react-dom");
var React = require("react");
var ReactDOMServer = require("react-dom/server");
const ReactRouter = require('react-router');
var ejs = require('ejs');

class Reactrender {
    constructor(options) {

        this.assets = options.assets;
        this.expressRouter = options.expressRouter;
        this.routes = options.routes || 'routes/index.jsx';
        this.views = options.views;
        this.base_url = '';
        this.middleware = this._middleware.bind(this);
        this.props = {
            locals: {}
        };
    }

    init(options) {
        var default_options = {
            props_locals:['site_url'],
            base_url:'',
            user:{
                id:null
            },
            data:{},
            title:'OPADC',
            parent:this
        };
        if(options){
            if(options.hasOwnProperty('base_url')){
                this.base_url = options['base_url'] || '';
                default_options.base_url = options['base_url'] || '';
            }
            if(options.hasOwnProperty('props_locals')){
                default_options.props_locals = options['props_locals'] || [];
            }
            if(options.hasOwnProperty('data')){
                default_options.data = options['data'] || {};
            }
        }


        this.expressRouter.get('*',function(req,res,next){
            req.isAuthenticated()
            next()
            /*if(req.isAuthenticated()){
                next()
            }else{
                if(req.url == '/dashboard' || req.url == '/client'){
                    res.redirect('/login');
                }else{
                    next();
                }
            }*/
        },function (req,res) {
            return this.middleware(default_options,req,res);
        }.bind(this));
        return this.expressRouter;

    }

    renderHtml(file, data, callback) {
        var renderProcess = [
            function (callback) {
                ejs.renderFile(path.join(this.views, file), data, function (error, result) {
                    callback(error, result);
                });
            }.bind(this)
        ];
        async.series(renderProcess, function (error, result) {
            callback(error, result);
        });

    }

    _middleware(options,req, res) {
        this.props.data = {};
        Object.keys(res.locals).forEach(function (key) {
            var local_key = key.split('_');
            if(local_key.hasOwnProperty('length')){
                if(local_key[0] == 'props'){
                    this.props.data[local_key[1]] = res.locals[key];
                }
            }
        }.bind(this));

        Object.keys(this.props).forEach(function (key) {
            this.props.locals[key] = res.locals[key];
        }.bind(this));

        this.props['title'] = this.props.locals['title'] || 'Online Public Assistance and Complaints Desk';
        this.props['site_url'] = res.locals.site_url;
        this.props['base_url'] = res.locals.site_url+this.base_url;
        this.props['params'] = res.locals.react_params || {};
        this.props['user'] = res.locals.props_user;
        this.props['authenticated'] = req.isAuthenticated();
        var data = {
            partials: function () {

            },
            layout: function () {

            },
            block: function () {

            }
        };

        var props = this.props;
        ReactRouter.match({
            routes: require(path.join(this.assets, this.routes)),
            location: req.url,
        }, function (error, redirectLocation, renderProps) {
            if (renderProps) {
                var assigned_obj = Object.assign({}, renderProps, {
                    createElement: function createElement(Component, renderProps) {
                        return React.createElement(Component, Object.assign({}, renderProps, {index: props}));
                    }
                });
                var html = ReactDOMServer.renderToString(
                    React.createElement(ReactRouter.RouterContext, assigned_obj)
                );
                res.send(html);

            } else {
                res.status(404).send('Not Found');
            }
        });
    }


}



module.exports = Reactrender;