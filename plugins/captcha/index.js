"use strict";

var path = require('path');
var fs = require('fs');
var svgCaptcha = require('svg-captcha');

class Captcha {
    constructor(options) {
        if (typeof options != 'object') {
            options = {
                width: options.width || 250, // width of captcha
                height: options.height || 100, // height of captcha
                fontSize: options.fontSize || 16, // captcha text size
                charPreset: options.charPreset // random character preset
            };
        }
        this.data = "";
        this.text = "";
        this.initialized = false;
        this.config = {
            width: options.width || 250, // width of captcha
            height: options.height || 100, // height of captcha
            fontSize: options.fontSize || 16, // captcha text size
            charPreset: options.charPreset // random character preset
        };
        this.svgCaptcha = svgCaptcha;
        this.response = null;
        this.request = null;
    }

    init(req, res, type) {
        var default_type = type || 'text';
        var newCaptcha = null;
        this.request = req;
        this.response = res;
        switch (default_type) {
            case 'math':
                newCaptcha = this.svgCaptcha.createMathExpr(this.config);
                break;
            case 'text':
                newCaptcha = this.svgCaptcha.create(this.config);
        }

        this.initialized = true;

        return {
            text: newCaptcha.text,
            data: newCaptcha.data
        };

    }

    refresh() {
        if (this.initialized) {


        }

    }

    generate() {

    }



}


module.exports = {

    "package": {
        dependencies: {
            "express-views": "0.0.1"
        }
    },

    "init": function(plugin, callback) {
        var captcha = new Captcha({

        });
        // captcha.init();
        
        plugin.fn('init', function(options) {
            return captcha.init();
        });


        callback(null);
    },
    "provides": function() {


    },

    "hooks": function() {


    },

    "on": function(event) {


    },

    "config": {
        name: 'captcha',
        config: {
            width: "300",
            height: "100",
            fontSize: "14",
            charPreset: "",
            size: 4,
            ignoreChars: "",
            noise: 1,
            color: true,
            background: '#cc9966',
        }
    }

};