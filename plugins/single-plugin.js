/**
 * This is the example module
 * @type {{init: module.exports."init", provides: module.exports."provides", hooks: module.exports."hooks", config: module.exports."config"}}
 */
var path = require('path');
var fs = require('fs');
var admin_routes = require('express').Router();
var node_routes = require('express').Router();
class Nodes{
    constructor(){
        
    }
}

module.exports = {

    "package":{
        dependencies:{
            "express-basics":"*",
            "express-views":"*"
        }
    },

    "init":function(plugin,callback){
        // console.log(plugin.getPath('files'));
        admin_routes.get('/',function (req,res) {
            res.send('admin override');
        });

        admin_routes.get('/users',function (req,res) {
            res.send('admin users');
        });



        plugin.app.use('/admin',admin_routes);
        callback(null);

        
    },
    lifecycle:{
        before:function(){

        },
        started:function(plugin,server){
            

        }
    },

    "config":{
        name:'simple-plugin',
        config:{
            "sehiya":"mastup"
        }
    }

};