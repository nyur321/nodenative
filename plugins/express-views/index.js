"use strict";
var path = require('path');
var fs = require('fs');
var ejs = require('ejs');
var ejsMate = require('ejs-mate');


module.exports = {

    "package":{
        dependencies:{

        }
    },

    "init":function(plugin,callback){
        plugin.app.engine('html', ejsMate);
        plugin.app.set('view engine', 'html');
        callback(null);
    },
    "provides":function(){


    },

    lifecycle:{
        before:function(){

        },
        started:function(){

        }
    },

    "config":{
        name:'views',
        config:{
            "engine": "ejs-mate",
            "ext":"html"
        },
        inherit:{
            "engine":"views.engine",
            "ext":"views.ext"
        }
    }

};