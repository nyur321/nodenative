'use strict'

/**
 * Initiates all express extensions
 * @param own
 */

var expressSession = require('express-session');
var Store = require('express-sequelize-session')(expressSession.Store);


module.exports = class {

    constructor(own) {
        this.app = own.app;
        this.config = own.config;
        this.own = own;

        this.defaultExtensions = [

            //{name:'csurf',ext:require("csurf"),params:null},
            {
                "name": "express-session", ext: expressSession,
                "params": {
                    name: own.c("web.session"),
                    secret: own.c("web.session_secret"),
                    resave: false,
                    saveUninitialized: true,
                    store: (own.c("web.session_store") == "database" ? new Store(own.db.sequelize) : null)
                }
            },
            {
                "name": "body-parser", ext: require("body-parser"),
                "params": null,
                "extended": [{
                    "name": "urlencoded",
                    "params": {
                        "extended": true
                    }
                }]
            },
            {
                "name": "cookie-parser",
                "ext": require("cookie-parser"),
                "params": [own.c("web.session_secret")]
            },
            {
                "name": "express-validator",
                "ext": require("express-validator"),
                "params": {customValidators: require(own.config.path.validations)}
            }

        ]


    }


    init(cb) {
        this.loadExpressExt();
        cb();
    }


    loadExpressExt() {
        var own = this.own;
        var app = this.app;

        this.defaultExtensions.forEach(function (exts) {
            own.registerExtension(exts, own);
        });


        app.use(function (req, res, next) {
            res.locals['site_url'] = app.get('site_url');
            res.locals['date'] = app.get('date');
            next();
        });


    }



};