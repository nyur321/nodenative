"use strict";

var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var expressSession = require('express-session');
var SequelizeStore = require('connect-session-sequelize')(expressSession.Store);
var Sequelize = require('sequelize');
var expressValidator = require('express-validator');
var passport = require('passport');
var passportSocketIo = require("passport.socketio");
module.exports = {

    "package": {
        dependencies: {
            "orm": "*"
        }
    },

    "init": function (plugin, callback) {
        var customValidators = {};
        plugin.setPath('validations', 'app/validations');
        try {
            customValidators = require(plugin.getPath('validations'));
        } catch (e) {
            plugin.system.log('warn', e.message + ' in express-basics');
        }

        plugin.middleware.addMiddleware('extensions', {
            name: 'body-parser',
            handler: bodyParser.urlencoded({extended: true}),
        });
        plugin.middleware.addMiddleware('extensions', {
            name: 'body-parser-json',
            handler: bodyParser.json(),
        });


        plugin.middleware.addMiddleware('extensions', {
            name: 'express-validator',
            handler: expressValidator({
                customValidators: customValidators
            })
        });

        var onAuthorizeSuccess = function (data, accept) {
            console.log('successful connection to socket.io');

            // The accept-callback still allows us to decide whether to
            // accept the connection or not.
            accept(null, true);

            // OR

            // If you use socket.io@1.X the callback looks different
            accept();
        };

        var onAuthorizeFail = function (data, message, error, accept) {
            if (error)
                throw new Error(message);
            console.log('failed connection to socket.io:', message);

            // We use this callback to log all of our failed connections.
            accept(null, false);

            // OR

            // If you use socket.io@1.X the callback looks different
            // If you don't want to accept the connection
            if (error)
                accept(new Error(message));
        };


        var SessiondbStore = null;

        if (plugin.config.session_store == 'sequelize' || plugin.config.session_store == 'database') {
            SessiondbStore = new SequelizeStore({db: plugin.system.db.connectors['sequelize']});
        }

        plugin.middleware.addMiddleware('extensions', {
            name: 'cookie-parser',
            handler: cookieParser(),
        });
        plugin.middleware.addMiddleware('extensions', {
            name: 'express-session',
            handler: expressSession({
                name: plugin.config.session,
                secret: plugin.config.session_secret,
                resave: false,
                saveUninitialized: true,
                store: SessiondbStore
            })
        });

        // plugin.middleware.addMiddleware('extensions', {
        //     name: 'passport-socket',
        //     type:'socket',
        //     handler: passportSocketIo.authorize({
        //         cookieParser: cookieParser,       // the same middleware you registrer in express
        //         key:          plugin.config.session,       // the name of the cookie where express/connect stores its session_id
        //         secret:       plugin.config.session_secret,    // the session_secret to parse the cookie
        //         store:        SessiondbStore,        // we NEED to use a sessionstore. no memorystore please
        //         success:      onAuthorizeSuccess,  // *optional* callback on success - read more below
        //         fail:         onAuthorizeFail,     // *optional* callback on fail/error - read more below
        //     })
        // });
        plugin.fn('passport-socket', function (options) {
            var default_options = {
                cookieParser: cookieParser,       // the same middleware you registrer in express
                key: plugin.config.session,       // the name of the cookie where express/connect stores its session_id
                secret: plugin.config.session_secret,    // the session_secret to parse the cookie
                store: SessiondbStore,     // *optional* callback on fail/error - read more below
            };
            if ($.common.typeOf(options) == 'object') {
                $.common.extend(default_options, options);
            }
            return passportSocketIo.authorize(default_options);
        });

        plugin.middleware.addMiddleware('extensions', {
            name: 'passport-initialize',
            handler: passport.initialize(),
        });
        plugin.middleware.addMiddleware('extensions', {
            name: 'passport-session',
            handler: passport.session(),
        });


        if (SessiondbStore) {
            plugin.system.hooks.pushEvents('model', function (event_cb) {
                event_cb(null);
            });
            SessiondbStore.sync().then(function () {
                callback(null);
            });
        }


    },
    "provides": function () {


    },

    "lifecycle": {
        before: function (plugin) {
            plugin.fn('passport', function () {
                return passport;
            });

            var static_dir = ['/public'];
            plugin.app.use('/public', plugin.middleware.use('static'), express.static(plugin.c('path.static')));
            if (plugin.common.typeOf(plugin.config.static_dir) == 'array') {
                plugin.config.static_dir.forEach(function (static_dir) {
                    if (static_dir != 'public') {
                        plugin.app.use('/' + static_dir, express.static(path.join(plugin.system.root, static_dir)));
                    }
                });
            }

        },
        started: function (plugin, server) {

        }
    },

    "config": {
        name: 'web',
        inherit: {
            "title": "web.title",
            "host": "web.host",
            "port": "web.port",
            "proxy": "web.proxy",
            "static_dir": "web.static_dir",
            "timezone": "web.timezone",
            "session": "web.session",
            "session_secret": "web.session_secret",
            "log_level": "web.log_level",
            "uploads": "web.uploads",
            "session_store": "web.session_store",
        },
        config: {
            "title": "Shift App",
            "host": null,
            "port": "1337",
            "proxy": null,
            "static_dir": [
                "public",
                "uploads"
            ],
            "timezone": "Asia/Manila",
            "session": "shinjs_app_session",
            "session_secret": "abc123456",
            "log_level": "silly",
            "uploads": "/static/uploads",
            "session_store": "sequelize",
        },
        validate: function (config) {
            if (typeof config == 'object') {
                for (var key_config in config) {
                    config[key_config].valid = true;
                }
            }
            return config;
        }
    }

};