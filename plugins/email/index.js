var fs = require("fs");
var _ = require("lodash");
var async = require("async");
var path = require('path');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var ejs = require('ejs');
var moment = require('moment');

class GmailComposer {
	constructor(config) {
		this.token = config.token;
		this.googleAuth = new googleAuth();
		this.auth = null;
		this.token_dir = config.token_dir;
		this.token_file = config.token_file || 'gmail-nodejs-quickstart.json';
		this.scopes = [
			'https://mail.google.com/',
			'https://www.googleapis.com/auth/gmail.modify',
			'https://www.googleapis.com/auth/gmail.compose',
			'https://www.googleapis.com/auth/gmail.send'
		];
		this.secretFile = config.secretFile || 'client_secret.json';
		this.proxy = config.proxy;
		this.gmail = google.gmail({
			version: 'v1',
			proxy: this.proxy
		});
		this.messages = [];
		this.defaultName = 'Shinjs Mailer';
		this.message_structure = {
			subject: '',
			to: '',
			from: {
				name: '',
				email: ''
			},
			message: '',
			html: true
		};
		this.message_id = 1;
		this.authorized = false;
	}

	authorize(callback) {
		var clientSecret = "";
		var clientId = "";
		var redirectUrl = "";
		var oauth2Client;
		this.loadSecret(function(err, credentials) {
			if (err) {
				console.log('Error loading client secret file: ' + err);
				return;
			} else {
				clientSecret = credentials.installed.client_secret;
				clientId = credentials.installed.client_id;
				redirectUrl = credentials.installed.redirect_uris[0];
				oauth2Client = new this.googleAuth.OAuth2(clientId, clientSecret, redirectUrl);
				// Check if we have previously stored a token.
				fs.readFile(path.join(this.token_dir, this.token_file), function(err, token) {
					if (err) {
						this.getNewToken(oauth2Client, callback);
					} else {
						oauth2Client.credentials = JSON.parse(token);
						this.auth = oauth2Client;
						this.authorized = true;
						callback(oauth2Client);
					}
				}.bind(this));
			}
		}.bind(this));
	}

	getNewToken(oauth2Client, callback) {
		var authUrl = oauth2Client.generateAuthUrl({
			access_type: 'offline',
			scope: this.scopes
		});

		console.log('Authorize this app by visiting this url: ', authUrl);

		var rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		rl.question('Enter the code from that page here: ', function(code) {
			rl.close();
			oauth2Client.getToken(code, function(err, token) {
				if (err) {
					console.log('Error while trying to retrieve access token', err);
					return;
				}
				oauth2Client.credentials = token;
				this.storeToken(token);
				callback(oauth2Client);
			}.bind(this));
		}.bind(this));
	}

	storeToken(token) {
		try {
			fs.mkdirSync(TOKEN_DIR);
		} catch (err) {
			if (err.code != 'EEXIST') {
				throw err;
			}
		}
		fs.writeFile(path.join(this.token_dir, this.token_file), JSON.stringify(token));
		console.log('Token stored to ' + this.token_dir);
	}

	loadSecret(callback) {
		fs.readFile(path.join(this.token_dir, this.secretFile), function processClientSecrets(err, content) {
			callback(err, JSON.parse(content));
		});
	}

	listLabels() {
		this.gmail.users.labels.list({
			auth: this.auth,
			userId: 'me',
		}, function(err, response) {
			// console.log(response);
			if (err) {
				console.log('The API returned an error: ' + err);
				return;
			}else{
				if(response){
					console.log('GMAIL api connected')
				}
			}
			var labels = response.labels;
			if (labels.length == 0) {
				// console.log('No labels found.');
			} else {
				// console.log('Labels:');
				for (var i = 0; i < labels.length; i++) {
					var label = labels[i];
					// console.log('- %s', label.name);
				}
			}
		});
	}

	compose(options, callback) {
		var message_structure = {
			subject: options.subject || '',
			to: options.to || '',
			from: {
				name: options.from ? (typeof options.from.name != 'undefined' ? options.from.name : options.from.name) : '',
				email: ''
			},
			message: options.message || '',
			body:'',
			template:'',
			html: true,
			type:'text/html',
			locals:{}
		};



		var str = ["Content-Type: "+(message_structure.html ? 'text/html' : 'text/plain')+"; charset=\"UTF-8\"\n",
			"MIME-Version: 1.0\n",
			"Content-Transfer-Encoding: 7bit\n",
			"to: ", message_structure.to, "\n",
			"from: ", message_structure.from.name, "\n",
			"subject: ", message_structure.subject, "\n\n",
			message_structure.message
		].join('');

		var encodedMail = {
			id: this.message_id,
			message_structure: message_structure,
			raw: new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_')
		};
		this.messages.push(encodedMail);
		this.message_id++;
		callback(null, encodedMail);
	}


	send(id, callback) {
		var message = _.find(this.messages, function(message) {
			return message.id == id;
		});

		if (this.authorized) {
			this.gmail.users.messages.send({
				auth: this.auth,
				userId: 'me',
				resource: {
					raw: message.raw
				}
			}, function(err, response) {
				console.log(err)
				callback(err, response)
			});
		}


	}



}


module.exports = {

	"package": {
		dependencies: {
			"express-basics": "*",
			"express-views": "*"
		}
	},

	"init": function(plugin, callback) {
		// // console.log(plugin.getPath('files'));

		var gmail = new GmailComposer({
			proxy: plugin.c('web.http_proxy') || null,
			token_dir: plugin.path.resources,
			token_file: 'gmail-nodejs-quickstart.json',
		});

		

		plugin.fn('compose', function(options, callback) {
			// console.log(options);
			// callback(options)
			gmail.compose(options, callback);
		});

		plugin.fn('send', function(message_id, callback) {
			gmail.send(message_id, callback);
		});

		gmail.authorize(function(auth) {
			gmail.listLabels();
		});

		callback(null);
	},
	lifecycle: {
		before: function() {

		},
		started: function(plugin, server) {


		}
	},

	"config": {
		name: 'mailer',
		inherit: {
			token_dir: "mail.token_dir",
			token_file: "mail.token_file",
			scopes: "mail.scopes",
			secret_file: "mail.secret_file"
		},
		config: {
			token_dir: "/resources",
			token_file: "",
			scopes: "",
			secret_file: ""
		}
	}

};